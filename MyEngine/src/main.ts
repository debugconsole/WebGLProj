
// This is meant for use with the AMD loader.
// require([
//     'mirror'
// ], function(mirror) {
//     'use strict';
//     /*global self,module*/
//     if (typeof window !== 'undefined') {
//         window.Mi = mirror;
//     } else if (typeof self !== 'undefined') {
//         self.Mi = mirror;
//     } else if(typeof module !== 'undefined') {
//         module.exports = mirror;
//     } else {
//         console.log('Unable to load Mi.');
//     }
// }, undefined, true);

//import * as mirror from "./mirror"
import * as mirror from "./index"

declare global {  interface Window {
    Mi: object
  }
}

window.Mi = mirror;
console.log("set window property:" , (<any>window).Mi);