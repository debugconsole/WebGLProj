import {ProgramInfo} from "./program"
import normal_vertex from "./shaders/normal_vertex"
import normal_fragment from "./shaders/normal_fragment"
import lambert_vs from "./shaders/lambert_vs"
import lambert_fs from "./shaders/lambert_fs"

export class BuiltInProgram{

    static createNormalProgram():ProgramInfo{
        return new ProgramInfo(normal_vertex,normal_fragment);
    }

    static createLambertProgram():ProgramInfo{
        return new ProgramInfo(lambert_vs,lambert_fs);
    }

}