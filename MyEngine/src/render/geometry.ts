
import { EGLDataType, TypedArray } from "./gldatatype"



export interface AttributeOptions{
    buffer: TypedArray;
    numComponents: number;
    sizeofStride: number;
    offsetofStride: number;
    normalized?: boolean;
    isConstantToShader?: boolean;
}


/**
 * attribute info
 */
export class AttributeInfo {

    name: string;
    buffer: TypedArray;
    isConstantToShader: boolean;

    /**
     * represent the type count of components in a attribute
     * must be 1-4 
     */
    numComponents: number;
    sizeofStride: number;
    offsetofStride: number;
    normalized: boolean;
    glType?: EGLDataType;
    glBuffer?: WebGLBuffer;
    buffertype:any;

    constructor(name:string,options:AttributeOptions){
        this.name = name;
        this.buffer = options.buffer;
        this.isConstantToShader = options.isConstantToShader || false;
        this.numComponents = options.numComponents;
        this.sizeofStride = options.sizeofStride;
        this.offsetofStride = options.offsetofStride;
        this.normalized = options.normalized || false;
        this._checkBufferType();
    }


    copy(source:AttributeInfo){
        this.name = source.name;
        if (source.buffertype) {
            this.buffertype = source.buffertype;
        }else{
            this.buffertype = Float32Array;
        }
        this.buffer = new this.buffertype(source.buffer.buffer.slice(0));
        this.isConstantToShader = source.isConstantToShader;
        this.numComponents = source.numComponents;
        this.sizeofStride = source.sizeofStride;
        this.offsetofStride = source.offsetofStride;
        this.normalized = source.normalized;
    }

    clone():AttributeInfo{
        let ret:AttributeInfo = <AttributeInfo>{};
        ret.copy(this);
        return ret;
    }

    private _checkBufferType(){

        if (!this.buffer) {
            this.buffertype = undefined;
        }

        if(this.buffer instanceof Float32Array){
            this.buffertype = Float32Array;
            this.glType = EGLDataType.FLOAT;
        }else if (this.buffer instanceof Int8Array) {
            this.buffertype = Int8Array;
            this.glType = EGLDataType.BYTE;
        }else if(this.buffer instanceof Uint8Array){
            this.buffertype = Uint8Array;
            this.glType = EGLDataType.UNSIGNED_BYTE;
        }else if(this.buffer instanceof Int16Array){
            this.buffertype = Int16Array;
            this.glType = EGLDataType.SHORT;
        }else if(this.buffer instanceof Uint16Array){
            this.buffertype = Uint16Array;
            this.glType = EGLDataType.UNSIGNED_SHORT;
        }else if(this.buffer instanceof Int32Array){
            this.buffertype = Int32Array;
            this.glType = EGLDataType.INT;
        }else if(this.buffer instanceof Uint32Array){
            this.buffertype = Uint32Array;
            this.glType = EGLDataType.UNSIGNED_INT;
        }else if(this.buffer instanceof Float64Array){
            this.buffertype = Float64Array;
            console.warn( 'Geometry Attributes: Unsupported data buffer format: Float64Array.' );
        }else{
            console.warn( 'Geometry Attributes: Invalid buffer data' );
            this.buffertype = undefined;
        }

    }

}

/**
 * provide the uniform format of vertex 
 */
export interface VertexAttribute {
    position?: AttributeInfo;
    normal?: AttributeInfo;
    color?: AttributeInfo;
    texcoord?: AttributeInfo;
    indices?: AttributeInfo;

}

/**
 * the attribute type of vertex format
 */
export type VertexAttrType = "position" | "normal" | "color" | "texcoord" | "indices";


export class Geometry {

    //private _glbufferMap:WeakMap<object,WebGLBuffer>;
    private _vertexInfo: VertexAttribute = {};
    public drawCount:number;
    public drawOffset:number;

    clone():Geometry{
        let ret = new Geometry();
        ret.copy(this);
        return ret;
    }

    copy(source:Geometry){
        this._vertexInfo.color = source.vertexInfo.color;
        this._vertexInfo.indices = source.vertexInfo.indices;
        this._vertexInfo.normal = source.vertexInfo.normal;
        this._vertexInfo.position = source.vertexInfo.position;
        this._vertexInfo.texcoord = source.vertexInfo.texcoord;
    }

    setVertexAttribute(name: VertexAttrType, options: AttributeOptions) {

        let attribute = new AttributeInfo(<string>name,options);

        // if ( name === "position" && !this._vertexInfo.indices ) {
        //     let componentCount = attribute.buffer.length / attribute.numComponents;
        //     this.drawCount = componentCount;
        //     this.drawOffset = 0;
        //     console.log(`position drawCount: ${this.drawCount} | ${this.drawOffset}`);
        // }

        // if ( name === "indices" ) {
        //     let componentCount = attribute.buffer.length / attribute.numComponents;
        //     this.drawCount = componentCount;
        //     this.drawOffset = 0;
        //     console.log(`indices drawCount: ${this.drawCount} | ${this.drawOffset}`);
        // }
        
        // let attribute = {
        //     name: name,
        //     buffer: options.buffer,
        //     isConstantToShader: false,
        //     numComponents: options.numComponents,
        //     sizeofStride: options.sizeofStride,
        //     offsetofStride: options.offsetofStride,
        //     normalized: options.normalized || false,
        // };

        // //one TypedArray buffer map to one webglbuffer
        // if (!attribute.glBuffer) {
        //     if (!this._glbufferMap) {
        //         this._glbufferMap = new WeakMap<object,WebGLBuffer>();
        //     }
        //     if (!this._glbufferMap.has(options.buffer)) {
        //         attribute.glBuffer = this._gl.createBuffer();
        //         this._gl.bindBuffer(this._gl.ARRAY_BUFFER, attribute.glBuffer);
        //         this._gl.bufferData(this._gl.ARRAY_BUFFER, attribute.buffer, this._gl.STATIC_DRAW);
        //         this._glbufferMap.set(attribute.buffer,attribute.glBuffer);
        //         this._gl.bindBuffer(this._gl.ARRAY_BUFFER, null);
        //     }else{
        //         attribute.glBuffer = this._glbufferMap.get(options.buffer);
        //     }

        // }

        this._vertexInfo[name] = attribute;

    }


    get vertexInfo(): VertexAttribute {
        return this._vertexInfo;
    }

}