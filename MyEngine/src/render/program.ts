import { AttributeInfo } from "./geometry"
import { EGLDataType } from "./gldatatype"


enum EShaderType {
    VERTEX_SHADER = 0,
    FRAGMENT_SHADER
}

/**
 * program info for create program
 * property 'program' will setting value when call Program.createProgram()
 */
export class ProgramInfo {

    vertexShaderSource: string;
    fragmentShaderSource: string;
    program?: Program;

    constructor(vsSrc: string, fsSrc: string) {
        this.vertexShaderSource = vsSrc;
        this.fragmentShaderSource = fsSrc;
    }

}


export class Program {

    static programIDCount: number = 0;
    id: string;
    name: string;
    usedTimes: number = 0;
    glProgram: WebGLProgram;
    glVertexShader: WebGLShader;
    glFragmentShader: WebGLShader;
    uniforms: Uniforms;
    attributes: Attributes;

    constructor(gl: WebGLRenderingContext, id: string, name: string, program: WebGLProgram,
        vertexShader: WebGLShader, fragmentShader: WebGLShader) {
        this.id = id;
        this.name = name;
        this.glProgram = program;
        this.glVertexShader = vertexShader;
        this.glFragmentShader = fragmentShader;
        this.uniforms = new Uniforms(gl, program);
        this.attributes = new Attributes(gl, program);

        console.log("program bingo!" + this.uniforms.map);
    }

    static createProgram(gl: WebGLRenderingContext,
        programInfo: ProgramInfo): Program {
        console.log("create vs");
        let vertexShader: WebGLShader = Program._createShader(gl,
            EShaderType.VERTEX_SHADER, programInfo.vertexShaderSource);
        console.log("create fs");
        let fragmentShader: WebGLShader = Program._createShader(gl,
            EShaderType.FRAGMENT_SHADER, programInfo.fragmentShaderSource);
        let program: WebGLProgram = Program._createProgram(gl, [vertexShader, fragmentShader]);
        let ret = new Program(gl, "", "", program, vertexShader, fragmentShader);
        programInfo.program = ret;

        return ret
    }

    private static _createShader(gl: WebGLRenderingContext,
        type: EShaderType,
        source: string): WebGLShader {
        let shader: WebGLShader;
        if (type === EShaderType.VERTEX_SHADER) {
            shader = gl.createShader(gl.VERTEX_SHADER);
        } else if (type === EShaderType.FRAGMENT_SHADER) {
            shader = gl.createShader(gl.FRAGMENT_SHADER);
        }

        if (!shader) {
            console.error("Create shader false with invalid shader type ! ")
            return null;
        }

        gl.shaderSource(shader, source);
        gl.compileShader(shader);

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            console.error('error occured compiling the shaders:' + gl.getShaderInfoLog(shader));
            gl.deleteShader(shader);
            return null;
        }

        return shader;

    }

    private static _createProgram(gl: WebGLRenderingContext,
        shaders: WebGLShader[]): WebGLProgram {

        //create program and attach shaders
        const program = gl.createProgram();
        shaders.forEach((shader) => {
            gl.attachShader(program, shader);
        });
        gl.linkProgram(program);

        //check the link status
        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            const lastError = gl.getProgramInfoLog(program);
            console.log("Error in program linking:" + lastError);
            gl.deleteProgram(program);
            return null;
        }

        return program;

    }

}


/**
 * it is used for making distinction
 */
export enum ESetterType {
    NormalSetter,
    SamplerSetter,
}

/**
 * Setter interface 
 * provide a common interface to set the value of uniform that any type.
 */
abstract class Setter {
    type: ESetterType = ESetterType.NormalSetter;
    cache: any;
    abstract setValue(gl: WebGLRenderingContext, addr: any, value: any, param?: any): void;

}


class Setter1f extends Setter {

    setValue(gl: WebGLRenderingContext, addr: any, value: any): void {
        gl.uniform1f(addr, value);
    }
}


class Setter1fv extends Setter {

    setValue(gl: WebGLRenderingContext, addr: any, value: any): void {
        gl.uniform1fv(addr, value);
    }
}

class Setter2fv extends Setter {

    setValue(gl: WebGLRenderingContext, addr: any, value: any): void {
        gl.uniform2fv(addr, value);
    }
}

class Setter3fv extends Setter {

    setValue(gl: WebGLRenderingContext, addr: any, value: any): void {
        gl.uniform3fv(addr, value);
    }
}

class Setter4fv extends Setter {

    setValue(gl: WebGLRenderingContext, addr: any, value: any): void {
        gl.uniform4fv(addr, value);
    }
}

class Setter1i extends Setter {

    setValue(gl: WebGLRenderingContext, addr: any, value: any): void {
        gl.uniform1i(addr, value);
    }
}

class Setter1iv extends Setter {

    setValue(gl: WebGLRenderingContext, addr: any, value: any): void {
        gl.uniform1iv(addr, value);
    }
}

class Setter2iv extends Setter {

    setValue(gl: WebGLRenderingContext, addr: any, value: any): void {
        gl.uniform2iv(addr, value);
    }
}

class Setter3iv extends Setter {

    setValue(gl: WebGLRenderingContext, addr: any, value: any): void {
        gl.uniform3iv(addr, value);
    }
}


class Setter4iv extends Setter {

    setValue(gl: WebGLRenderingContext, addr: any, value: any): void {
        gl.uniform4iv(addr, value);
    }
}


class Setter1ui extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniform1ui(addr, value);
    }
}

class Setter1uiv extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniform1uiv(addr, value);
    }
}

class Setter2uiv extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniform2uiv(addr, value);
    }
}

class Setter3uiv extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniform3uiv(addr, value);
    }
}


class Setter4uiv extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniform4uiv(addr, value);
    }
}


class SetterMatrix2fv extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniformMatrix2fv(addr, false, value);
    }
}

class SetterMatrix3fv extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniformMatrix3fv(addr, false, value);
    }
}


class SetterMatrix4fv extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniformMatrix4fv(addr, false, value);
    }
}

class SetterMatrix23f extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniformMatrix2x3fv(addr, false, value);
    }
}

class SetterMatrix32f extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniformMatrix3x2fv(addr, false, value);
    }
}


class SetterMatrix24f extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniformMatrix3x2fv(addr, false, value);
    }
}


class SetterMatrix42f extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniformMatrix4x2fv(addr, false, value);
    }
}

class SetterMatrix34f extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniformMatrix3x4fv(addr, false, value);
    }
}


class SetterMatrix43f extends Setter {

    setValue(gl: any, addr: any, value: any): void {
        gl.uniformMatrix4x3fv(addr, false, value);
    }
}


class SetterSampler extends Setter {

    public type: ESetterType = ESetterType.SamplerSetter;

    constructor(
        public bindPoint: number
    ) {
        super();
    }
    setValue(gl: any, addr: any, value: any , param:any): void {
        let textureUnit:number = <number>param || 0;
        //console.log("set sampler:" + textureUnit);
        gl.activeTexture(gl.TEXTURE0 + textureUnit);
        gl.bindTexture(this.bindPoint, value);
        gl.uniform1i(addr, textureUnit);

    }
}


/**
 * the uniforms manager for a program
 */
class Uniforms {

    static typeMap: { [key: number]: Setter } = {
        [EGLDataType.FLOAT]: new Setter1f(),
        [EGLDataType.FLOAT_VEC2]: new Setter2fv(),
        [EGLDataType.FLOAT_VEC3]: new Setter3fv(),
        [EGLDataType.FLOAT_VEC4]: new Setter4fv(),
        [EGLDataType.INT]: new Setter1i(),
        [EGLDataType.INT_VEC2]: new Setter2iv(),
        [EGLDataType.INT_VEC3]: new Setter3iv(),
        [EGLDataType.INT_VEC4]: new Setter4iv(),
        [EGLDataType.UNSIGNED_INT]: new Setter1ui(),
        [EGLDataType.UNSIGNED_INT_VEC2]: new Setter2uiv(),
        [EGLDataType.UNSIGNED_INT_VEC3]: new Setter3uiv(),
        [EGLDataType.UNSIGNED_INT_VEC4]: new Setter4uiv(),
        [EGLDataType.BOOL]: new Setter1ui(),
        [EGLDataType.BOOL_VEC2]: new Setter2uiv(),
        [EGLDataType.BOOL_VEC3]: new Setter3uiv(),
        [EGLDataType.BOOL_VEC4]: new Setter4uiv(),
        [EGLDataType.FLOAT_MAT2]: new SetterMatrix2fv(),
        [EGLDataType.FLOAT_MAT3]: new SetterMatrix3fv(),
        [EGLDataType.FLOAT_MAT4]: new SetterMatrix4fv(),
        [EGLDataType.FLOAT_MAT2x3]: new SetterMatrix23f(),
        [EGLDataType.FLOAT_MAT2x4]: new SetterMatrix24f(),
        [EGLDataType.FLOAT_MAT3x2]: new SetterMatrix32f(),
        [EGLDataType.FLOAT_MAT3x4]: new SetterMatrix34f(),
        [EGLDataType.FLOAT_MAT4x2]: new SetterMatrix42f(),
        [EGLDataType.FLOAT_MAT4x3]: new SetterMatrix43f(),

        [EGLDataType.SAMPLER_2D]: new SetterSampler(EGLDataType.TEXTURE_2D),
        [EGLDataType.SAMPLER_CUBE]: new SetterSampler(EGLDataType.TEXTURE_CUBE_MAP),
        [EGLDataType.SAMPLER_3D]: new SetterSampler(EGLDataType.TEXTURE_3D),
        [EGLDataType.SAMPLER_2D_SHADOW]: new SetterSampler(EGLDataType.TEXTURE_2D),
        [EGLDataType.SAMPLER_2D_ARRAY]: new SetterSampler(EGLDataType.TEXTURE_2D_ARRAY),
        [EGLDataType.SAMPLER_2D_ARRAY_SHADOW]: new SetterSampler(EGLDataType.TEXTURE_2D_ARRAY),

        [EGLDataType.INT_SAMPLER_2D]: new SetterSampler(EGLDataType.TEXTURE_2D),
        [EGLDataType.INT_SAMPLER_3D]: new SetterSampler(EGLDataType.TEXTURE_3D),
        [EGLDataType.INT_SAMPLER_CUBE]: new SetterSampler(EGLDataType.TEXTURE_CUBE_MAP),
        [EGLDataType.INT_SAMPLER_2D_ARRAY]: new SetterSampler(EGLDataType.TEXTURE_2D_ARRAY),

        [EGLDataType.UNSIGNED_INT_SAMPLER_2D]: new SetterSampler(EGLDataType.TEXTURE_2D),
        [EGLDataType.UNSIGNED_INT_SAMPLER_3D]: new SetterSampler(EGLDataType.TEXTURE_3D),
        [EGLDataType.UNSIGNED_INT_SAMPLER_CUBE]: new SetterSampler(EGLDataType.TEXTURE_CUBE_MAP),
        [EGLDataType.UNSIGNED_INT_SAMPLER_2D_ARRAY]: new SetterSampler(EGLDataType.TEXTURE_2D_ARRAY),

    }

    /**
     * uniforms map by name
     */
    map: { [key: string]: Uniform } = {};

    /**
     * uniforms seq
     */
    seq: Uniform[] = [];

    /**
     * the sampler uniform count in this uniforms
     */
    private _textureCount: number = 0;

    constructor(gl: WebGLRenderingContext, program: WebGLProgram) {
        let n = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);

        for (let i = 0; i < n; ++i) {

            let info: WebGLActiveInfo = gl.getActiveUniform(program, i);
            let addr: WebGLUniformLocation = gl.getUniformLocation(program, info.name);

            this._parseUniform(info, addr);
            console.log("parse uniform:" + info.name + "|" + addr);

        }
    }

    _parseUniform(info: WebGLActiveInfo, addr: WebGLUniformLocation) {
        var id = <any>info.name | 0;
        this._addUniform(id, info.name, addr, info.type);
        console.log("add uniform:" + info.name + " | " + info.type);
    }

    _addUniform(id: number, name: string, addr: any, uniformType: any) {
        let uniform = new Uniform(id, addr, Uniforms.typeMap[uniformType]);
        if (uniform.setter.type === ESetterType.SamplerSetter) {
            //the sampler setter use this textureunit
            uniform.textureUnit = this._textureCount;
            this._textureCount++;
        }
        this.map[name] = uniform;
        this.seq.push(uniform);

    }

}

export class Uniform {
    id: number;
    addr: number;
    setter: Setter;
    /**
     * if this is a sampler uniform this property is not null the otherwise it is null
     */
    textureUnit: number;

    constructor(id: number, addr: number, setter: Setter) {
        this.id = id;
        this.addr = addr;
        this.setter = setter;
    }

}



class AttribFloatSetter extends Setter {
    setValue(gl: any, addr: any, value: any): void {
        let attrib = <AttributeInfo>value;
        if (attrib.isConstantToShader === true) {
            gl.disableVertexAttribArray(addr);
            let setValue = attrib.buffer;
            switch (setValue.length) {
                case 4:
                    gl.vertexAttrib4fv(addr, setValue.buffer);
                    break;
                case 3:
                    gl.vertexAttrib3fv(addr, setValue.buffer);
                    break;
                case 2:
                    gl.vertexAttrib2fv(addr, setValue.buffer);
                    break;
                case 1:
                    gl.vertexAttrib1fv(addr, setValue.buffer);
                    break;
                default:
                    throw new Error('the length of a float constant value must be between 1 and 4!');
            }
        } else {
            if (!attrib.glBuffer) {
                //console.log("no glbuffer:"+ attrib.buffer.length);
                attrib.glBuffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                gl.bufferData(gl.ARRAY_BUFFER, attrib.buffer, gl.STATIC_DRAW);
            } else {
                //console.log("has glbuffer");
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
            }

            gl.enableVertexAttribArray(addr);
            //console.log(`attr [${attrib.name}]:  ${ attrib.numComponents} | ${ attrib.sizeofStride} | ${ attrib.offsetofStride}`)
            gl.vertexAttribPointer(addr, attrib.numComponents, gl.FLOAT,
                attrib.normalized || false, attrib.sizeofStride || 0, attrib.offsetofStride || 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }

    }
}


class AttribIntSetter extends Setter {
    setValue(gl: any, addr: any, value: any): void {
        let attrib = <AttributeInfo>value;
        if (attrib.isConstantToShader !== true) {
            if (!attrib.glBuffer) {
                attrib.glBuffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                gl.bufferData(gl.ARRAY_BUFFER, attrib.buffer, gl.STATIC_DRAW);
            } else {
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
            }

            gl.enableVertexAttribArray(addr);
            gl.vertexAttribIPointer(addr, attrib.numComponents,
                attrib.glType || gl.INT, attrib.sizeofStride || 0, attrib.offsetofStride || 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }
    }
}

class AttribUIntSetter extends Setter {
    setValue(gl: any, addr: any, value: any): void {
        let attrib = <AttributeInfo>value;
        if (attrib.isConstantToShader !== true) {
            if (!attrib.glBuffer) {
                attrib.glBuffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                gl.bufferData(gl.ARRAY_BUFFER, attrib.buffer, gl.STATIC_DRAW);
            } else {
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
            }

            gl.enableVertexAttribArray(addr);
            gl.vertexAttribIPointer(addr, attrib.numComponents,
                attrib.glType || gl.UNSIGNED_INT, attrib.sizeofStride || 0, attrib.offsetofStride || 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }
    }
}


export class Attribute {
    id: number;
    addr: number;
    setter: Setter;
    buffer: WebGLBuffer;
    type: EGLDataType;

    constructor(id: number, addr: number, type: EGLDataType) {
        this.id = id;
        this.addr = addr;
        this.setter = Attributes.typeMap[type];
        this.type = type;
    }
}

export class Attributes {

    static typeMap: { [key: number]: Setter } = {
        [EGLDataType.FLOAT]: new AttribFloatSetter(),
        [EGLDataType.FLOAT_VEC2]: new AttribFloatSetter(),
        [EGLDataType.FLOAT_VEC3]: new AttribFloatSetter(),
        [EGLDataType.FLOAT_VEC4]: new AttribFloatSetter(),
        [EGLDataType.INT]: new AttribIntSetter(),
        [EGLDataType.INT_VEC2]: new AttribIntSetter(),
        [EGLDataType.INT_VEC3]: new AttribIntSetter(),
        [EGLDataType.INT_VEC4]: new AttribIntSetter(),
        [EGLDataType.UNSIGNED_INT]: new AttribUIntSetter(),
        [EGLDataType.UNSIGNED_INT_VEC2]: new AttribUIntSetter(),
        [EGLDataType.UNSIGNED_INT_VEC3]: new AttribUIntSetter(),
        [EGLDataType.UNSIGNED_INT_VEC4]: new AttribUIntSetter(),
        [EGLDataType.BOOL]: new AttribIntSetter(),
        [EGLDataType.BOOL_VEC2]: new AttribIntSetter(),
        [EGLDataType.BOOL_VEC3]: new AttribIntSetter(),
        [EGLDataType.BOOL_VEC4]: new AttribIntSetter(),
        // [GLDataType.FLOAT_MAT2]:new Setter1f(),
        // [GLDataType.FLOAT_MAT3]:new Setter1f(),
        // [GLDataType.FLOAT_MAT4]:new Setter1f(),
    }

    /**
     * attributes map by name
     */
    map: { [key: string]: Attribute } = {};

    /**
     * attributes seq
     */
    seq: Attribute[] = [];

    /**
     * check is BuiltIn variables return boolean
     * @param info 
     * @returns boolean
     */
    _isBuiltIn(info: WebGLActiveInfo) {
        const name = info.name;
        return name.startsWith("gl_") || name.startsWith("webgl_");
    }

    constructor(gl: WebGLRenderingContext, program: WebGLProgram) {
        const numAttribs = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);
        for (let i = 0; i < numAttribs; ++i) {
            const attribInfo: WebGLActiveInfo = gl.getActiveAttrib(program, i);
            if (this._isBuiltIn(attribInfo)) {
                continue;
            }

            const index = gl.getAttribLocation(program, attribInfo.name);
            const id = <any>attribInfo.name | 0;
            const attribute = new Attribute(id, index, attribInfo.type);
            this.map[attribInfo.name] = attribute;
            this.seq.push(attribute);
            console.log("parse attribute:" + attribInfo.name + "|" + index);
        }
    }


}







