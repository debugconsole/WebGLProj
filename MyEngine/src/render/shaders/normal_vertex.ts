export default /* glsl */`
attribute vec4 position;
uniform mat4 transform;
void main() {
    gl_Position = transform * position;
}
`