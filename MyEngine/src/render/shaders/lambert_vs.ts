export default /* glsl */`
attribute vec4 position;
attribute vec4 normal;
uniform mat4 transform;
uniform vec3 lightColor;        //入射光颜色
uniform vec3 lightDir;          //入射光方向
uniform vec3 lightColorAmbient; // 环境光颜色
varying vec4 fragColor;         //这里作为顶点光源计算颜色输出到fs

void main() {
    gl_Position = transform * position;
    vec4 aColor = vec4(1.0,1.0,1.0,1.0);
    //lambert光照模型计算
    vec3 normalizeNormal = normalize(vec3(normal));// 归一化法向量
    float cos = max(dot(lightDir, normalizeNormal), 0.0);// 计算入射角余弦值
    vec3 diffuse = lightColor * vec3(aColor) * cos;// 计算漫反射颜色
    vec3 ambient = lightColorAmbient * aColor.rgb;// 计算环境光反射颜色
    fragColor = vec4(diffuse + ambient, aColor.a); // outColor = ambient + diffuse
}
`