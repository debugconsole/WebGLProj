
import {VertexAttribute} from "./geometry"
import {GLDrawType,GLBingBufferTarget,TypedArray} from "./gldatatype"


export class GLContext {

    static gl:WebGLRenderingContext;
    static canvas:HTMLCanvasElement;
    /**
     * init glContext object,it'a global object
     * @param cavID 
     */
    static init(cavID: string) {
        const canvas: HTMLCanvasElement = <HTMLCanvasElement>document.querySelector(`#${cavID}`);
        GLContext.canvas = canvas;
        GLContext.gl = canvas.getContext('webgl');
    }

    /**
     * Bind glbuffer
     * @param glTarget 
     * @param buff 
     * @param arrayData 
     * @param drawtype 
     */
    static glBindBuffer(
        gl:WebGLRenderingContext,
        glTarget: GLBingBufferTarget,
        glBuffer: WebGLBuffer,
        typedArray: TypedArray,
        drawtype: GLDrawType
    ): void {
        let bufferType = (glTarget === GLBingBufferTarget.ARRAY_BUFFER)?gl.ARRAY_BUFFER:gl.ELEMENT_ARRAY_BUFFER;
        gl.bindBuffer(bufferType, glBuffer);
        gl.bufferData(bufferType, typedArray, drawtype);
    }

    /**
     * create glBuffer from TypeArray
     * @param type          ARRAY_BUFFER | ELEMENT_ARRAY_BUFFER
     * @param typedArray    TypedArray
     * @param drawtype      (optional)GLDrawType
     */
    static createBufferFromTypedArray(
        gl:WebGLRenderingContext,
        type: GLBingBufferTarget,
        typedArray: TypedArray,
        drawtype?: GLDrawType
    ) {
        const glBuffer = gl.createBuffer();  
        GLContext.bindBuffer(gl,glBuffer,type,typedArray, drawtype);
        return glBuffer;
    }


    static bindBuffer(  
        gl:WebGLRenderingContext,
        glBuffer:any,
        type: GLBingBufferTarget,
        typedArray: TypedArray,
        drawtype?: GLDrawType){

        let glDrawType = gl.STATIC_DRAW;
        if (drawtype === GLDrawType.DYNAMIC_DRAW) {
            glDrawType = gl.DYNAMIC_DRAW;
        }if (drawtype === GLDrawType.STREAM_DRAW){
            glDrawType = gl.STREAM_DRAW;
        }
        GLContext.glBindBuffer(gl,type, glBuffer, typedArray, glDrawType);
    }


    /**
     * createWebGLBuffersFromVertexInfo
     * @param vertexInfo 
     */
    static bindIBOFromVertexInfo(vertexInfo: VertexAttribute ) {
        let vertexStruct: any = vertexInfo;
        let gl = GLContext.gl;

        if(vertexInfo.hasOwnProperty("indices")){
            let indicesProperty = vertexStruct["indices"];
            if(!indicesProperty.glBuffer){
                //console.log("indices glBuffer Create !!!");
                indicesProperty.glBuffer = 
                    GLContext.createBufferFromTypedArray(gl,gl.ELEMENT_ARRAY_BUFFER,indicesProperty.buffer);
            }else{
                GLContext.bindBuffer(gl,indicesProperty.glBuffer,gl.ELEMENT_ARRAY_BUFFER, indicesProperty.buffer, gl.STATIC_DRAW);
            }
            
        }

        // Object.keys(vertexStruct).forEach((component) => {
        //     const type = component === "indices" ? gl.ELEMENT_ARRAY_BUFFER : gl.ARRAY_BUFFER;
        //     vertexStruct[component].glBuffer = GLContext.createBufferFromTypedArray(gl,type, vertexStruct[component]);
        // })
    }


}


