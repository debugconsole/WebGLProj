import { RenderGroup, RenderList, DrawableObj, EDrawType } from "./drawobject"
import { ProgramInfo, Program, Attribute, Uniform , ESetterType } from "./program"
import { VertexAttribute } from "./geometry"
import { Vector4 } from "../math/vector4"
import { State,EEnabledCapability } from "./renderstate"
import { Helper } from "../utils/helper"

/**
 * Defined render group types for sorting render objs
 */
export enum ERenderGroupType {
    BackGround,
    Opaque,
    Transparence,
    UILayout,
}

/**
 * Renderer
 */
export class Renderer {


    clearRenderGroup() {
        for (const key in this.renderGroups) {
            if (this.renderGroups.hasOwnProperty(key)) {
                this.renderGroups[key].clear();
            }
        }
    }

    /**
     * add a render obj to the render group
     * @param drawObj 
     * @param renderType 
     */
    addDrawAbleObj(drawObj: DrawableObj, renderType: ERenderGroupType) {

        if (this.renderGroups.hasOwnProperty(renderType)) {
            const renderGroup = this.renderGroups[renderType];
            if (renderGroup.renderListArray.length === 0) {
                renderGroup.renderListArray.push(new RenderList());
            }
            renderGroup.renderListArray[0].renderObjs.push(drawObj);
        }

    }


    calldraw:number = 0;

    clear(){
        
        let gl = this.gl;

        gl.clearColor(0.3, 0.3, 0.3, 1.0);
        gl.clearDepth(1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );
        this.calldraw = 0;
    }


    /**
     * render pipline
     */
    render() {
        let count:number = 0;
        for (const key in this.renderGroups) {
            if (this.renderGroups.hasOwnProperty(key)) {
                const renderGroup = this.renderGroups[key];
                renderGroup.renderListArray.forEach(renderList => {
                    this._drawObjects(renderList);
                    count += renderList.renderObjs.length;
                });
            }
        }

        //console.log(">>drawobj count:" + count);

    }


    /**
     * clear current color buffer
     */
    clearColorBuffer() {
        this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    }

    // renderGroups:RenderGroup[];
    renderGroups: { [key: number]: RenderGroup };
    lastUsedProgramInfo: ProgramInfo;
    gl: WebGLRenderingContext;

    // glviewport setting
    _viewport: Vector4;
    _state: State;

    /**
     * setviewport
     */
    viewport(vp: Vector4) {
        this.gl.viewport(vp.x, vp.y, vp.z, vp.w);
        this._viewport = vp.clone();
    }


    constructor(gl: WebGLRenderingContext) {
        this.gl = gl;
        this._setupRenderGroups();
        this._state = new State(gl);
        this._state.enable(EEnabledCapability.DEPTH_TEST);
        this._state.enable(EEnabledCapability.CULL_FACE);
    }

    /**
     * setup the default rendergroups
     */
    private _setupRenderGroups() {
        this.renderGroups = {};
        this.renderGroups[ERenderGroupType.BackGround] = new RenderGroup(ERenderGroupType.BackGround, 500);
        this.renderGroups[ERenderGroupType.Opaque] = new RenderGroup(ERenderGroupType.Opaque, 400);
        this.renderGroups[ERenderGroupType.Transparence] = new RenderGroup(ERenderGroupType.Transparence, 300);
        this.renderGroups[ERenderGroupType.UILayout] = new RenderGroup(ERenderGroupType.UILayout, 100);
    }


    private _drawObject(drawObj: DrawableObj) {

        //console.log("draw...:" + this.calldraw++);

        if (  !drawObj.vertexInfo && !drawObj.programInfo) {
            throw new Error("there is invalid vertexInfo or programInfo in vertexInfo");
        }
        
        //init drawable buffer
        drawObj.bindIBO();

        //setup program
        if (this.lastUsedProgramInfo !== drawObj.programInfo) {
            if (!drawObj.programInfo.program) {
                console.log("create program");
                Program.createProgram(this.gl, drawObj.programInfo);
            }
            console.log("setup program");
            this.lastUsedProgramInfo = drawObj.programInfo;
            this.gl.useProgram(drawObj.programInfo.program.glProgram);
        }

        let vertexInfo: VertexAttribute = drawObj.vertexInfo;
        let program: Program = drawObj.programInfo.program;

        //do Attribute setters
        for (const key in program.attributes.map) {
            if (program.attributes.map.hasOwnProperty(key)) {
                if (vertexInfo.hasOwnProperty(key)) {
                    //console.log("set attribute :" + key);
                    const attribInfo = (<any>vertexInfo)[key];
                    const attribute: Attribute = program.attributes.map[key];
                    attribute.setter.setValue(this.gl, attribute.addr, attribInfo);
                }else{
                    console.warn("vertexInfo isn't has property:" + key);
                }
            }
        }

        let uniforms = drawObj.uniforms;
        //do Uniforms setters
        for (const key in program.uniforms.map) {
            if (program.uniforms.map.hasOwnProperty(key)) {
                if (uniforms && uniforms.hasOwnProperty(key)) {
                    //console.log("set uniform1 :" + key);
                    const uniformValue = uniforms[key];
                    const uniform: Uniform = program.uniforms.map[key];
                    if (uniform.setter.type === ESetterType.SamplerSetter) {
                        uniform.setter.setValue(this.gl, uniform.addr, uniformValue,uniform.textureUnit);
                    }else{
                        uniform.setter.setValue(this.gl, uniform.addr, uniformValue);
                    }
                    
                }else{
                    console.warn("drawobj uniforms isn't has:" + key);
                }
            }
        }

        //begin draw--------------------------
        // this.gl.clearColor(0.3, 0.3, 0.3, 1.0);
        // this.gl.clear(this.gl.COLOR_BUFFER_BIT);

        let glDrawType = this.gl.TRIANGLES;

        if (drawObj.drawType === EDrawType.Points) {
            glDrawType = this.gl.POINTS;
        } else if (drawObj.drawType === EDrawType.Lines) {
            glDrawType = this.gl.LINES;
        }

        //console.log("[draw debug1]:" + drawObj.vertexInfo.position.buffer);
        if (!drawObj.vertexInfo.indices) { 
            //console.log("[drawArrays2:]" + drawObj.drawType + "|" + drawObj.drawArrayOffset + "|" + drawObj.drawArrayCount);
            this.gl.drawArrays(glDrawType, drawObj.drawArrayOffset, drawObj.drawArrayCount);
        } else {
            //console.log("[drawelements2:]" + drawObj.drawType + "|" + drawObj.drawArrayOffset + "|" 
            //            + drawObj.drawArrayCount + " | " + drawObj.vertexInfo.indices.glType);
            this.gl.drawElements(glDrawType, drawObj.drawArrayCount, drawObj.vertexInfo.indices.glType|this.gl.UNSIGNED_BYTE, drawObj.drawArrayOffset);
        }

        //end draw--------------------------

    }

    private _drawObjects(renderList: RenderList) {
        Helper.calculateTimestamp(()=>{
            renderList.renderObjs.forEach(renderObj => {
                this._drawObject(renderObj);
            });
        },"2");

    }




}