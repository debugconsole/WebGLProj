import { NumberMap } from "../types"



export enum EEnabledCapability {
    BLEND,
    CULL_FACE,
    DEPTH_TEST,
    DITHER,
    POLYGON_OFFSET_FILL,
    SAMPLE_ALPHA_TO_COVERAGE,
    SAMPLE_COVERAGE,
    SCISSOR_TEST,
    STENCIL_TEST
}

export class State {

    constructor(
        public gl: WebGLRenderingContext) {
        this._initEnabledCapabilitieConvertor();
    }

    _initEnabledCapabilitieConvertor() {

        if (this._enabledCapabilitieNameMap) {
            return;
        }

        this._enabledCapabilitieNameMap = {};

        let keys = Object.keys(EEnabledCapability);
        keys.forEach(key => {
            let keynum = (<any>EEnabledCapability)[key];
            switch (keynum) {
                case EEnabledCapability.BLEND:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.BLEND;
                    break;
                case EEnabledCapability.CULL_FACE:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.CULL_FACE;
                    break;
                case EEnabledCapability.DEPTH_TEST:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.DEPTH_TEST;
                    break;
                case EEnabledCapability.DITHER:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.DITHER;
                    break;
                case EEnabledCapability.POLYGON_OFFSET_FILL:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.POLYGON_OFFSET_FILL;
                    break;
                case EEnabledCapability.SAMPLE_ALPHA_TO_COVERAGE:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.SAMPLE_ALPHA_TO_COVERAGE;
                    break;
                case EEnabledCapability.SAMPLE_COVERAGE:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.SAMPLE_COVERAGE;
                    break;
                case EEnabledCapability.SCISSOR_TEST:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.SCISSOR_TEST;
                    break;
                case EEnabledCapability.STENCIL_TEST:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.STENCIL_TEST;
                    break;
            }

        });

        //console.log("EnableCapabilities:" + JSON.stringify(this._enabledCapabilitieNameMap));
    }

    private _enabledCapabilitieNameMap: NumberMap<number>;
    enabledCapabilities: NumberMap<boolean> = {};

    enable(id: EEnabledCapability) {

        let glKey = this._enabledCapabilitieNameMap[id];
        if (this.enabledCapabilities[glKey] !== true) {

            this.gl.enable(glKey);
            this.enabledCapabilities[glKey] = true;

        }

    }

    disable(id: EEnabledCapability) {

        let glKey = this._enabledCapabilitieNameMap[id];
        if (this.enabledCapabilities[glKey] !== false) {

            this.gl.disable(glKey);
            this.enabledCapabilities[glKey] = false;

        }

    }

}