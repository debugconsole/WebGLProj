import { VertexAttribute } from "./geometry"
import { ProgramInfo } from "./program"
import { GLContext } from "./glcontext";

/**
 * Draw type map to the type of opengl draw
 */
export enum EDrawType{
    Points=0,
    Lines,
    Lines_Loop,
    Lines_Strip,
    Triangles,
    Triangles_Strip,
    Triangle_Fan,
}

/**
 * the drawable object
 */
export class DrawableObj {


    bindIBO(){
        GLContext.bindIBOFromVertexInfo(this.vertexInfo);
    }


    drawType:EDrawType;
    
    /**
     * the offset pass to gl.drawArray or gl.drawElements. Dafaults to 0
     */
    drawArrayOffset:number;

    /**
     * the count pass to gl.drawArray or gl.drawElements. Dafaults to 
     */
    drawArrayCount:number;

    vertexInfo: VertexAttribute;
    programInfo: ProgramInfo;
    uniforms:{[key:string]:number[]|number}={};
    onBeforeDraw: (obj: DrawableObj) => void;
    onAfterDraw: (obj: DrawableObj) => void;

    
}

/**
 * struct of render list
 */
export class RenderList {
    renderObjs: DrawableObj[] = [];
}

/**
 * struct of render group
 */
export class RenderGroup {
    id: number;
    priority: number;
    renderListArray: RenderList[];

    clear(){
        this.renderListArray = [];
    }

    constructor(id:number,priority:number){
        this.id = id;
        this.priority = priority;
        this.renderListArray = [];
    }
    
}

