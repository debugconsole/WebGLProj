import { DrawableObj, EDrawType, RenderList, RenderGroup } from "./render/drawobject"
import { Geometry } from "./render/geometry"
import { GLContext } from "./render/glcontext"
import { ProgramInfo } from "./render/program"
import { Renderer, ERenderGroupType } from "./render/renderer"
import { ResourceManager, MapObj } from "./core/resourcemgr"
import { Matrix4 } from "./math/matrix4"
import { Vector3 } from "./math/vector3"
import { Vector4 } from "./math/vector4"
import { Scene } from "./core/scene"
import { SceneNode } from "./core/scenenode"
import { Entity } from "./core/entity"
import { Engine } from "./core/engine"
import { Viewer } from "./core/viewer"
import { Camera } from "./core/camera"
import { PerspectiveCamera } from "./core/camera"
import { RenderTarget } from "./core/rendertarget"
import { BoxGeometry } from "./geometries/boxgeometry"
import { Texture } from "./core/texture"

import { GLTFLoader } from "./thirdpart/loaders/gltfloader"


let Mirror = {
    ['DrawableObj']: DrawableObj,
    ['EDrawType']: EDrawType,
    ['RenderList']: RenderList,
    ['RenderGroup']: RenderGroup,
    ['Geometry']: Geometry,
    ['GLContext']: GLContext,
    ['ProgramInfo']: ProgramInfo,
    ['Renderer']: Renderer,
    ['ERenderGroupType']: ERenderGroupType,
    ['ResourceManager']: ResourceManager,
    ['Matrix4']: Matrix4,
    ['Vector3']: Vector3,
    ['Vector4']: Vector4,
    ['Scene']: Scene,
    ['SceneNode']: SceneNode,
    ['Entity']: Entity,
    ['Engine']: Engine,
    ['Camera']: Camera,
    ['PerspectiveCamera']: PerspectiveCamera,
    ['Viewer']: Viewer,
    ['BoxGeometry']:BoxGeometry,
    ['Texture']:Texture,
    ['GLTFLoader']:GLTFLoader,
    ['RenderTarget']:RenderTarget
}

export default Mirror;


    // constructor(){
    //     this.init();
    //     Mirror2
    // }

    // requestUpdateFrame = window.requestAnimationFrame;

    // init() {

    //     // look for vendor prefixed function
    //     if (!defined(this.requestUpdateFrame)) {
    //         var vendors = ['webkit', 'moz', 'ms', 'o'];
    //         var i = 0;
    //         var len = vendors.length;
    //         while (i < len && !defined(requestUpdateFrame)) {
    //             requestUpdateFrame = window[vendors[i] + 'RequestAnimationFrame'];
    //             ++i;
    //         }
    //     }

    //     // build an requestUpdateFrame based on setTimeout
    //     if (!defined(requestUpdateFrame)) {
    //         var msPerFrame = 1000.0 / 60.0;
    //         var lastFrameTime = 0;
    //         requestUpdateFrame = function(callback) {
    //             var currentTime = getTimestamp();

    //             // schedule the callback to target 60fps, 16.7ms per frame,
    //             // accounting for the time taken by the callback
    //             var delay = Math.max(msPerFrame - (currentTime - lastFrameTime), 0);
    //             lastFrameTime = currentTime + delay;

    //             return setTimeout(function() {
    //                 callback(lastFrameTime);
    //             }, delay);
    //         };
    //     }
    // }

    // //开始渲染循环
    // startRenderLoop(){
    //     window.requestUpdateFrame(renderMain);
    // }

    // //渲染入口递归调用requestUpdateFrame刷新
    // renderMain(timestamp){
    //     renderAFrame(timestamp)
    //     //用于在下一帧页面刷新前调用
    //     requestUpdateFrame(renderMain);
    // }
