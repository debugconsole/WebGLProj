import {ResourceManager,MapObj} from "./core/resourcemgr"
import WebGLInstaller from "./core/webglinstaller"

import {GLContext} from "./render/glcontext"
import {DrawableObj, EDrawType} from "./render/drawobject"
import {ProgramInfo} from "./render/program"
import {Renderer, ERenderGroupType} from "./render/renderer"
import {Geometry} from "./render/geometry"

export let main=()=>{

    console.log("main run ...");
    
    GLContext.init('canvas');
    let gl = GLContext.gl;
    let render: Renderer = new Renderer(gl);

    let vs = "./shaders/test1_v.glsl";
    let fs = "./shaders/test1_f.glsl";
    let shaders = [vs,fs];

    //load resource
    ResourceManager.getSingleton().loadShaders(shaders).then( shadercache =>{
        let shaderMap = <MapObj>shadercache;
        console.log("===>" + JSON.stringify(shaderMap));

        let drawObj = new DrawableObj;
        drawObj.drawType = EDrawType.Triangles;
        //set program infomation
        drawObj.programInfo =  new ProgramInfo(shaderMap[vs],shaderMap[fs]);

        //set geometry infomation
        let vertices1 = new Float32Array([
            -0.5, -0.5,
            0, 0.5,
            0.5, -0.5
        ]);
        let geometry = new Geometry();
        geometry.setVertexAttribute("position",{
            buffer:vertices1,
            numComponents:2,
            sizeofStride:8,
            offsetofStride:0
        });
        drawObj.vertexInfo = geometry.vertexInfo;
        drawObj.drawArrayOffset = 0;
        drawObj.drawArrayCount = 3;

        render.addDrawAbleObj(drawObj,ERenderGroupType.Opaque);

        console.log("begin draw ..... " );
        //draw
        render.render();

        console.log("end draw ..... " );

    })
    
    console.log("main done!");




    


}


let drawPointsSample = () =>{
    
    //初始化GLContext
    GLContext.init('canvas');

}



/**
 * 使用简单封装WebGLInstaller绘制三个点例子
 */
let testWebglConstant = ()=>{

    let webgl:WebGLInstaller = new WebGLInstaller();


    let vs = "./shaders/test1_v.glsl";
    let fs = "./shaders/test1_f.glsl";
    let shaders = [vs,fs];
    //load resource
    ResourceManager.getSingleton().loadShaders(shaders).then( shadercache =>{
        let shaderMap = <MapObj>shadercache;
        console.log("===>" + JSON.stringify(shaderMap));
        if(webgl.setupProgramFromScript(shaderMap[vs],shaderMap[fs])){
            let vertices1 = new Float32Array([
                -0.5, -0.5,
                0, 0.5,
                0.5, -0.5
            ]);
            console.log("draw1 .... ");
            webgl.drawBuffer(vertices1);
            console.log("draw over .... ");
        }

    })
    
    console.log("main done!");
}
