export class Helper{

    static calculateTimestamp(callback:any,flag?:string){
        let start = performance.now();
        callback();
        let diffMillSeconds = performance.now() - start;
        flag = flag || "";
        //console.log(`${flag}: a task took ${diffMillSeconds}  milliseconds to execute. `);
    }

}