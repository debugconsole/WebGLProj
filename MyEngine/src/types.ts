export type CallBack1<T> = (param1:T)=>void
export type CallBack = ()=>void
export type StringMap<T> = {[key:string]:T}
export type NumberMap<T> = {[key:number]:T}
