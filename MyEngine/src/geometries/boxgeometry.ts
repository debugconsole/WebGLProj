
import {Geometry, VertexAttrType} from "../render/geometry"
import { TypedArray } from "../render/gldatatype"
import { Vector4 } from "../math/vector4"

export class BoxGeometry extends Geometry{

    
    constructor(options:{width:number,height:number,length:number,faceUV?:Vector4[],faceColors?:Vector4[]}){
        super();
        //this.setVertexAttribute()
        let width = options.width ||  1;
        let height = options.height ||  1;
        let length = options.length ||  1;
        let nbFaces = 6;
        let vertices = [1,-1, 1, -1, -1, 1, -1, 1, 1, 1, 1, 1, 1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, 1, -1, 1, -1, -1, 1, -1, 1, 1, 1, 1, -1, 1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, 1, -1, 1, -1, 1, 1, -1, 1, 1, 1, 1, -1, 1, 1, -1, -1, -1, -1, -1, -1, -1, 1];
        let normals = [0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0];
        let indices = [0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23];
        let uvs = [];
        let colors = [];
        let faceUV: Vector4[] = options.faceUV || new Array<Vector4>(6);
        let faceColors = options.faceColors;
        for (let f = 0; f < 6; f++) {
            if (faceUV[f] === undefined) {
                faceUV[f] = new Vector4(0, 0, 1, 1);
            }
            if (faceColors && faceColors[f] === undefined) {
                faceColors[f] = new Vector4(1, 1, 1, 1);
            }
        }

        // Create each face in turn.
        for (let index = 0; index < nbFaces; index++) {
            uvs.push(faceUV[index].z, faceUV[index].w);
            uvs.push(faceUV[index].x, faceUV[index].w);
            uvs.push(faceUV[index].x, faceUV[index].y);
            uvs.push(faceUV[index].z, faceUV[index].y);
            if (faceColors) {
                for (let c = 0; c < 4; c++) {
                    colors.push(faceColors[index].x, faceColors[index].y, faceColors[index].z, faceColors[index].w);
                }
            }
        }

        this.setVertexAttribute("position",{
            buffer:new Float32Array(vertices),
            numComponents:3,
            sizeofStride:12,
            offsetofStride:0,
        });

        this.setVertexAttribute("normal",{
            buffer:new Float32Array(normals),
            numComponents:3,
            sizeofStride:12,
            offsetofStride:0,
        });

        this.setVertexAttribute("texcoord",{
            buffer:new Float32Array(uvs),
            numComponents:2,
            sizeofStride:8,
            offsetofStride:0,
        });

        this.setVertexAttribute("indices",{
            buffer:new Uint16Array(indices),
            numComponents:1,
            sizeofStride:2,
            offsetofStride:0,
        });

        let scaleArray = [width / 2, height / 2, length / 2]
        for (let index = 0; index < vertices.length; index++) {
            vertices[index] = vertices[index] * scaleArray[index % 3];    
        }
        
        // this._vertexInfo.position = new Float32Array(vertices);
        // vertexData.indices = indices;
        // vertexData.positions = positions;
        // vertexData.normals = normals;
        // vertexData.uvs = uvs;
    
        if (faceColors) {
            // var totalColors = (sideOrientation === VertexData.DOUBLESIDE) ? colors.concat(colors) : colors;
            // vertexData.colors = totalColors;
            this.setVertexAttribute("color",{
                buffer:new Float32Array(colors),
                numComponents:4,
                sizeofStride:16,
                offsetofStride:0,
            });
        }

    }



}