import { GLContext } from "../render/glcontext"
import { Entity } from "../core/entity"
import { CallBack1 } from "../types"
import { SceneNode } from "./scenenode"
import { Matrix4 } from "../math/matrix4";


export class Scene extends SceneNode {

    constructor(){
        super();
        this.type = "Scene";
    }


    updatePositionTransform(viewMatrix:Matrix4,projectMatrix:Matrix4,node?:SceneNode,){

        let children = node?node.children:this.children;
        if(children){
            children.forEach(node=>{
                if (node.type === "Entity") {
                    (<Entity>node).updatePositionTransform(viewMatrix,projectMatrix);
                }
                this.updatePositionTransform(viewMatrix,projectMatrix,node);
            });
        }

    }

    // /**
    //  * run a render loop
    //  */
    // startRenderLoop() {

    //     console.log("----start scene render loop----");

    //     let requestUpdateFrame = window.requestAnimationFrame;

    //     let renderMain = (timestamp: number) => {
    //         //render a frame
    //         this._renderAFrame(timestamp);
    //         //continue render next frame
    //         requestUpdateFrame(renderMain);

    //     }

    //     requestUpdateFrame(renderMain);

    // }



    // constructor(cavName: string) {
    //     GLContext.init(cavName);
    //     this.renderer = new Renderer(GLContext.gl);
    //     this.startRenderLoop();
    // }


    // /**
    //  * render a frame
    //  * @param timestamp 
    //  */
    // private _renderAFrame(timestamp: number) {
    //     //console.log("-----renderAFrame-------");
    //     //update all entities


    //     this.renderer.render();
    // }


}