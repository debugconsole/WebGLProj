
import { DrawableObj } from "../render/drawobject"
import { Mesh } from "./mesh"
import { Primitive } from "./primitive"
import { Geometry } from "../render/geometry"
import { ERenderGroupType } from "../render/renderer"
import { SceneNode } from "./scenenode"
import { BuiltInProgram} from "../render/builtinprogram"
import { Matrix4 } from "../math/matrix4"


/**
 * a entity is also a scene node, which has another actions;
 */
export class Entity extends SceneNode{

    public drawableObjs:DrawableObj[]=[];
    public renderType:ERenderGroupType;
    /**
     * if craete from mesh ,it is not null
     */
    public mesh:Mesh = undefined;
    private _currentTransform:Matrix4;

    updatePositionTransform(viewMatrix:Matrix4,projectMatrix:Matrix4){
        
        //let modelWorldmatrix = Matrix4.multiply(this.worldMatrix,this.matrix);
        let pvMatrix = Matrix4.multiply(projectMatrix,viewMatrix);
        let transformMatrix = Matrix4.multiply(pvMatrix,this.worldMatrix);

        if ( this._currentTransform === undefined || !this._currentTransform.equals(transformMatrix)) {
            this._currentTransform = transformMatrix;
            console.log("transformdata:" + transformMatrix.data);
            this.drawableObjs.forEach(drawObj=>{
                drawObj.uniforms["transform"] = transformMatrix.data;
            });
        }
        
    }

    /**
     * set uniform to all the drawobjs
     */
    setUniform(uniform:string,data:number[]|number){
        this.drawableObjs.forEach(drawObj => {
            drawObj.uniforms[uniform] = data;
        });
    }


    static createEntFromDrawableObj(drawableObj:DrawableObj,renderType?:ERenderGroupType):Entity{
        let ent = new Entity(renderType);
        ent.drawableObjs.push(drawableObj);

        //guess the drawcount of drawobj
        let indicesVertexProperty = drawableObj.vertexInfo.indices;
        let positionVertexProperty = drawableObj.vertexInfo.position;
        if (indicesVertexProperty) {
            drawableObj.drawArrayCount = indicesVertexProperty.buffer.length / indicesVertexProperty.numComponents;
            drawableObj.drawArrayOffset = 0;
        }else{
            drawableObj.drawArrayCount = positionVertexProperty.buffer.length / positionVertexProperty.numComponents;
            drawableObj.drawArrayOffset = 0;
        }

        return ent;
    }

    static createEntFromMesh(mesh:Mesh):Entity{
        let ent:Entity = new Entity();
        //ent.drawableObj = drawableObj;

        mesh.primitives.forEach((primitive, key, map)=>{
            console.log("create drawobj");
            let drawObj = new DrawableObj();
            drawObj.vertexInfo = primitive.geometry.vertexInfo;
            drawObj.drawArrayCount=primitive.geometry.drawCount;
            drawObj.drawArrayOffset=primitive.geometry.drawOffset;
            drawObj.drawType = primitive.drawMode;
            drawObj.programInfo = BuiltInProgram.createNormalProgram();
            ent.drawableObjs.push(drawObj);
        });
        ent.mesh = mesh;

        return ent;
    }

    constructor(renderType?:ERenderGroupType){
        super();
        this.renderType = renderType || ERenderGroupType.Opaque;
        this.type = "Entity";

        // this.addUpdateListener((ent)=>{
        //     let perspectiveMatrix = camera.projectionMatrix;
        //     let viewMatrix = camera.viewMatrix;
        //     let transformMatrix = Matrix4.multiply(perspectiveMatrix,viewMatrix);
        //     ent.drawableObjs.forEach(drawobj=>{
        //         drawobj.uniforms["transform"] = transformMatrix.data;
        //     })
        // });

    }





}