
export class Event<T>{

    private _listenerMap:Map<object,T> = new Map<object,T>();

    addListener(listener:T){
        this._listenerMap.set(<any>listener,listener);
    }

    removeListener(listener:T){
        this._listenerMap.delete(<any>listener);
    }

    clear(){
        this._listenerMap.clear();
    }

    emit(...param:any){
        this._listenerMap.forEach((value,key,map) => {
            (<any>value)(...param);
        });
    }

}