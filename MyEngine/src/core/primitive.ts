import {Geometry} from "../render/geometry"
import {Material} from "../render/material"
import {EDrawType} from "../render/drawobject"

export class Primitive{

    public name:string;

    constructor(

        public geometry?:Geometry,

        public material?:Material,

        public drawMode?:EDrawType){

    }

    clone():Primitive{
        let ret = new Primitive();
        ret.copy(this);
        return ret;
    }

    copy(source:Primitive){
        this.geometry = source.geometry.clone();
        this.material = source.material.clone();
        this.drawMode = source.drawMode;
        this.name = source.name;
    }

}