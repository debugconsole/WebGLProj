
import { Vector4 } from "../math/vector4"
import { Camera,PerspectiveCamera } from "./camera"
import { Scene } from "./scene"
import { Renderer } from "../render/renderer"
import { RenderTarget } from "./rendertarget"
import { Event } from "./events"
import { CallBack } from "../types"


interface ViewerOptions{
    renderer:Renderer,
    container:HTMLElement,
    width?:number,
    height?:number,
    camera?:Camera,
    xoffset?:number,
    yoffset?:number
}

let defaultOptions = {
    width:1280,
    height:720,
    camera:null,
    xoffset:0,
    yoffset:0,
} as ViewerOptions


/**
 * for binding a viewer to a camera with a scene 
 */
export class Viewer{

    readonly name:string;
    readonly camera:Camera;
    readonly scene:Scene;
    //readonly canvasID:string;
    readonly viewportRect:Vector4;
    readonly renderer:Renderer;
    readonly width:number;
    readonly height:number;
    private container:HTMLElement;


    static gViewerCount:number = 0;
    /**
     * if renderTarget == null means that render to screen by default
     */
    private _renderTarget:RenderTarget = undefined;

    // events
    public eventBeforeRender:Event<CallBack> = new Event<CallBack>();
    public eventAfterRender:Event<CallBack> = new Event<CallBack>();
    public eventMouseMove:Event<any> = new Event<any>();
    public eventMouseUp:Event<any> = new Event<any>();
    public eventMouseDown:Event<any> = new Event<any>();
    public eventMouseWheel:Event<any> = new Event<any>();
    private _eventMouseMoveHandleRef!: (this:HTMLElement,ev:MouseEvent)=>void;
    private _eventMouseDownHandleRef!: (this:HTMLElement,ev:MouseEvent)=>void;
    private _eventMouseUpHandleRef!: (this:HTMLElement,ev:MouseEvent)=>any;
    private _eventMouseWheelHandleRef!: (this:HTMLElement,ev:MouseEvent)=>void;

    constructor(options:ViewerOptions){
        let opt = {} as ViewerOptions;
        Object.assign(opt,defaultOptions,options);                        
        this.name = "Viewer" + Viewer.gViewerCount++;                                      
        this.width = opt.width;
        this.height = opt.height;
        this.viewportRect = new Vector4(opt.xoffset,opt.yoffset,opt.width,opt.height);
        //this.canvasID = canvasID;
        //GLContext.init(this.canvasID);
        this.renderer = opt.renderer;//new Renderer(GLContext.gl);
        this.bindContainer(opt.container);

        //permit there is no camera,user draw directly
        if (opt.camera) {
            this.camera = opt.camera;
            this.scene = opt.camera.scene;
        }else{
            this.scene = new Scene();
            this.camera = new PerspectiveCamera(this.scene,75,this.width/this.height,1,10000);
        }


        // bind events
        this._eventMouseMoveHandleRef = this._mousemoveHandler.bind(this);
        this._eventMouseUpHandleRef = this._mouseupHandler.bind(this);
        this._eventMouseDownHandleRef = this._mousedownHandler.bind(this);
        this._eventMouseWheelHandleRef = this._mousewheelHandler.bind(this);

        this.eventBeforeRender.onFire(()=>{
            if (this._renderTarget) {
                this._renderTarget.beginRenderToTarget();
            }
        })

        this.eventAfterRender.onFire(()=>{
            if (this._renderTarget) {
                this._renderTarget.endRenderToTarget();
            }
        })

        

    }

    get renderTarget(){
        return this._renderTarget;
    }

    set renderTarget(value:RenderTarget){
        this._renderTarget = value;
    }

    setRenderTarget(value:RenderTarget){
        this.renderTarget = value;
    }


    // ----------------- mouse event handler ------------------

    bindContainer(container:HTMLElement){
        if (this.container) {
            this._removeAllEvents();
        }

        this.container = container;
        this._registerAllEvents();
    }

    private _mouseupHandler(opts:any){
        this.eventMouseUp.fire(opts);
    }
    private _mousedownHandler(opts:any){
        this.eventMouseDown.fire(opts);
    }
    private _mousemoveHandler(opts:any){
        this.eventMouseMove.fire(opts);
    }

    private _mousewheelHandler(opts:any){
        this.eventMouseWheel.fire(opts);
    }



    private _registerAllEvents(){
        this.container.addEventListener('mouseup', this._eventMouseUpHandleRef);
        this.container.addEventListener('mousedown', this._eventMouseDownHandleRef);
        this.container.addEventListener('mousemove', this._eventMouseMoveHandleRef);
        this.container.addEventListener('wheel', this._eventMouseWheelHandleRef);
    }

    private _removeAllEvents(){
        this.container.removeEventListener('mouseup', this._eventMouseUpHandleRef);
        this.container.removeEventListener('mousedown', this._eventMouseDownHandleRef);
        this.container.removeEventListener('mousemove', this._eventMouseMoveHandleRef);
        this.container.removeEventListener('wheel', this._eventMouseWheelHandleRef);
    }

}