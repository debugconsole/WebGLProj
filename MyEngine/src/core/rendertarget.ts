import { GLContext } from "../render/glcontext"
import { Texture } from "./texture"
import { Vector4 } from "../math/vector4";

export class RenderTarget {

    rttFramebuffer:WebGLFramebuffer;


    /**
     * create a new framebuffer ,  texture , renderbuffer and bind texture and renderbuffer to framebuffer
     * @param width 
     * @param height 
     */
    static createTextureRenderTarget(width:number,height:number):RenderTarget{

        try{

            let gl = GLContext.gl;

            let ret = new RenderTarget();
            //create frame buffer      
            ret.rttFramebuffer = gl.createFramebuffer();
            let rttFramebuffer = ret.rttFramebuffer;
            gl.bindFramebuffer(gl.FRAMEBUFFER, rttFramebuffer);      
            // rttFramebuffer.width = width;      
            // rttFramebuffer.height = height; 

            let glTexture = Texture.createEmptyTexture(width,height).texture;

            //create render buffer      
            var renderbuffer = gl.createRenderbuffer();      
            gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);      
            //设置当前工作的渲染缓冲的存储大小      
            gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, //这两个参数是固定的      
                width, height);     
            
            //texture绑定到frame buffer中      
            //https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/framebufferTexture2D      
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0,       
                gl.TEXTURE_2D, //target      
                glTexture, //source, A WebGLTexture object whose image to attach.      
                0);//A GLint specifying the mipmap level of the texture image to be attached. Must be 0.          
            
            //把render buffer绑定到frame buffer上      
            gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT,       
                gl.RENDERBUFFER, //renderBufferTarget, A GLenum specifying the binding point (target) for the render buffer.      
                renderbuffer);//A WebGLRenderbuffer object to attach.  

            //unbind      
            gl.bindTexture(gl.TEXTURE_2D, null);      
            gl.bindRenderbuffer(gl.RENDERBUFFER, null);      
            gl.bindFramebuffer(gl.FRAMEBUFFER, null);  
            return ret;
        }catch(err){
            console.error("createTextureRenderTarget ERROR:" + err );
            throw err;
        }
        
    }

    pickColorFromRenderTarget(screenX:number,ScreenY:number):Vector4{
        let gl = GLContext.gl;
        let color;
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.rttFramebuffer);

        let pixelData = new Uint8Array(4 * 1);
        gl.readPixels(screenX,ScreenY,1,1,gl.RGBA,gl.UNSIGNED_BYTE,pixelData);
        if (pixelData && pixelData.length) {
            let length = 4 * 1;
            color = new Vector4(pixelData[0],pixelData[1],pixelData[2],pixelData[3]);
        }
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);  
        return color;
    }


    beginRenderToTarget(){

        console.log("begin render to FBO");
        let gl = GLContext.gl;
        //bind to FBO
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.rttFramebuffer);  

    }

    endRenderToTarget(){

        console.log("end render to FBO");
        //unbind
        let gl = GLContext.gl;
        gl.bindFramebuffer(gl.FRAMEBUFFER, null); 
    }


}