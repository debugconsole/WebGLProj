import { Primitive } from "./primitive"


export class Mesh{

    primitives:Map<object,Primitive> = new Map<object,Primitive>();

    addPrimitive(primitive:Primitive){

        if (!primitive) {
            return;
        }

        this.primitives.set(primitive,primitive);

    }

    removePrimitive(primitive:Primitive){

        if (!primitive) {
            return;
        }

        this.primitives.delete(primitive);

    }

    copy(source:Mesh){
        this.primitives.clear();
        source.primitives.forEach(primitive => {
            this.addPrimitive(primitive.clone());
        });
    }

    clone():Mesh{
        let ret:Mesh = new Mesh();
        ret.copy(this)
        return ret;
    }

}