import { Matrix4 } from "../math/matrix4"
import { Utils } from "./utils"
import { Vector3 } from "../math/vector3"
import { Vector4 } from "../math/vector4"
import { CallBack1 } from "../types"


/*
 * represent a node in scene. 
 * scene node is the basic class for all scene object.
 */
export class SceneNode {

    //base info
    id: number;
    name: string;
    type: string = "SceneNode";

    //transform
    _position: Vector3 = Vector3.ZERO;
    _quaternion: Vector4 = Vector4.ZERO;
    _scale: Vector3 = Vector3.ZERO;
    matrix: Matrix4 = Matrix4.IDENTITY;
    worldMatrix: Matrix4 = Matrix4.IDENTITY;

    //node tree
    parent: SceneNode;
    children: SceneNode[] = [];
    onChange: CallBack1<SceneNode>[] = [];

    
    get position():Vector3{
        return this._position;
    }

    set position(value:Vector3){
        this._position = value;
        this._updateTransform();
    }

    get quaternion():Vector4{
        return this._quaternion;
    }

    set quaternion(value:Vector4){
        this._quaternion = value;
        this._updateTransform();
    }

    get scale():Vector3{
        return this._scale;
    }

    set scale(value:Vector3){
        this._scale = value;
        this._updateTransform();
    }

    private static nodeID: number = 0;
    constructor(parent?: SceneNode) {
        this.id = SceneNode.nodeID++;
        this.parent = parent;
    }

    copyFrom(source: SceneNode): SceneNode {
        let ret: SceneNode = Utils.deepCopy(source);
        this.id = source.id;
        this.name = source.name;
        this.position = source.position;
        this.parent = source.parent;
        return ret;
    }

    clone(): SceneNode {
        let ret = new SceneNode();
        ret.copyFrom(this);
        return ret;
    }

    private _updateListenerArray: CallBack1<SceneNode>[] = [];

    /**
     * add a listener in update propress
     */
    addUpdateListener(listener: CallBack1<SceneNode>) {
        if (listener) {
            this._updateListenerArray.push(listener);
        }
    }

    /**
     * update entity property preframe
     */
    update() {
        this._updateListenerArray.forEach(listener => {
            listener(this);
        });
        let children = this.children;
        if (children && children.length > 0) {
            children.forEach(node => {
                node.update();
            });
        }
    }

    /**
     * do a callback in traversing all node of tree
     * @param callback 
     */
    traverse(callback:CallBack1<SceneNode>){
        if (callback) {
            callback(this);
        }
        let children = this.children;
        if (children && children.length > 0 ) {
            children.forEach(node => {
                node.traverse(callback);
            }); 
        }
    }

    /**
     * add a child
     */
    addNode(node: SceneNode) {

        if (!node) {
            console.error("node isn't a scenenode");
            return;
        }

        console.log("addNode..");
        this.children.push(node);
        node.parent = this;

        if (this.onChange) {
            console.log("onchange..........:" + this.onChange.length);
            this.onChange.forEach(listener => {
                listener(this);
            })
        }

    }

    /**
     * add a onChange listener 
     * @param callback 
     */
    addOnChangeListener(callback: CallBack1<SceneNode>) {
        let index = this.onChange.length;
        this.onChange.push(callback);
        return index;
    }

    /**
     * remove a onChange Listener
     * @param index 
     */
    removeOnChangeListener(index: number) {
        this.onChange.slice(index, 1);
    }

    private _updateTransform(){
        this.matrix.compose(this._position,this._quaternion,this._scale);
        let parent = this.parent;
        this.worldMatrix = this.matrix;
        while(parent){
            this.worldMatrix = Matrix4.multiply(parent.matrix,this.worldMatrix);
            parent = parent.parent;
        }
        
    }


    applyMatrix( matrix:Matrix4 ) {

        this.matrix = Matrix4.multiply(matrix,this.matrix);
		//this.matrix.multiplyMatrices( matrix, this.matrix );
		this.matrix.decompose( this.position, this.quaternion, this.scale );

	}

}