import { Renderer } from "../render/renderer"
import { Viewer } from "./viewer"
import { GLContext } from "../render/glcontext"
import { Camera } from "./camera"
import { Event } from "./event"
import { CallBack } from "../types"

export class Canvas{

    public static gViewerPriority:number = 0;
    public renderer:Renderer;
    public viewerList:Viewer[]=[];
    public mousemoveEvent:Event<CallBack> = new Event<CallBack>();
    
    constructor(canvasID:string){
        GLContext.init(canvasID);
        this.renderer = new Renderer(GLContext.gl);
        if (GLContext.canvas) {
            GLContext.canvas.onmousemove = (event:MouseEvent)=>{
                this.mousemoveEvent.emit(event);
            }
        }
    }

    addViewer(viewer:Viewer,priority?:number){
        priority = priority || Canvas.gViewerPriority++;
        this.viewerList.push(viewer);
    }

    createViewer(width:number,height:number,
            camera?:Camera):Viewer{
        //let viewer = new Viewer({renderer:this.renderer,width,height,camera});
        return null;
    }


    setCursorStyle(cursorStyle:string){
        cursorStyle = cursorStyle || 'default';
        GLContext.canvas.style.cursor=cursorStyle;
    }




}