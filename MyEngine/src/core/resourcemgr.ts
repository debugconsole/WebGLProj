
export interface MapObj{
    [key:string]:string
}


/**
 * 所有资源在这里preload
 */
export class ResourceManager{

    /**
     * 这是个单例
     */
    private static singleton:ResourceManager;
    static getSingleton():ResourceManager{
        if(!ResourceManager.singleton){
            ResourceManager.singleton = new ResourceManager();
        }
        return ResourceManager.singleton;
    }
    
    /**
     * 缓存shader资源
     */
    shaderCache:MapObj = {};


    /**
     * 异步加载所有shader资源
     *  shaderCache 作为异步加载后的资源作为参数被传递给resolve
     * @param shaderFileList 
     */
    loadShaders(shaderFileList:string[]){
    
        return new Promise((resolve,reject)=>{
            let handleCount = 0;
            let isfinished = ()=>{
                if (handleCount >= shaderFileList.length) {
                    resolve(this.shaderCache)
                }
            }
            shaderFileList.forEach(fileName => {
                this._fetchResource(fileName).then(resp=>{

                    if(resp.ok){
                        resp.text().then(value=>{
                            this.shaderCache[fileName] = value;
                            handleCount+=1;
                            isfinished();
                        })

                    }else{
                        console.log(`load shader false ${fileName}`);
                        handleCount+=1;
                        isfinished();
                    }
    
                }
                ,reason => {
                    console.log(`load shader false ${fileName}: ${reason}`);
                    handleCount+=1;
                    isfinished();
                }).catch(e=>{
                    reject(e);
                })

            });

        })

    }


    /**
     * request a resource in the url by 'fetch'
     * @param resPath 
     */
    private _fetchResource(resPath:string):Promise<Response>{
        return fetch(resPath,{
            method:'get',
            headers:new Headers({
                'Conten-Type':'text/plain'
            }),
        });

    }


}
