

export default class WebGLInstaller
{

    glshader:any = null;
    gl:WebGLRenderingContext = null;
    program:WebGLProgram = null;

    constructor(){
        //GL初始化-----------------------------
        var canvas = <HTMLCanvasElement>document.getElementById('canvas');
        if (!canvas) {
            console.error("can't get canvas.");
            return;
        }
        console.log("111111111111");
        //获取webgl上下文
        this.gl = <WebGLRenderingContext>canvas.getContext("webgl");
        if (!this.gl) { 
            console.log("no webgl2");
            this.gl = <WebGLRenderingContext>canvas.getContext("webgl");
            if (!this.gl) {
                console.error("no support webgl.");
                return;
            }
        }
    }


    drawBuffer(vertexArray:Float32Array){
        let buffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER,buffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER,vertexArray,this.gl.STATIC_DRAW);

        var positionLocation = this.gl.getAttribLocation(this.program, "a_position");
        this.gl.enableVertexAttribArray(positionLocation);
        this.gl.vertexAttribPointer(positionLocation, 2, this.gl.FLOAT, false, 0, 0);

        //清理旧的绘制新的
        this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
        this.gl.drawArrays(this.gl.POINTS,0,3);

    }


    /**
     * 装载一个Program
     * @param vsShaderSource 
     * @param fsShaderSource 
     */
    setupProgramFromScript(vsShaderSource:any,fsShaderSource:any){
        // console.log("VS:" + vsShaderSource);
        let vsShader = this._createShader(vsShaderSource,this.gl.VERTEX_SHADER);
        let fsShader = this._createShader(fsShaderSource,this.gl.FRAGMENT_SHADER);
        if ( vsShader && fsShader ) {
            this.program = this._createProgram(vsShader,fsShader);
            if (this.program) {
                return true
            }
        }

        return false;
        
    }

    /**
     * 创建Shader
     * @param {*} shaderSource Shader代码
     * @param {*} shaderType    Shader类型
     */
    private _createShader(shaderSource:any,shaderType:any){

        //console.log(`-->Type:${shaderType},Shader: ${shaderSource}`);
        if (shaderType === this.gl.VERTEX_SHADER || shaderType === this.gl.FRAGMENT_SHADER) {
            this.glshader = this.gl.createShader(shaderType);
        }else{
            return;
        }

        this.gl.shaderSource(this.glshader,shaderSource);
        this.gl.compileShader(this.glshader);
        if (this.gl.getShaderParameter(this.glshader,this.gl.COMPILE_STATUS)) {
            console.log(`-->createAndCompileShaderSuccessful!!` );
            return this.glshader;
        }else{
            console.log(`-->CompileShaderFalse` );
            // 编译失败，弹出错误消息
            alert(this.gl.getShaderInfoLog(this.glshader));
        }

    }

    /**
     * 绑定Shader 创建Program 
     * @param {*} vs 
     * @param {*} fs 
     */
    private _createProgram(vs:any,fs:any){
        let program = this.gl.createProgram();
        if (!program) {
            return;
        }
        this.gl.attachShader(program,vs);
        this.gl.attachShader(program,fs);
        this.gl.linkProgram(program);
        if (this.gl.getProgramParameter(program,this.gl.LINK_STATUS)) {
            this.gl.useProgram(program);
            return program;
        }else{
            // 编译失败，弹出错误消息
            alert(this.gl.getProgramInfoLog(program));
        }
    }

}


