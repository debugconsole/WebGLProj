type CB<T> = (e:T)=>void

export class Event<T> {
    // eventname <-> listener[]
    events:Set<CB<T>> = new Set<CB<T>>();
  
    onFire(callback: CB<T>) {
        if (!this.events.has(callback)) {
            this.events.add(callback);
        }
      console.log('onEvent:', this.events);
    }
  
    onFireOnce(callback: (arg?: any) => void) {
      const oneCall = async (a:T) => {
        await callback(a);
        this.off(oneCall);
      };
      this.onFire(oneCall);
    }
  
    fire(arg?: any) {
        this.events.forEach(cb=>{
            cb(arg);
        })
    }
  
    off(callback: CB<T>) {
        if (this.events.has(callback)) {
            this.events.delete(callback);
        }
    }
  
    hasEvent(callback: CB<T>) {
      return this.events.has(callback);
    }
  }