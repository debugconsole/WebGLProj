
export class Utils {

    static deepCopy(o:any):any{
        if (o instanceof Array) {
            let n = [];
            for (let i = 0; i < o.length; ++i) {
                n[i] = Utils.deepCopy(o[i]);
            }
            return n;
    
        } else if (o instanceof Object) {
            let n:any = {}
            for (let i in o) {
                n[i] = Utils.deepCopy(o[i]);
            }
            return n;
        } else {
            return o;
        }
    }
}