import { GLContext } from "../render/glcontext"

export class Texture{


    constructor(
        public texture:WebGLTexture,
    ){}

    /**
     * Returns true if value is power of 2
     * @param {number} value number to check.
     * @return true if value is power of 2
     */
    private _isPowerOf2(value:number) {
        return (value & (value - 1)) === 0;
    }

    static createTexture(image:any){
        let gl = GLContext.gl;
        let texture = gl.createTexture();// 创建纹理
        gl.activeTexture(gl.TEXTURE0);// 激活0号纹理单元（0号是默认激活的纹理单元）
        gl.bindTexture(gl.TEXTURE_2D, texture);// 绑定纹理对象到激活的纹理单元
        //gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);// 图像坐标与纹理坐标Y轴方向相反，需进行Y轴反转
        // config texParameteri
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);// 纹理放大方式
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);// 纹理缩小方式
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);// 纹理水平填充方式
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);// 纹理垂直填充方式
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);// 配置纹理图像
        gl.bindTexture(gl.TEXTURE_2D, null);
        return new Texture(texture);
    }

    static createEmptyTexture(width:number,height:number,border:number=0){
        let gl = GLContext.gl;
        
        let texture = gl.createTexture();// 创建纹理
        gl.activeTexture(gl.TEXTURE0);// 激活0号纹理单元（0号是默认激活的纹理单元）
        gl.bindTexture(gl.TEXTURE_2D, texture);// 绑定纹理对象到激活的纹理单元
        //gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);// 图像坐标与纹理坐标Y轴方向相反，需进行Y轴反转
        // config texParameteri
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);// 纹理放大方式
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);// 纹理缩小方式
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);// 纹理水平填充方式
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);// 纹理垂直填充方式
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB,width,height,border, gl.RGB, gl.UNSIGNED_BYTE, null);// 配置纹理图像
        gl.bindTexture(gl.TEXTURE_2D, null);
        return new Texture(texture);
    }

}