import { Matrix4 } from "../math/matrix4"
import { SceneNode } from "./scenenode"
import { Vector3 } from "../math/vector3"
import {Scene} from "./scene"


export class Camera extends SceneNode {

    _viewMatrix: Matrix4 = Matrix4.IDENTITY;
    _projectionMatrix: Matrix4 = Matrix4.IDENTITY;
    upVector: Vector3 = undefined;
    lookAt: Vector3 = undefined;
    scene:Scene = undefined;

    constructor(scene:Scene){
        super();
        this.scene = scene;
    }

    get viewMatrix(): Matrix4 {
        return this._viewMatrix;
    }

    get projectionMatrix(): Matrix4 {
        return this._projectionMatrix;
    }


    updateMatrix() {

        if (this.upVector && this.lookAt) {
            this._viewMatrix = Matrix4.lookAt2(this.position, this.lookAt, this.upVector);
        }

    }
}


export class PerspectiveCamera extends Camera {

    /**
     * fov in degree
     */
    fovy: number;
    aspect: number;
    near: number;
    far: number;

    constructor(scene:Scene,fov: number, aspect: number, near: number, far: number) {
        super(scene);
        this.fovy = fov;
        this.aspect = aspect;
        this.near = near;
        this.far = far;
        this.addUpdateListener((cam) => {
            (<PerspectiveCamera>cam).updateMatrix();
        })
    }


    updateMatrix() {
        super.updateMatrix();
        this._projectionMatrix = Matrix4.perspective((this.fovy / 180) * Math.PI, this.aspect, this.near, this.far);
    }


}

