import { Scene } from "./scene"
import { Canvas } from "./canvas"
import { Viewer } from "./viewer"
import { Entity } from "./entity"
import { StringMap } from "../types"
import { SceneNode } from "./scenenode"
import { Helper } from "../utils/helper"
import { Stats } from "../utils/fpsstate"
import { Renderer } from "../render/renderer"

export class Engine {

    private static _singleton: Engine;
    static get singleton() {
        if (!Engine._singleton) {
            Engine._singleton = new Engine();
        }
        return Engine._singleton;
    }

    private constructor() { }

    private _viewerList: Viewer[] = [];
    static gViewerPriority: number = 0;

    /**
     * user entry ,init engine
     */
    init() {
        this.setEnableStatsPanel(true);
        this._startRenderLoop();
    }


    private stats:any;
    setEnableStatsPanel(enabled:boolean){
        if (enabled) {
            let statsConstruct:any = Stats;
            this.stats = new statsConstruct();
            document.body.appendChild(this.stats.dom);
        }
    }

    addViewer(canvas: Canvas, viewer: Viewer, priority?: number) {
        canvas.addViewer(viewer, priority);
        this._bindSceneChangeToRender(viewer);
    }


    createDefaultView(container:HTMLElement):Viewer{
        let canvas = document.createElement('canvas');
        canvas.style.position = 'absolute';
        canvas.style.left = '0';
        canvas.style.top = '0';
        canvas.style.width = '100%';
        canvas.style.height = '100%';
        canvas.style.zIndex = '-1';
        container.appendChild(canvas);
        let gl = canvas.getContext('webgl');
        let renderer = new Renderer(gl);
        let viewer = new Viewer({renderer,container});
        this._viewerList.push(viewer);
        return viewer;
    }


    updateSceneEntityToRenderer(viewer: Viewer) {
        console.log("scene change......." + viewer.scene.children.length);
        //clear first
        viewer.renderer.clearRenderGroup();
        //setup drawobj
        this._updateNodeToRenderer(viewer, viewer.scene);

    }

    private _updateNodeToRenderer(viewer: Viewer, node: SceneNode) {
        node.children.forEach(childnode => {
            if (childnode.type === "Entity") {
                let ent: Entity = <Entity>childnode;
                console.log("add ent...");
                ent.drawableObjs.forEach(drawableObj => {
                    //console.log("add drawableObj..." + ent.renderType);
                    viewer.renderer.addDrawAbleObj(drawableObj, ent.renderType);
                });

            }

            if (childnode.children && childnode.children.length > 0) {
                this._updateNodeToRenderer(viewer, childnode);
            }

        });
    }



    private _bindSceneChangeToRender(viewer: Viewer) {

        viewer.scene.addOnChangeListener(scene => {
            this.updateSceneEntityToRenderer(viewer);
        });

        this.updateSceneEntityToRenderer(viewer);

    }

    /**
     * run a render loop
     */
    _startRenderLoop() {

        console.log("----start scene render loop----");

        try {
            let requestUpdateFrame = window.requestAnimationFrame;

            let renderMain = (timestamp: number) => {
                //render a frame
                this._renderAFrame(timestamp);

                if (this.stats) {
                    this.stats.update();
                }

                //continue render next frame
                requestUpdateFrame(renderMain);

            }

            requestUpdateFrame(renderMain);
        } catch (excp) {
            console.log("EXCP:" + excp);
        }


    }

    /**
     * render a frame
     * @param timestamp 
     */
    private _renderAFrame(timestamp: number) {
        console.log("render a frame----------------------");

        let viewers = this._viewerList;
        for (let index = 0,len = viewers.length; index < len; index++) {
            const element = viewers[index];
            this._renderAView(element);
        }
    }



    private _renderAView(viewer: Viewer) {

        // take a timestamp at the beginning.
        let start = performance.now();
        //render each viewer


        console.log("viewer name:" + viewer.name);

        viewer.eventBeforeRender.fire();

        // start update and render scene---------------------
        // update viewport
        // viewer.renderer.viewport(viewer.viewportRect);

        let camera = viewer.camera;
        if (camera) {
            // update camera matrix
            camera.updateMatrix();
        }

        // update scene
        viewer.scene.update();

        // update transform
        if (camera) {
            viewer.scene.updatePositionTransform(camera.viewMatrix, camera.projectionMatrix);
        }

        Helper.calculateTimestamp(() => {
            // render
            viewer.renderer.clear();
            viewer.renderer.render();
        },"1");

        // end update and render scene---------------------

        viewer.eventAfterRender.fire();


        // take a final timestamp.
        let diffMillSeconds = performance.now() - start;
        let fps = 1000 / diffMillSeconds;

        // calculate the time taken and output the result in the console
        //console.log(`'fps:${fps} a frame took ${diffMillSeconds}  milliseconds to execute. `);

    }

}