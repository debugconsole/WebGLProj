

export type OnLoadCallBack = (response: any) => void
export type OnProgressCallBack = (request: ProgressEvent) => void
export type OnErrorCallBack = (event: ErrorEvent) => void


export class LoadingManager {

    public itemsTotal: number = 0;
    public itemsLoaded: number = 0;
    public isLoading: boolean = false;

    constructor(
        public onStart?: (url: string, loaded: number, total: number) => void,
        public onLoad?: () => void,
        public onProgress?: (url: string, loaded: number, total: number) => void,
        public onError?: (url: string) => void
    ) { }


    load(url: string,
        onLoad?: OnLoadCallBack,
        onProgress?: OnProgressCallBack,
        onError?: OnErrorCallBack) { }


    itemStart(url: string) {
        this.itemsTotal++;
        if (this.isLoading === false) {
            if (this.onStart) {
                this.onStart(url, this.itemsLoaded, this.itemsTotal);
            }
        }
        this.isLoading = true;
    }

    itemEnd(url: string) {
        this.itemsLoaded++;
        if (this.onProgress) {
            this.onProgress(url, this.itemsLoaded, this.itemsTotal);
        }

        if (this.itemsLoaded === this.itemsTotal) {
            this.isLoading = false;
            if (this.onLoad) {
                this.onLoad();
            }
        }
    }

    itemError(url: string) {
        if (this.onError) {
            this.onError(url);
        }
    }


}