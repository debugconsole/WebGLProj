import { MapCache } from "./mapcache"
import {
    LoadingManager,
    OnLoadCallBack,
    OnProgressCallBack,
    OnErrorCallBack
} from "./loadingmanager"



export class ImageLoader extends LoadingManager {


    static get singleton() {
        if (ImageLoader._singleton === undefined) {
            ImageLoader._singleton = new ImageLoader();
        }
        return ImageLoader._singleton;
    }

    private constructor() {
        super();
    }

    private _cache: MapCache = new MapCache();
    private static _singleton: ImageLoader;

    load(url: string,
        onLoad?: OnLoadCallBack,
        onProgress?: OnProgressCallBack,
        onError?: OnErrorCallBack) {

        let cached = this._cache.get(url);
        if (cached) {

            this.itemStart(url);
            setTimeout(() => {
                if (onLoad) onLoad(cached);
                this.itemEnd(url);
            }, 0);

            return cached;
        }

        let img = new Image();
        img.crossOrigin = "";
        img.src = url;

        // if the image data is in the cache of browser
        if (img.complete) {

            this.itemStart(url);
            setTimeout(() => {
                if (onLoad) onLoad(img);
                this.itemEnd(url);
            }, 0);

            return;
        }

        img.onload = () => {
            if (onLoad) onLoad(img);
            this.itemEnd(url);
        }

    }




}