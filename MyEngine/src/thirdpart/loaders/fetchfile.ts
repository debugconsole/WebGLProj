export function fetchFile(url:string){
    const resp = fetch(url);
    return resp.then((rep)=>{
        return rep.blob().then(blob=>{
            return blob.arrayBuffer();
        });
    });
}

export function fetchText(url:string){
    const resp = fetch(url);
    return resp.then((rep)=>{
        return rep.text();
      });
}

export function fetchJson(url:string){
    const resp = fetch(url);
    return resp.then((rep)=>{
        return rep.json();
      });
}
