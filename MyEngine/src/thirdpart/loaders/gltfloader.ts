import { MapCache } from "./mapcache"
import { FileLoader } from "./fileloader"
import {
	LoadingManager,
	OnLoadCallBack,
	OnProgressCallBack,
	OnErrorCallBack
} from "./loadingmanager"
import { LoaderUtils } from "./loaderutils"
import { Geometry, AttributeInfo } from "../../render/geometry"
import { Material } from "../../render/material"
import { EDrawType } from "../../render/drawobject"
import { Mesh } from "../../core/mesh"
import { Primitive } from "../../core/primitive"
import { SceneNode } from "../../core/scenenode"
import { Entity } from "../../core/entity"
import { Scene } from "../../core/scene"
import { Matrix4 } from "../../math/matrix4"
import { Vector3 } from "../../math/vector3"
import { Vector4 } from "../../math/vector4"

const BINARY_EXTENSION_BUFFER_NAME = 'binary_glTF';
const BINARY_EXTENSION_HEADER_MAGIC = 'glTF';
const BINARY_EXTENSION_HEADER_LENGTH = 12;
const BINARY_EXTENSION_CHUNK_TYPES = { JSON: 0x4E4F534A, BIN: 0x004E4942 };


/*********************************/
/********** EXTENSIONS ***********/
/*********************************/
const EXTENSIONS = {
	KHR_BINARY_GLTF: 'KHR_binary_glTF',
	KHR_DRACO_MESH_COMPRESSION: 'KHR_draco_mesh_compression',
	KHR_LIGHTS_PUNCTUAL: 'KHR_lights_punctual',
	KHR_MATERIALS_PBR_SPECULAR_GLOSSINESS: 'KHR_materials_pbrSpecularGlossiness',
	KHR_MATERIALS_UNLIT: 'KHR_materials_unlit',
	KHR_TEXTURE_TRANSFORM: 'KHR_texture_transform',
	MSFT_TEXTURE_DDS: 'MSFT_texture_dds'
};

let ATTRIBUTES = {
	POSITION: 'position',
	NORMAL: 'normal',
	TANGENT: 'tangent',
	TEXCOORD_0: 'uv',
	TEXCOORD_1: 'uv2',
	COLOR_0: 'color',
	WEIGHTS_0: 'skinWeight',
	JOINTS_0: 'skinIndex',
};

let WEBGL_COMPONENT_TYPES = {
	5120: Int8Array,
	5121: Uint8Array,
	5122: Int16Array,
	5123: Uint16Array,
	5125: Uint32Array,
	5126: Float32Array
};


let WEBGL_TYPE_SIZES = {
	'SCALAR': 1,
	'VEC2': 2,
	'VEC3': 3,
	'VEC4': 4,
	'MAT2': 4,
	'MAT3': 9,
	'MAT4': 16
};

let WEBGL_CONSTANTS = {
	FLOAT: 5126,
	//FLOAT_MAT2: 35674,
	FLOAT_MAT3: 35675,
	FLOAT_MAT4: 35676,
	FLOAT_VEC2: 35664,
	FLOAT_VEC3: 35665,
	FLOAT_VEC4: 35666,
	LINEAR: 9729,
	REPEAT: 10497,
	SAMPLER_2D: 35678,
	POINTS: 0,
	LINES: 1,
	LINE_LOOP: 2,
	LINE_STRIP: 3,
	TRIANGLES: 4,
	TRIANGLE_STRIP: 5,
	TRIANGLE_FAN: 6,
	UNSIGNED_BYTE: 5121,
	UNSIGNED_SHORT: 5123
};



export class GLTFLoader extends LoadingManager {

	static get singleton() {
		if (GLTFLoader._singleton === undefined) {
			GLTFLoader._singleton = new GLTFLoader();
		}
		return GLTFLoader._singleton;
	}

	private constructor() {
		super();
	}

	private _cache: MapCache = new MapCache();
	private static _singleton: GLTFLoader;
	public resourcePath: string;

	load(url: string,
		onLoad?: OnLoadCallBack,
		onProgress?: OnProgressCallBack,
		onError?: OnErrorCallBack) {
		let loader = FileLoader.singleton;


		if (!this.resourcePath) {
			this.resourcePath = LoaderUtils.extractUrlBase(url);
		}

		console.log("start load ...");
		let _onError = (e: any) => {

			if (onError) {

				onError(e);

			} else {

				console.error(e);

			}

			this.itemError(url);
			this.itemEnd(url);

		};

		loader.load(url, (resp: Response) => {
			console.log(`load gltf[${url}] : ${resp.ok} }`);
			try {
				// resp.arrayBuffer().then((buffer:ArrayBuffer)=>{
				// 	console.log("aaaa:" + buffer);
				// 	let bb = Buffer.from(buffer);
				// 	let cc = JSON.parse(bb.toString());
				// 	console.log("bbbb:" + JSON.stringify(cc));
				// },err=>{
				// 	console.log("err:" + err);
				// })

				// resp.json().then((json:any)=>{
				// 	console.log("aaaa:" + JSON.stringify(json));
				// },err=>{
				// 	console.log("err:" + err);
				// })

				//let data = (<Response>resp).arrayBuffer;

				resp.arrayBuffer().then((buffer: ArrayBuffer) => {
					this.parse(buffer, (gltf) => {
						onLoad(gltf);
					}, _onError);
				});

			} catch (e) {
				console.error("gltfloader exception :" + e);
			}

		});
	}


	parse(data: ArrayBuffer, onLoad: OnLoadCallBack, onError: OnErrorCallBack) {

		let content;
		let extensions: any = {};

		if (typeof data === 'string') {

			content = data;

		} else {
			var magic = LoaderUtils.decodeText(new Uint8Array(data, 0, 4));

			if (magic === BINARY_EXTENSION_HEADER_MAGIC) {

				console.log("it's glb file data stream");
				try {

					extensions[EXTENSIONS.KHR_BINARY_GLTF] = new GLTFBinaryExtension(data);

				} catch (error) {

					if (onError) onError(error);
					return;

				}

				content = extensions[EXTENSIONS.KHR_BINARY_GLTF].content;

			} else {
				console.log("it's gltf file data stream");
				content = LoaderUtils.decodeText(new Uint8Array(data));

			}

		}

		var json = JSON.parse(content);
		//console.log("json:" + JSON.stringify(json));

		if (json.asset === undefined || json.asset.version[0] < 2) {
			console.error("gltf Unsupported asset");
			if (onError) onError(<any>('THREE.GLTFLoader: Unsupported asset. glTF versions >=2.0 are supported. Use LegacyGLTFLoader instead.'));
			return;

		}

		if (json.extensionsUsed) {
			console.log("need extensions!!!");
		}

		var parser = new GLTFParser(json, extensions, { path: this.resourcePath });
		parser.parse((scene: any, scenes: any, cameras: any, animations: any, json: any) => {

			let glTF = {
				scene: scene,
				scenes: scenes,
				cameras: cameras,
				animations: animations,
				asset: json.asset,
				parser: parser,
				userData: {}
			};

			console.log("gltf parser ok..............:" + glTF.scene);
			onLoad(glTF);
		}, onError)

	}



}


class GLTFBinaryExtension {
	public header: any;
	public name: string = EXTENSIONS.KHR_BINARY_GLTF;
	public content: any = null;
	public body: any = null;

	constructor(data: any) {

		var headerView = new DataView(data, 0, BINARY_EXTENSION_HEADER_LENGTH);

		this.header = {
			magic: LoaderUtils.decodeText(new Uint8Array(data.slice(0, 4))),
			version: headerView.getUint32(4, true),
			length: headerView.getUint32(8, true)
		};

		if (this.header.magic !== BINARY_EXTENSION_HEADER_MAGIC) {

			throw new Error('THREE.GLTFLoader: Unsupported glTF-Binary header.');

		} else if (this.header.version < 2.0) {

			throw new Error('THREE.GLTFLoader: Legacy binary file detected. Use LegacyGLTFLoader instead.');

		}

		var chunkView = new DataView(data, BINARY_EXTENSION_HEADER_LENGTH);
		var chunkIndex = 0;

		while (chunkIndex < chunkView.byteLength) {

			var chunkLength = chunkView.getUint32(chunkIndex, true);
			chunkIndex += 4;

			var chunkType = chunkView.getUint32(chunkIndex, true);
			chunkIndex += 4;

			if (chunkType === BINARY_EXTENSION_CHUNK_TYPES.JSON) {

				var contentArray = new Uint8Array(data, BINARY_EXTENSION_HEADER_LENGTH + chunkIndex, chunkLength);
				this.content = LoaderUtils.decodeText(contentArray);

			} else if (chunkType === BINARY_EXTENSION_CHUNK_TYPES.BIN) {

				var byteOffset = BINARY_EXTENSION_HEADER_LENGTH + chunkIndex;
				this.body = data.slice(byteOffset, byteOffset + chunkLength);

			}

			// Clients must ignore chunks with unknown types.

			chunkIndex += chunkLength;

		}

		if (this.content === null) {

			throw new Error('THREE.GLTFLoader: JSON content not found.');

		}

	}
}


interface GLTFParserOptions {
	path: string;
}

class GLTFParser {

	json: any;
	extensions: any;
	options: any;
	cache: MapCache;
	fileLoader: FileLoader;
	primitiveCache: any = {};

	constructor(json: any, extensions: any, options?: GLTFParserOptions) {
		this.json = json || {};
		this.extensions = extensions || {};
		this.options = options || {};
		this.cache = new MapCache();
		this.fileLoader = FileLoader.singleton;
	}

	parse(onLoad: (...rest: any[]) => void, onError: OnErrorCallBack) {

		var json = this.json;

		// this.loadBuffer(0).then((resp=>{

		// }));

		// // Clear the loader cache
		// this.cache.clear();

		// Mark the special nodes/meshes in json for efficient parse
		this.markDefs();

		// Fire the callback on complete
		this.getMultiDependencies([
			'scene',
			'animation',
			'camera'

		]).then((dependencies: any) => {

			var scenes = dependencies.scenes || [];
			var scene = scenes[json.scene || 0];
			var animations = dependencies.animations || [];
			var cameras = dependencies.cameras || [];

			console.log("load All ok .......:" + scenes + "|" + scene);

			onLoad(scene, scenes, cameras, animations, json);

		}).catch(onError);


	}


	markDefs() {

		let nodeDefs = this.json.nodes || [];
		let skinDefs = this.json.skins || [];
		let meshDefs = this.json.meshes || [];

		let meshReferences: any = {};
		let meshUses: any = {};

		// Nothing in the node definition indicates whether it is a Bone or an
		// Object3D. Use the skins' joint references to mark bones.
		for (let skinIndex = 0, skinLength = skinDefs.length; skinIndex < skinLength; skinIndex++) {

			let joints = skinDefs[skinIndex].joints;

			for (let i = 0, il = joints.length; i < il; i++) {

				nodeDefs[joints[i]].isBone = true;

			}

		}

		// Meshes can (and should) be reused by multiple nodes in a glTF asset. To
		// avoid having more than one THREE.Mesh with the same name, count
		// references and rename instances below.
		//
		// Example: CesiumMilkTruck sample model reuses "Wheel" meshes.
		for (let nodeIndex = 0, nodeLength = nodeDefs.length; nodeIndex < nodeLength; nodeIndex++) {

			let nodeDef = nodeDefs[nodeIndex];

			if (nodeDef.mesh !== undefined) {

				if (meshReferences[nodeDef.mesh] === undefined) {

					meshReferences[nodeDef.mesh] = meshUses[nodeDef.mesh] = 0;

				}

				meshReferences[nodeDef.mesh]++;

				// Nothing in the mesh definition indicates whether it is
				// a SkinnedMesh or Mesh. Use the node's mesh reference
				// to mark SkinnedMesh if node has skin.
				if (nodeDef.skin !== undefined) {

					meshDefs[nodeDef.mesh].isSkinnedMesh = true;

				}

			}

		}

		this.json.meshReferences = meshReferences;
		this.json.meshUses = meshUses;
	}


	getMultiDependencies(types: string[]) {
		let results: any = {};
		let pending = [];

		for (let i = 0, il = types.length; i < il; i++) {

			let type = types[i];
			let promise = this.getDependencies(type);

			// TODO: Error-prone use of a callback inside a loop.
			let value = promise.then((value: any) => {
				let key = type + (type === 'mesh' ? 'es' : 's');
				console.log("getDependencies...." + key + " | " + value);
				results[key] = value;
			});

			//console.log("getDependencies...." + type + " | " + value);

			pending.push(value);

		}


		return Promise.all(pending).then(() => {

			console.log(" getMultiDependencies ... ok :" + Object.getOwnPropertyNames(results));

			return results;

		});
	}

	getDependencies(type: string) {

		let dependencies = this.cache.get(type);

		if (!dependencies) {

			let defs = this.json[type + (type === 'mesh' ? 'es' : 's')] || [];
			dependencies = Promise.all(defs.map((def: any, index: number) => {
				return this.getDependency(type, index);
			}));

			this.cache.add(type, dependencies);

		}

		return dependencies;
	}


	getDependency(type: string, index: any) {
		let cacheKey = type + ':' + index;
		let dependency = this.cache.get(cacheKey);

		if (!dependency) {

			switch (type) {

				case 'scene':
					console.log("load scene....");
					dependency = this.loadScene(index);
					break;

				case 'node':
					console.log("load node....");
					dependency = this.loadNode(index);
					break;

				case 'mesh':
					console.log("load mesh....");
					dependency = this.loadMesh(index);
					break;

				case 'accessor':
					console.log("load accessor....");
					dependency = this.loadAccessor(index);
					break;

				case 'bufferView':
					console.log("load bufferView....");
					dependency = this.loadBufferView(index);
					break;

				case 'buffer':
					console.log("load buffer....");
					dependency = this.loadBuffer(index);
					break;

				// case 'material':
				// 	dependency = this.loadMaterial( index );
				// 	break;

				// case 'texture':
				// 	dependency = this.loadTexture( index );
				// 	break;

				// case 'skin':
				// 	dependency = this.loadSkin( index );
				// 	break;

				// case 'animation':
				// 	dependency = this.loadAnimation( index );
				// 	break;

				// case 'camera':
				// 	dependency = this.loadCamera( index );
				// 	break;

				// case 'light':
				// 	dependency = this.extensions[ EXTENSIONS.KHR_LIGHTS_PUNCTUAL ].loadLight( index );
				// 	break;

				default:
					throw new Error('Unknown type: ' + type);

			}

			this.cache.add(cacheKey, dependency);

		}

		return dependency;

	}



	resolveURL(url: string, path: string) {

		// Invalid URL
		if (typeof url !== 'string' || url === '') return '';

		// Absolute URL http://,https://,//
		if (/^(https?:)?\/\//i.test(url)) return url;

		// Data URI
		if (/^data:.*,.*$/i.test(url)) return url;

		// Blob URL
		if (/^blob:.*$/i.test(url)) return url;

		// Relative URL
		return path + url;

	}


	loadBuffer(index: number) {
		var bufferDef = this.json.buffers[index];
		var loader = this.fileLoader;

		if (bufferDef.type && bufferDef.type !== 'arraybuffer') {

			throw new Error('THREE.GLTFLoader: ' + bufferDef.type + ' buffer type is not supported.');

		}

		// If present, GLB container is required to be the first buffer.
		if (bufferDef.uri === undefined && index === 0) {

			return Promise.resolve(this.extensions[EXTENSIONS.KHR_BINARY_GLTF].body);

		}

		var options = this.options;

		return new Promise((resolve, reject) => {
			loader.load(this.resolveURL(bufferDef.uri, options.path), (resp: Response) => {
				console.log("loadbuffer done ");
				let arraybuffer = resp.arrayBuffer();
				arraybuffer.then((buffer) => {
					console.log("loadbuffer ok");
					resolve(buffer)
				}).catch((reson) => {
					console.log("loadbuffer false:" + reson);
				});
			}, undefined, () => {
				reject(new Error('THREE.GLTFLoader: Failed to load buffer "' + bufferDef.uri + '".'));
			});

		})

	}



	loadBufferView(index: number) {

		var bufferViewDef = this.json.bufferViews[index];

		return this.getDependency('buffer', bufferViewDef.buffer).then((buffer: any) => {
			console.log("loadbufferview ok");
			var byteLength = bufferViewDef.byteLength || 0;
			var byteOffset = bufferViewDef.byteOffset || 0;
			return buffer.slice(byteOffset, byteOffset + byteLength);

		});
	}


	loadNode(nodeIndex: number) {
		var nodeDef = this.json.nodes[nodeIndex];
		var meshReferences = this.json.meshReferences;
		var meshUses = this.json.meshUses;

		let parseNode = (): Promise<SceneNode> => {
			if (nodeDef.isBone === true) {
				//todo.. return bone
			} else if (nodeDef.mesh !== undefined) {
				return this.getDependency('mesh', nodeDef.mesh).then((mesh: Mesh) => {

					console.log("mesh load ok.....!");
					var node;

					if (meshReferences[nodeDef.mesh] > 1) {

						var instanceNum = meshUses[nodeDef.mesh]++;

						node = mesh.clone();
						//node.name += '_instance_' + instanceNum;

						// // onBeforeRender copy for Specular-Glossiness
						// node.onBeforeRender = mesh.onBeforeRender;

						// for ( var i = 0, il = node.children.length; i < il; i ++ ) {

						// 	node.children[ i ].name += '_instance_' + instanceNum;
						// 	node.children[ i ].onBeforeRender = mesh.children[ i ].onBeforeRender;

						// }

					} else {

						node = mesh;

					}

					// if weights are provided on the node, override weights on the mesh.
					// if ( nodeDef.weights !== undefined ) {

					// 	node.traverse(  ( o:any )=> {

					// 		if ( ! o.isMesh ) return;

					// 		for ( var i = 0, il = nodeDef.weights.length; i < il; i ++ ) {

					// 			o.morphTargetInfluences[ i ] = nodeDef.weights[ i ];

					// 		}

					// 	} );

					// }

					let ent = Entity.createEntFromMesh(<Mesh>node);
					return ent;

				});
			} else if (nodeDef.camera !== undefined) {

			} else if (nodeDef.extensions
				&& nodeDef.extensions[EXTENSIONS.KHR_LIGHTS_PUNCTUAL]
				&& nodeDef.extensions[EXTENSIONS.KHR_LIGHTS_PUNCTUAL].light !== undefined) {

			}

			return Promise.resolve(new SceneNode());
		}

		return parseNode().then((node: SceneNode) => {

			console.log("Entity create ok .................");

			if ( nodeDef.matrix !== undefined ) {

				let matrix = Matrix4.fromArray(nodeDef.matrix);
				node.applyMatrix( matrix );

			} else {

				if ( nodeDef.translation !== undefined ) {
					//node.position = Vector3.fromArray(nodeDef.translation,0);
				}

				if ( nodeDef.rotation !== undefined ) {
					node.quaternion = Vector4.fromArray(nodeDef.rotation,0);
				}

				if ( nodeDef.scale !== undefined ) {
					node.scale = Vector3.fromArray(nodeDef.scale,0);
				}

			}

			return node;
			
		})


	}

	loadMesh(meshIndex: number) {

		//先忽略材质

		//
		let meshDef = this.json.meshes[meshIndex];
		let primitives = meshDef.primitives;

		return this.loadGeometries(primitives).then((geometries: any) => {
			console.log("geometries load ok ,geometries count:" + geometries.length);
			//let isMultiMaterial = geometries.length === 1 && geometries[ 0 ].groups.length > 0;
			let submeshes: Primitive[] = [];

			for (let i = 0, il = geometries.length; i < il; i++) {
				let geometry = geometries[i];
				let primitive = primitives[i];

				let submesh: Primitive;
				let material: Material = undefined;
				// create Primitive
				switch (primitive.mode) {
					case WEBGL_CONSTANTS.TRIANGLES:
						submesh = new Primitive(geometry, material, EDrawType.Triangles);
						break;
					case WEBGL_CONSTANTS.TRIANGLE_STRIP:
						submesh = new Primitive(geometry, material, EDrawType.Triangles_Strip);
						break;
					case WEBGL_CONSTANTS.TRIANGLE_FAN:
						submesh = new Primitive(geometry, material, EDrawType.Triangle_Fan);
						break;
					default:
						throw new Error("GLTFLoader : primitive mode unsupported " + primitive.mode);
				}
				submesh.name = meshDef.name || ('mesh_' + meshIndex);
				submeshes.push(submesh);

			}

			//create Mesh
			let mesh = new Mesh();

			for (let i = 0, il = submeshes.length; i < il; i++) {
				mesh.addPrimitive(submeshes[i]);
			}

			return mesh;

		})


	}


	loadAccessor(index: number) {
		var accessorDef = this.json.accessors[index];
		if (accessorDef.bufferView === undefined && accessorDef.sparse === undefined) {

			// Ignore empty accessors, which may be used to declare runtime
			// information about attributes coming from another source (e.g. Draco
			// compression extension).
			return Promise.resolve(null);

		}

		var pendingBufferViews = [];

		if (accessorDef.bufferView !== undefined) {

			pendingBufferViews.push(this.getDependency('bufferView', accessorDef.bufferView));

		} else {

			pendingBufferViews.push(null);

		}

		if (accessorDef.sparse !== undefined) {

			pendingBufferViews.push(this.getDependency('bufferView', accessorDef.sparse.indices.bufferView));
			pendingBufferViews.push(this.getDependency('bufferView', accessorDef.sparse.values.bufferView));

		}

		return Promise.all(pendingBufferViews).then((bufferViews) => {

			console.log("load all buffer views ok....");
			let bufferView = bufferViews[0];
			let itemSize = (<any>WEBGL_TYPE_SIZES)[accessorDef.type];
			let TypedArray = (<any>WEBGL_COMPONENT_TYPES)[accessorDef.componentType];
			let elementBytes = TypedArray.BYTES_PER_ELEMENT;
			let itemBytes = elementBytes * itemSize;
			let byteOffset = accessorDef.byteOffset || 0;
			let byteStride = accessorDef.bufferView !== undefined ? this.json.bufferViews[accessorDef.bufferView].byteStride : undefined;
			let normalized = accessorDef.normalized === true;
			let array;
			let bufferAttribute: AttributeInfo;

			if (byteStride && byteStride !== itemBytes) {

			} else {
				if (bufferView === null) {
					array = new TypedArray(accessorDef.count * itemSize);
				} else {
					array = new TypedArray(bufferView, byteOffset, accessorDef.count * itemSize);
				}

				bufferAttribute = new AttributeInfo("", {
					buffer: array,
					numComponents: itemSize,
					sizeofStride: itemBytes,
					offsetofStride: 0,
					normalized: normalized,
				});

			}

			if (accessorDef.sparse !== undefined) {
				//
			}

			return bufferAttribute;

		});




	}


	/**
	 * create Geometries from primitives.
	 * @param primitives 
	 */
	loadGeometries(primitives: any) {

		console.log("load geometries");

		let cache = this.primitiveCache;

		let isMultiPass: boolean = this.isMultiPassGeometry(primitives);
		let originalPrimitives;
		if (isMultiPass) {
			originalPrimitives = primitives; // save original primitives and use later

			// We build a single BufferGeometry with .groups from multiple primitives
			// because all primitives share the same attributes/morph/mode and have indices.

			primitives = [primitives[0]];

			// Sets .groups and combined indices to a geometry later in this method.

		}


		let createDracoPrimitive = (primitive: any) => {

			return this.extensions[EXTENSIONS.KHR_DRACO_MESH_COMPRESSION]
				.decodePrimitive(primitive, this)
				.then((geometry: any) => {

					return this.addPrimitiveAttributes(geometry, primitive);

				});

		}


		let pending = [];

		for (var i = 0, il = primitives.length; i < il; i++) {

			var primitive = primitives[i];
			var cacheKey = this.createPrimitiveKey(primitive);

			// See if we've already created this geometry
			var cached = cache[cacheKey];

			if (cached) {

				// Use the cached geometry if it exists
				pending.push(cached.promise);

			} else {

				var geometryPromise;

				if (primitive.extensions && primitive.extensions[EXTENSIONS.KHR_DRACO_MESH_COMPRESSION]) {

					// Use DRACO geometry if available
					console.log("create draco geometry");
					geometryPromise = createDracoPrimitive(primitive);

				} else {

					// Otherwise create a new geometry
					console.log("create new geometry");
					geometryPromise = this.addPrimitiveAttributes(new Geometry(), primitive);

				}

				// Cache this geometry
				cache[cacheKey] = { primitive: primitive, promise: geometryPromise };


				pending.push(geometryPromise);

			}

		}

		return Promise.all(pending).then((geometries) => {
			console.log("all geometry load ok..........")
			return geometries;
		})

	}


	addPrimitiveAttributes(geometry: Geometry, primitiveDef: any) {

		let attributes = primitiveDef.attributes;
		var pending = [];

		let assignAttributeAccessor = (accessorIndex: number, attributeName: string) => {

			return this.getDependency('accessor', accessorIndex)
				.then((accessor: AttributeInfo) => {

					console.log("load accessor ok:" + attributeName + " | " + accessor.buffer.length);
					//geometry.addAttribute( attributeName, accessor );
					accessor.name = attributeName;
					geometry.setVertexAttribute(<any>attributeName, accessor);

					if (primitiveDef.indices) {
						if (attributeName === 'indices') {
							geometry.drawCount = accessor.buffer.length / accessor.numComponents;
							geometry.drawOffset = 0;
						}
					}else{
						if (attributeName === 'position'){
							geometry.drawCount = accessor.buffer.length / accessor.numComponents;
							geometry.drawOffset = 0;
						} 
					}

				});

		}

		for (var gltfAttributeName in attributes) {

			var threeAttributeName = (<any>ATTRIBUTES)[gltfAttributeName];

			if (!threeAttributeName) continue;

			// Skip attributes already provided by e.g. Draco extension.
			if (threeAttributeName in geometry.vertexInfo) continue;

			console.log("setup attribute:" + gltfAttributeName);
			pending.push(assignAttributeAccessor(attributes[gltfAttributeName], threeAttributeName));

		}

		if (primitiveDef.indices !== undefined) {
			console.log("setup attribute indices");
			pending.push(assignAttributeAccessor(primitiveDef.indices, 'indices'));

			// var accessor = this.getDependency( 'accessor', primitiveDef.indices ).then(  ( accessor:number ) =>{

			// 	geometry.setIndex( accessor );

			// } );

			// pending.push( accessor );

		}

		//assignExtrasToUserData( geometry, primitiveDef );

		return Promise.all(pending).then(() => {

			console.log("geometry attribute load ok..........");
			return geometry;
			// return primitiveDef.targets !== undefined
			// 	? addMorphTargets( geometry, primitiveDef.targets, parser )
			// 	: geometry;

		});

	}


	isMultiPassGeometry(primitives: any): boolean {
		if (primitives.length < 2) return false;
		var primitive0 = primitives[0];
		var targets0 = primitive0.targets || [];
		if (primitive0.indices === undefined) return false;

		for (var i = 1, il = primitives.length; i < il; i++) {

			var primitive = primitives[i];

			if (primitive0.mode !== primitive.mode) return false;
			if (primitive.indices === undefined) return false;
			if (primitive.extensions && primitive.extensions[EXTENSIONS.KHR_DRACO_MESH_COMPRESSION]) return false;
			if (!this.isObjectEqual(primitive0.attributes, primitive.attributes)) return false;

			var targets = primitive.targets || [];

			if (targets0.length !== targets.length) return false;

			for (var j = 0, jl = targets0.length; j < jl; j++) {

				if (!this.isObjectEqual(targets0[j], targets[j])) return false;

			}

		}

		return true;
	}


	isObjectEqual(a: any, b: any): boolean {

		if (Object.keys(a).length !== Object.keys(b).length) return false;

		for (var key in a) {

			if (a[key] !== b[key]) return false;

		}

		return true;

	}


	createPrimitiveKey(primitiveDef: any) {

		var dracoExtension = primitiveDef.extensions && primitiveDef.extensions[EXTENSIONS.KHR_DRACO_MESH_COMPRESSION];
		var geometryKey;

		if (dracoExtension) {

			geometryKey = 'draco:' + dracoExtension.bufferView
				+ ':' + dracoExtension.indices
				+ ':' + this.createAttributesKey(dracoExtension.attributes);

		} else {

			geometryKey = primitiveDef.indices + ':' + this.createAttributesKey(primitiveDef.attributes) + ':' + primitiveDef.mode;

		}

		return geometryKey;

	}


	createAttributesKey(attributes: any) {

		var attributesKey = '';

		var keys = Object.keys(attributes).sort();

		for (var i = 0, il = keys.length; i < il; i++) {

			attributesKey += keys[i] + ':' + attributes[keys[i]] + ';';

		}

		return attributesKey;

	}



	loadScene(sceneIndex: number) {

		// scene node hierachy builder

		let buildNodeHierachy = (nodeId: any, parentObject: Scene, json: any, parser: any) => {

			var nodeDef = json.nodes[nodeId];

			return this.getDependency('node', nodeId).then((node: any) => {

				console.log("node load ok..............:" + nodeId);
				// build node hierachy

				parentObject.addNode(node);

				var pending = [];

				if (nodeDef.children) {

					var children = nodeDef.children;

					for (var i = 0, il = children.length; i < il; i++) {

						var child = children[i];
						pending.push(buildNodeHierachy(child, node, json, parser));

					}

				}

				return Promise.all(pending);

			});

		}


		var json = this.json;
		var extensions = this.extensions;
		var sceneDef = this.json.scenes[sceneIndex];
		var parser = this;

		var scene = new Scene();
		if (sceneDef.name !== undefined) scene.name = sceneDef.name;

		//assignExtrasToUserData( scene, sceneDef );

		//if ( sceneDef.extensions ) addUnknownExtensionsToUserData( extensions, scene, sceneDef );

		var nodeIds = sceneDef.nodes || [];

		var pending = [];

		for (var i = 0, il = nodeIds.length; i < il; i++) {

			pending.push(buildNodeHierachy(nodeIds[i], scene, json, parser));

		}

		return Promise.all(pending).then(function () {
			console.log("scene load ok..........");
			return scene;

		});

	}











}