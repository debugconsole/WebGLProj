import { MapCache } from "./mapcache"
import { LoadingManager,
        OnLoadCallBack,
        OnProgressCallBack,
        OnErrorCallBack} from "./loadingmanager"
import { StringMap } from "../../types"

type LoadingCallback = {
    onLoad: OnLoadCallBack,
    onProgress: OnProgressCallBack,
    onErr: OnErrorCallBack
}

/**
 * it is a quotation right out of threejs and there has some modification for typescript.
 * the primitive author is mrdoob
 */
export class FileLoader extends LoadingManager {

    private static _singleton:FileLoader;
    static get singleton(){
        if (FileLoader._singleton === undefined) {
            FileLoader._singleton = new FileLoader();
        }
        return FileLoader._singleton;
    }

    private _cache: MapCache = new MapCache();
    private _loading:StringMap<LoadingCallback[]>={};
    private _path: string;

    private constructor() {
        super();
    }

    setPath(path:string){
        this._path = path;
    }

    load(url: string,
        onLoad?: OnLoadCallBack,
        onProgress?: OnProgressCallBack,
        onError?: OnErrorCallBack ) {

        let loadUrl = url || "";
        if (this._path) {
            loadUrl = this._path + loadUrl;
        }

        //if there has _cache
        let cached = this._cache.get(url);
        if (cached !== undefined) {

            this.itemStart(url);
            setTimeout( ()=>{
                if (onLoad) onLoad(cached);
                this.itemEnd(url);
            }, 0);

            return cached;

        }

        //check if request is duplicate
        if( this._loading[url] != undefined){
            this._loading[url].push({
                onLoad:onLoad,
                onProgress:onProgress,
                onErr:onError
            })

            return;
        }

        // Check for data: URI
        let dataUriRegex = /^data:(.*?)(;base64)?,(.*)$/;
        let dataUriRegexResult = url.match(dataUriRegex);

        this._loading[url] = [];
        this._loading[url].push({
            onLoad:onLoad,
            onProgress:onProgress,
            onErr:onError
        });

        this._fetchResource(url).then(resp => {

            let callbacks = this._loading[ url ];
            delete this._loading[ url ];

            if (resp.ok === true) {

                console.log("fileloader load ok......");
                callbacks.forEach(callback => {
                    if (callback.onLoad) {
                        callback.onLoad(resp);
                    }
                });
                this.itemEnd(url);
            }else{
                this.itemError(url);
                this.itemEnd(url);
            }

        });

        this.itemStart( url );

    }


    /**
     * request a resource in the url by 'fetch'
     * @param url 
     */
    private _fetchResource(url: string): Promise<Response> {
        return fetch(url);
    }


}