

export class MapCache{
    objects:any={}
    
    get(key:string|number){
        return this.objects[key];
    }


    add(key:string|number,object:any){
        this.objects[key]=object;
    }

    remove(key:string|number){
        delete this.objects[key];
    }

    clear(){
        this.objects={};
    }

}