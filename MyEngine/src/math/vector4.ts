import { defaultValue } from "../core/defaultvalue"


export class Vector4{

    private _x:number;
    private _y:number;
    private _z:number;
    private _w:number;

    public static get ZERO():Vector4{
        return new Vector4(0,0,0,0);
    }

    constructor(x:number = 0,y:number = 0,z:number = 0,w:number = 0){
        this._x = x;
        this._y = y;
        this._z = z;
        this._w = w;
    }

    get x(){return this._x;}
    get y(){return this._y;}
    get z(){return this._z;}
    get w(){return this._w;}
    set x(val:number){this._x = val};
    set y(val:number){this._y = val};
    set z(val:number){this._z = val};
    set w(val:number){this._w = val};

    set(x:number,y:number,z:number,w:number){
        this._x = x || 0;
        this._y = y || 0;
        this._z = z || 0;
        this._w = w || 0;
    }

    clone(){
        return new Vector4(this._x,this._y,this._z,this._w);
    }


    static add(left:Vector4,right:Vector4,result:Vector4):Vector4{

        let ret = defaultValue(result,new Vector4(0,0,0,0));
        ret._x = left._x + right._x;
        ret._y = left._y + right._y;
        ret._z = left._z + right._z;
        ret._w = left._w + right._w;
        return ret;

    }


    static addScalar(left:Vector4,s:number,result:Vector4):Vector4{
        let ret = defaultValue(result,new Vector4(0,0,0,0));
        ret._x = left._x + s;
        ret._y = left._y + s;
        ret._z = left._z + s;
        ret._w = left._w + s;
        return ret;
    }

    static sub(left:Vector4,right:Vector4,result:Vector4):Vector4{
        let ret = defaultValue(result,new Vector4(0,0,0,0));
        ret._x = left._x - right._x;
        ret._y = left._y - right._y;
        ret._z = left._z - right._z;
        ret._w = left._w - right._w;
        return ret;
    }

    static subScalar(left:Vector4,s:number,result:Vector4){
        let ret = defaultValue(result,new Vector4(0,0,0,0));
        ret._x = left._x - s;
        ret._y = left._y - s;
        ret._z = left._z - s;
        ret._w = left._w - s;
        return ret;
    }


    static multiplyScalar(left:Vector4,s:number,result:Vector4):Vector4{
        let ret = defaultValue(result,new Vector4(0,0,0,0));
        ret._x = left._x * s;
        ret._y = left._y * s;
        ret._z = left._z * s;
        ret._w = left._w * s;
		return ret;
    }


    static divideScalar(left:Vector4,s:number,result:Vector4):Vector4{
        let ret = defaultValue(result,new Vector4(0,0,0,0));
        ret._x = left._x / s;
        ret._y = left._y / s;
        ret._z = left._z / s;
        ret._w = left._w / s;
		return ret;
    }

    static negate(vec4:Vector4,result:Vector4){
        let ret = defaultValue(result,new Vector4(0,0,0,0));
        ret._x = -vec4._x;
        ret._y = -vec4._y;
        ret._z = -vec4._z;
        ret._w = -vec4._w;
		return ret;
    }

    static dot(left:Vector4,right:Vector4):number{
        return left._x * right._x + left._y * right._y + left._z * right._z  + left._w * right._w;
    }

    static sizeSquare(vec4:Vector4):number{
        return vec4._x * vec4._x + vec4._y * vec4._y + vec4._z * vec4._z + vec4._w * vec4._w;
    }

    static size(vec4:Vector4):number{
        return Math.sqrt( vec4._x * vec4._x + vec4._y * vec4._y + vec4._z * vec4._z + vec4._w * vec4._w );
    }

    static normalize(vec4:Vector4,result:Vector4) {
		return Vector4.divideScalar( vec4, Vector4.size(vec4) || 1 ,result);
    }

    static distanceToSquare( left:Vector4 , right:Vector4 ){
        var dx = left._x - right._x, dy = left._y - right._y, dz = left._z - right._z, dw = left._w - right._w;
        return dx * dx + dy * dy + dz * dz + dw * dw;
    }

    static distanceTo(left:Vector4 , right:Vector4){
        return Math.sqrt(Vector4.distanceToSquare(left,right));
    }

    static fromArray(array:number[], offset:number, result?:Vector4):Vector4{
        let ret = defaultValue(result,new Vector4(0,0,0,0));

		if ( offset === undefined ) offset = 0;
		ret._x = array[ offset ];
		ret._y = array[ offset + 1 ];
        ret._z = array[ offset + 2 ];
        ret._z = array[ offset + 3 ];
        
        return ret;

    }

}