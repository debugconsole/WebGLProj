
import { defaultValue } from "../core/defaultvalue"
import { Vector3 } from "./vector3"
import { Vector4 } from "./vector4";

export class Matrix4 {

    private _dataArray: number[] = [];

    static get IDENTITY():Matrix4{
        return new Matrix4(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1);
    };

    constructor(column0Row0?: number,
        column0Row1?: number,
        column0Row2?: number,
        column0Row3?: number,
        column1Row0?: number,
        column1Row1?: number,
        column1Row2?: number,
        column1Row3?: number,
        column2Row0?: number,
        column2Row1?: number,
        column2Row2?: number,
        column2Row3?: number,
        column3Row0?: number,
        column3Row1?: number,
        column3Row2?: number,
        column3Row3?: number
    ) {
        let datas = this._dataArray;
        datas[0] = defaultValue(column0Row0, 1.0);
        datas[1] = defaultValue(column0Row1, 0.0);
        datas[2] = defaultValue(column0Row2, 0.0);
        datas[3] = defaultValue(column0Row3, 0.0);
        datas[4] = defaultValue(column1Row0, 0.0);
        datas[5] = defaultValue(column1Row1, 1.0);
        datas[6] = defaultValue(column1Row2, 0.0);
        datas[7] = defaultValue(column1Row3, 0.0);
        datas[8] = defaultValue(column2Row0, 0.0);
        datas[9] = defaultValue(column2Row1, 0.0);
        datas[10] = defaultValue(column2Row2, 1.0);
        datas[11] = defaultValue(column2Row3, 0.0);
        datas[12] = defaultValue(column3Row0, 0.0);
        datas[13] = defaultValue(column3Row1, 0.0);
        datas[14] = defaultValue(column3Row2, 0.0);
        datas[15] = defaultValue(column3Row3, 1.0);
    }

    get data(){
        return this._dataArray;
    }


    copy(matrix4: Matrix4) {
        let datas = this._dataArray;
        let matrixDatas = matrix4._dataArray;
        datas[0] = matrixDatas[0];
        datas[1] = matrixDatas[1];
        datas[2] = matrixDatas[2];
        datas[3] = matrixDatas[3];
        datas[4] = matrixDatas[4];
        datas[5] = matrixDatas[5];
        datas[6] = matrixDatas[6];
        datas[7] = matrixDatas[7];
        datas[8] = matrixDatas[8];
        datas[9] = matrixDatas[9];
        datas[10] = matrixDatas[10];
        datas[11] = matrixDatas[11];
        datas[12] = matrixDatas[12];
        datas[13] = matrixDatas[13];
        datas[14] = matrixDatas[14];
        datas[15] = matrixDatas[15];
        return this;
    }

    clone(){
        let ret= new Matrix4();
        ret.copy(this);
        return ret;
    }

    equals( matrix: Matrix4):boolean {

        if (!matrix) {
            return false;    
        }

		var te = this._dataArray;
		var me = matrix._dataArray;

		for ( var i = 0; i < 16; i ++ ) {
			if ( te[ i ] !== me[ i ] ) return false;
		}

		return true;

	}


    determinant(){
        let te = this._dataArray;

		let n11 = te[ 0 ], n12 = te[ 4 ], n13 = te[ 8 ], n14 = te[ 12 ];
		let n21 = te[ 1 ], n22 = te[ 5 ], n23 = te[ 9 ], n24 = te[ 13 ];
		let n31 = te[ 2 ], n32 = te[ 6 ], n33 = te[ 10 ], n34 = te[ 14 ];
		let n41 = te[ 3 ], n42 = te[ 7 ], n43 = te[ 11 ], n44 = te[ 15 ];

		//TODO: make this more efficient
		//( based on http://www.euclideanspace.com/maths/algebra/matrix/functions/inverse/fourD/index.htm )

		return (
			n41 * (
				+ n14 * n23 * n32
				 - n13 * n24 * n32
				 - n14 * n22 * n33
				 + n12 * n24 * n33
				 + n13 * n22 * n34
				 - n12 * n23 * n34
			) +
			n42 * (
				+ n11 * n23 * n34
				 - n11 * n24 * n33
				 + n14 * n21 * n33
				 - n13 * n21 * n34
				 + n13 * n24 * n31
				 - n14 * n23 * n31
			) +
			n43 * (
				+ n11 * n24 * n32
				 - n11 * n22 * n34
				 - n14 * n21 * n32
				 + n12 * n21 * n34
				 + n14 * n22 * n31
				 - n12 * n24 * n31
			) +
			n44 * (
				- n13 * n22 * n31
				 - n11 * n23 * n32
				 + n11 * n22 * n33
				 + n13 * n21 * n32
				 - n12 * n21 * n33
				 + n12 * n23 * n31
			)

		);
    }


    compose( position:Vector3, quaternion:Vector4, scale:Vector3 ) {

		let te = this._dataArray;

		let x = quaternion.x, y = quaternion.y, z = quaternion.z, w = quaternion.w;
		let x2 = x + x,	y2 = y + y, z2 = z + z;
		let xx = x * x2, xy = x * y2, xz = x * z2;
		let yy = y * y2, yz = y * z2, zz = z * z2;
		let wx = w * x2, wy = w * y2, wz = w * z2;

		let sx = scale.x, sy = scale.y, sz = scale.z;

	        te[ 0 ] = ( 1 - ( yy + zz ) ) * sx;
	        te[ 1 ] = ( xy + wz ) * sx;
	        te[ 2 ] = ( xz - wy ) * sx;
	        te[ 3 ] = 0;

	        te[ 4 ] = ( xy - wz ) * sy;
	        te[ 5 ] = ( 1 - ( xx + zz ) ) * sy;
	        te[ 6 ] = ( yz + wx ) * sy;
	        te[ 7 ] = 0;

	        te[ 8 ] = ( xz + wy ) * sz;
	        te[ 9 ] = ( yz - wx ) * sz;
	        te[ 10 ] = ( 1 - ( xx + yy ) ) * sz;
	        te[ 11 ] = 0;

	        te[ 12 ] = position.x;
	        te[ 13 ] = position.y;
	        te[ 14 ] = position.z;
	        te[ 15 ] = 1;

	        return this;

	}

    decompose(position:Vector3, quaternion:Vector4, scale:Vector3){

		let matrix = new Matrix4();
        let vector:Vector3 = Vector3.ZERO;
        
        let te = this._dataArray;

        let sx = vector.set( te[ 0 ], te[ 1 ], te[ 2 ] ).size();
        let sy = vector.set( te[ 4 ], te[ 5 ], te[ 6 ] ).size();
        let sz = vector.set( te[ 8 ], te[ 9 ], te[ 10 ] ).size();

        // if determine is negative, we need to invert one scale
        let det = this.determinant();
        if ( det < 0 ) sx = - sx;

        position.x = te[ 12 ];
        position.y = te[ 13 ];
        position.z = te[ 14 ];

        // scale the rotation part
        matrix.copy( this );

        var invSX = 1 / sx;
        var invSY = 1 / sy;
        var invSZ = 1 / sz;

        matrix.data[ 0 ] *= invSX;
        matrix.data[ 1 ] *= invSX;
        matrix.data[ 2 ] *= invSX;

        matrix.data[ 4 ] *= invSY;
        matrix.data[ 5 ] *= invSY;
        matrix.data[ 6 ] *= invSY;

        matrix.data[ 8 ] *= invSZ;
        matrix.data[ 9 ] *= invSZ;
        matrix.data[ 10 ] *= invSZ;

        //not Implemented
        //quaternion.setFromRotationMatrix( matrix );

        scale.x = sx;
        scale.y = sy;
        scale.z = sz;

        return this;
    }

    static fromArray(array:number[],result?:Matrix4){
        let ret = defaultValue(result,new Matrix4);
        let retDatas = ret._dataArray;
        retDatas[0] = array[0];
        retDatas[1] = array[1];
        retDatas[2] = array[2];
        retDatas[3] = array[3];
        retDatas[4] = array[4];
        retDatas[5] = array[5];
        retDatas[6] = array[6];
        retDatas[7] = array[7];
        retDatas[8] = array[8];
        retDatas[9] = array[9];
        retDatas[10] = array[10];
        retDatas[11] = array[11];
        retDatas[12] = array[12];
        retDatas[13] = array[13];
        retDatas[14] = array[14];
        retDatas[15] = array[15];    
        return  result;
    }

    static multiply(a: Matrix4, b: Matrix4): Matrix4 {
        let aDatas = a._dataArray;
        let left0 = aDatas[0];
        let left1 = aDatas[1];
        let left2 = aDatas[2];
        let left3 = aDatas[3];
        let left4 = aDatas[4];
        let left5 = aDatas[5];
        let left6 = aDatas[6];
        let left7 = aDatas[7];
        let left8 = aDatas[8];
        let left9 = aDatas[9];
        let left10 = aDatas[10];
        let left11 = aDatas[11];
        let left12 = aDatas[12];
        let left13 = aDatas[13];
        let left14 = aDatas[14];
        let left15 = aDatas[15];

        let bDatas = b._dataArray;
        let right0 = bDatas[0];
        let right1 = bDatas[1];
        let right2 = bDatas[2];
        let right3 = bDatas[3];
        let right4 = bDatas[4];
        let right5 = bDatas[5];
        let right6 = bDatas[6];
        let right7 = bDatas[7];
        let right8 = bDatas[8];
        let right9 = bDatas[9];
        let right10 = bDatas[10];
        let right11 = bDatas[11];
        let right12 = bDatas[12];
        let right13 = bDatas[13];
        let right14 = bDatas[14];
        let right15 = bDatas[15];

        let col0Row0 = left0 * right0 + left4 * right1 + left8 * right2 + left12 * right3;
        let col0Row1 = left1 * right0 + left5 * right1 + left9 * right2 + left13 * right3;
        let col0Row2 = left2 * right0 + left6 * right1 + left10 * right2 + left14 * right3;
        let col0Row3 = left3 * right0 + left7 * right1 + left11 * right2 + left15 * right3;

        let col1Row0 = left0 * right4 + left4 * right5 + left8 * right6 + left12 * right7;
        let col1Row1 = left1 * right4 + left5 * right5 + left9 * right6 + left13 * right7;
        let col1Row2 = left2 * right4 + left6 * right5 + left10 * right6 + left14 * right7;
        let col1Row3 = left3 * right4 + left7 * right5 + left11 * right6 + left15 * right7;

        let col2Row0 = left0 * right8 + left4 * right9 + left8 * right10 + left12 * right11;
        let col2Row1 = left1 * right8 + left5 * right9 + left9 * right10 + left13 * right11;
        let col2Row2 = left2 * right8 + left6 * right9 + left10 * right10 + left14 * right11;
        let col2Row3 = left3 * right8 + left7 * right9 + left11 * right10 + left15 * right11;

        let col3Row0 = left0 * right12 + left4 * right13 + left8 * right14 + left12 * right15;
        let col3Row1 = left1 * right12 + left5 * right13 + left9 * right14 + left13 * right15;
        let col3Row2 = left2 * right12 + left6 * right13 + left10 * right14 + left14 * right15;
        let col3Row3 = left3 * right12 + left7 * right13 + left11 * right14 + left15 * right15;

        return new Matrix4(col0Row0, col0Row1, col0Row2, col0Row3,
            col1Row0, col1Row1, col1Row2, col1Row3,
            col2Row0, col2Row1, col2Row2, col2Row3,
            col3Row0, col3Row1, col3Row2, col3Row3);

    }

    static add(left: Matrix4, right: Matrix4): Matrix4 {
        let leftDatas = left._dataArray;
        let rightDatas = right._dataArray;
        let result = new Matrix4;
        let retDatas = result._dataArray;
        retDatas[0] = leftDatas[0] + rightDatas[0];
        retDatas[1] = leftDatas[1] + rightDatas[1];
        retDatas[2] = leftDatas[2] + rightDatas[2];
        retDatas[3] = leftDatas[3] + rightDatas[3];
        retDatas[4] = leftDatas[4] + rightDatas[4];
        retDatas[5] = leftDatas[5] + rightDatas[5];
        retDatas[6] = leftDatas[6] + rightDatas[6];
        retDatas[7] = leftDatas[7] + rightDatas[7];
        retDatas[8] = leftDatas[8] + rightDatas[8];
        retDatas[9] = leftDatas[9] + rightDatas[9];
        retDatas[10] = leftDatas[10] + rightDatas[10];
        retDatas[11] = leftDatas[11] + rightDatas[11];
        retDatas[12] = leftDatas[12] + rightDatas[12];
        retDatas[13] = leftDatas[13] + rightDatas[13];
        retDatas[14] = leftDatas[14] + rightDatas[14];
        retDatas[15] = leftDatas[15] + rightDatas[15];
        return result;
    }


    static subtract(left: Matrix4, right: Matrix4): Matrix4 {
        let leftDatas = left._dataArray;
        let rightDatas = right._dataArray;
        let result = new Matrix4;
        let retDatas = result._dataArray;
        retDatas[0] = leftDatas[0] - rightDatas[0];
        retDatas[1] = leftDatas[1] - rightDatas[1];
        retDatas[2] = leftDatas[2] - rightDatas[2];
        retDatas[3] = leftDatas[3] - rightDatas[3];
        retDatas[4] = leftDatas[4] - rightDatas[4];
        retDatas[5] = leftDatas[5] - rightDatas[5];
        retDatas[6] = leftDatas[6] - rightDatas[6];
        retDatas[7] = leftDatas[7] - rightDatas[7];
        retDatas[8] = leftDatas[8] - rightDatas[8];
        retDatas[9] = leftDatas[9] - rightDatas[9];
        retDatas[10] = leftDatas[10] - rightDatas[10];
        retDatas[11] = leftDatas[11] - rightDatas[11];
        retDatas[12] = leftDatas[12] - rightDatas[12];
        retDatas[13] = leftDatas[13] - rightDatas[13];
        retDatas[14] = leftDatas[14] - rightDatas[14];
        retDatas[15] = leftDatas[15] - rightDatas[15];
        return result;
    }


    static multiplyByScale(left: Matrix4, scale: Vector3): Matrix4 {

        let scaleX = scale.x;
        let scaleY = scale.y;
        let scaleZ = scale.z;

        // Faster than Cartesian3.equals
        // if ((scaleX === 1.0) && (scaleY === 1.0) && (scaleZ === 1.0)) {
        //     return Matrix4.clone(matrix, result);
        // }
        let result = new Matrix4;
        let retDatas = result._dataArray;
        let leftDatas = left._dataArray;
        retDatas[0] = scaleX * leftDatas[0];
        retDatas[1] = scaleX * leftDatas[1];
        retDatas[2] = scaleX * leftDatas[2];
        retDatas[3] = 0.0;
        retDatas[4] = scaleY * leftDatas[4];
        retDatas[5] = scaleY * leftDatas[5];
        retDatas[6] = scaleY * leftDatas[6];
        retDatas[7] = 0.0;
        retDatas[8] = scaleZ * leftDatas[8];
        retDatas[9] = scaleZ * leftDatas[9];
        retDatas[10] = scaleZ * leftDatas[10];
        retDatas[11] = 0.0;
        retDatas[12] = leftDatas[12];
        retDatas[13] = leftDatas[13];
        retDatas[14] = leftDatas[14];
        retDatas[15] = 1.0;
        return result;

    }

    static negate(matrix: Matrix4): Matrix4 {
        let result = new Matrix4;
        let retDatas = result._dataArray;
        let matrixDatas = matrix._dataArray;
        retDatas[0] = -matrixDatas[0];
        retDatas[1] = -matrixDatas[1];
        retDatas[2] = -matrixDatas[2];
        retDatas[3] = -matrixDatas[3];
        retDatas[4] = -matrixDatas[4];
        retDatas[5] = -matrixDatas[5];
        retDatas[6] = -matrixDatas[6];
        retDatas[7] = -matrixDatas[7];
        retDatas[8] = -matrixDatas[8];
        retDatas[9] = -matrixDatas[9];
        retDatas[10] = -matrixDatas[10];
        retDatas[11] = -matrixDatas[11];
        retDatas[12] = -matrixDatas[12];
        retDatas[13] = -matrixDatas[13];
        retDatas[14] = -matrixDatas[14];
        retDatas[15] = -matrixDatas[15];
        return result;
    }

    /**
     * reference to three.js
     * @param eye 
     * @param target 
     * @param up 
     */
    static lookat(eye:Vector3, target:Vector3, up:Vector3):Matrix4{
        let vecZ:Vector3 = new Vector3;
        let vecX:Vector3 = new Vector3;
        let vecY:Vector3 = new Vector3;

        let result = new Matrix4;
        let retDatas = result._dataArray;
        Vector3.sub(eye,target,vecZ);

        if ( Vector3.sizeSquare(vecZ) === 0 ) {

            // eye and target are in the same position
            vecZ.z = 1;
        }

        vecZ.normalize();
        //Vector3.normalize(vecZ,vecZ);
        Vector3.cross(up,vecZ,vecX);

        if ( Vector3.sizeSquare(vecX) === 0 ) {

            // up and z are parallel

            if ( Math.abs( up.z ) === 1 ) {

                vecZ.x += 0.0001;

            } else {

                vecZ.z += 0.0001;

            }

            vecZ.normalize();
            //Vector3.normalize(vecZ,vecZ);
            Vector3.cross(up,vecZ,vecX);

        }

        vecX.normalize();
        //Vector3.normalize(vecX,vecX);
        Vector3.cross(vecZ,vecX,vecY);

        retDatas[ 0 ] = vecX.x; retDatas[ 4 ] = vecY.x; retDatas[ 8 ] = vecZ.x;
        retDatas[ 1 ] = vecX.y; retDatas[ 5 ] = vecY.y; retDatas[ 9 ] = vecZ.y;
        retDatas[ 2 ] = vecX.z; retDatas[ 6 ] = vecY.z; retDatas[ 10 ] = vecZ.z;
        
        return result;

    }


    /**
     * reference to gl-matrix
     * @param eye 
     * @param target 
     * @param up 
     * @param result 
     */
    static lookAt2(eye:Vector3, target:Vector3, up:Vector3,result?:Matrix4) {
        let ret:Matrix4 = defaultValue(result,new Matrix4);

        var x0, x1, x2, y0, y1, y2, z0, z1, z2, len;
        var eyex = eye.x;
        var eyey = eye.y;
        var eyez = eye.z;
        var upx = up.x;
        var upy = up.y;
        var upz = up.z;
        var centerx = target.x;
        var centery = target.y;
        var centerz = target.z;
        var EPSILON = 0.000001;
    
        if (Math.abs(eyex - centerx) < EPSILON && Math.abs(eyey - centery) < EPSILON && Math.abs(eyez - centerz) < EPSILON) {
            return Matrix4.IDENTITY;
        }
    
        z0 = eyex - centerx;
        z1 = eyey - centery;
        z2 = eyez - centerz;
        len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
        z0 *= len;
        z1 *= len;
        z2 *= len;
        x0 = upy * z2 - upz * z1;
        x1 = upz * z0 - upx * z2;
        x2 = upx * z1 - upy * z0;
        len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
    
        if (!len) {
          x0 = 0;
          x1 = 0;
          x2 = 0;
        } else {
          len = 1 / len;
          x0 *= len;
          x1 *= len;
          x2 *= len;
        }
    
        y0 = z1 * x2 - z2 * x1;
        y1 = z2 * x0 - z0 * x2;
        y2 = z0 * x1 - z1 * x0;
        len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
    
        if (!len) {
          y0 = 0;
          y1 = 0;
          y2 = 0;
        } else {
          len = 1 / len;
          y0 *= len;
          y1 *= len;
          y2 *= len;
        }
    
        let retDatas = ret.data;
        retDatas[0] = x0;
        retDatas[1] = y0;
        retDatas[2] = z0;
        retDatas[3] = 0;
        retDatas[4] = x1;
        retDatas[5] = y1;
        retDatas[6] = z1;
        retDatas[7] = 0;
        retDatas[8] = x2;
        retDatas[9] = y2;
        retDatas[10] = z2;
        retDatas[11] = 0;
        retDatas[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
        retDatas[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
        retDatas[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
        retDatas[15] = 1;
        return ret;
      }


    /**
     * Generates a perspective projection matrix
     * @param fovy  the radian of fov
     * @param aspect the aspect ratio of the frustum.(width/height)
     * @param near the distance to the nearer depth clipping plane
     * @param far the distance to the farther depth clipping plane
     */
    static perspective(fovy:number, aspect:number, near:number, far:number) {

        let result = new Matrix4;
        let retDatas = result._dataArray;
        var f = 1.0 / Math.tan(fovy / 2),
            nf;
        retDatas[0] = f / aspect;
        retDatas[1] = 0;
        retDatas[2] = 0;
        retDatas[3] = 0;
        retDatas[4] = 0;
        retDatas[5] = f;
        retDatas[6] = 0;
        retDatas[7] = 0;
        retDatas[8] = 0;
        retDatas[9] = 0;
        retDatas[11] = -1;
        retDatas[12] = 0;
        retDatas[13] = 0;
        retDatas[15] = 0;
    
        if (far != null && far !== Infinity) {
          nf = 1 / (near - far);
          retDatas[10] = (far + near) * nf;
          retDatas[14] = 2 * far * near * nf;
        } else {
            retDatas[10] = -1;
            retDatas[14] = -2 * near;
        }
    
        return result;

      }

    //abs
    //getRotation
    //inverse
    //IDENTITY
    //ZERO

}