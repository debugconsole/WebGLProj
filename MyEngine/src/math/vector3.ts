import { defaultValue } from "../core/defaultvalue"


export class Vector3{

    private _x:number;
    private _y:number;
    private _z:number;

    static get ZERO(){
        return new Vector3(0,0,0);
    }

    constructor(x:number = 0,y:number = 0,z:number = 0){
        this._x = x;
        this._y = y;
        this._z = z;
    }

    get x(){return this._x;}
    get y(){return this._y;}
    get z(){return this._z;}
    set x(val:number){this._x = val};
    set y(val:number){this._y = val};
    set z(val:number){this._z = val};

    tostring():string{
        return `[${this._x},${this._y},${this._z}]`
    }

    set(x:number,y:number,z:number):Vector3{
        this._x = x || 0;
        this._y = y || 0;
        this._z = z || 0;
        return this;
    }

    clone(){
        return new Vector3(this._x,this._y,this._z);
    }


    static add(left:Vector3,right:Vector3,result:Vector3):Vector3{

        let ret = defaultValue(result,new Vector3(0,0,0));
        ret._x = left._x + right._x;
        ret._y = left._y + right._y;
        ret._z = left._z + right._z;
        
        return ret;

    }

    toArray(){
        let ret = [];
        ret[0] = this._x;
        ret[1] = this._y;
        ret[2] = this._z;
        return ret;
    }

    static fromArray(array:number[], offset?:number, result?:Vector3):Vector3{
        let ret = defaultValue(result,new Vector3(0,0,0));

		if ( offset === undefined ) offset = 0;
		ret._x = array[ offset ];
		ret._y = array[ offset + 1 ];
        ret._z = array[ offset + 2 ];
        
        return ret;

    }

    static cross(left:Vector3,right:Vector3,result:Vector3):Vector3{

        let ret = defaultValue(result,new Vector3(0,0,0));
        var leftX = left.x;
        var leftY = left.y;
        var leftZ = left.z;
        var rightX = right.x;
        var rightY = right.y;
        var rightZ = right.z;

        ret._x = leftY * rightZ - leftZ * rightY;
        ret._y = leftZ * rightX - leftX * rightZ;
        ret._z = leftX * rightY - leftY * rightX;
        
        return ret;

    }

    static addScalar(left:Vector3,s:number,result:Vector3):Vector3{
        let ret = defaultValue(result,new Vector3(0,0,0));
        ret._x = left._x + s;
        ret._y = left._y + s;
        ret._z = left._z + s;
        return ret;
    }

    static sub(left:Vector3,right:Vector3,result:Vector3):Vector3{
        let ret = defaultValue(result,new Vector3(0,0,0));
        ret._x = left._x - right._x;
        ret._y = left._y - right._y;
        ret._z = left._z - right._z;
        return ret;
    }

    static subScalar(left:Vector3,s:number,result:Vector3){
        let ret = defaultValue(result,new Vector3(0,0,0));
        ret._x = left._x - s;
        ret._y = left._y - s;
        ret._z = left._z - s;
        return ret;
    }


    static multiply(left:Vector3,right:Vector3,result:Vector3):Vector3{
        let ret = defaultValue(result,new Vector3(0,0,0));
        ret._x = left._x * right._x;
        ret._y = left._y * right._y;
        ret._z = left._z * right._z;
		return ret;
    }

    static multiplyScalar(left:Vector3,s:number,result:Vector3):Vector3{
        let ret = defaultValue(result,new Vector3(0,0,0));
        ret._x = left._x * s;
        ret._y = left._y * s;
        ret._z = left._z * s;
		return ret;
    }

    static divide(left:Vector3,t:Vector3,result:Vector3):Vector3{
        let ret = defaultValue(result,new Vector3(0,0,0));
        ret._x = left._x / t._x;
        ret._y = left._y / t._y;
        ret._z = left._z / t._z;
		return ret;
    }

    divideScalar(s:number):Vector3{

         this._x = this._x / s;
         this._y = this._y / s;
         this._z = this._z / s;

		return this;
    }

    static negate(vec3:Vector3,result:Vector3){
        let ret = defaultValue(result,new Vector3(0,0,0));
        ret._x = -vec3._x;
        ret._y = -vec3._y;
        ret._z = -vec3._z;
		return ret;
    }

    static dot(left:Vector3,right:Vector3):number{
        return left._x * right._x + left._y * right._y + left._z * right._z;
    }

    static sizeSquare(vec3:Vector3):number{
        return vec3._x * vec3._x + vec3._y * vec3._y + vec3._z * vec3._z;
    }

    size():number{
        return Math.sqrt( this._x * this._x + this._y * this._y + this._z * this._z );
    }

    normalize() {
		return this.divideScalar( this.size() || 1 );
    }

    static distanceToSquare( left:Vector3 , right:Vector3 ){
        var dx = left._x - right._x, dy = left._y - right._y, dz = left._z - right._z;
        return dx * dx + dy * dy + dz * dz;
    }

    static distanceTo(left:Vector3 , right:Vector3){
        return Math.sqrt(Vector3.distanceToSquare(left,right));
    }

}