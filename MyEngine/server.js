var express = require('express');

var exp= express();

exp.use(express.static('bin'));
exp.use(express.static('data'));
exp.use(express.static('bin/examples'));

exp.get('/', function (req, res) {
    //res.redirect( './index.html');
    //res.send('Hello World');
    res.render('index',{
        title:'imooc 首页'
    });
});

exp.get('/index', function (req, res) {
    //res.redirect( './index.html');
    //res.send('Hello World');
    res.render('index',{
        title:'imooc 首页'
    });
});

var server =exp.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("应用实例，访问地址为 http://%s:%s", host, port)
})
