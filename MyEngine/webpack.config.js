const path = require('path');
function resolve (dir) {
    return path.join(__dirname, '..', dir)
}
module.exports = {
    mode: 'development',

    entry: './src/main.ts',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'bin')
    },

    module: {
        rules: [{
            test: /\.ts$/,
            loader: "ts-loader",
            exclude: /node_modules/,
            options: {
                // 指定特定的ts编译配置，为了区分脚本的ts配置
                configFile: path.resolve(__dirname, './tsconfig.json'),
            },
        },{
            test:/\.js$/,use:"source-map-loader"
        }]
    },
    resolve: {
        extensions: [
            '.ts'
        ],
        alias:{
            '@':resolve('src')
        }
    },
    devtool: "source-map",

};