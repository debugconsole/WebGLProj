/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/main.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/core/camera.ts":
/*!****************************!*\
  !*** ./src/core/camera.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.PerspectiveCamera = exports.Camera = void 0;
const matrix4_1 = __webpack_require__(/*! ../math/matrix4 */ "./src/math/matrix4.ts");
const scenenode_1 = __webpack_require__(/*! ./scenenode */ "./src/core/scenenode.ts");
class Camera extends scenenode_1.SceneNode {
    constructor(scene) {
        super();
        this._viewMatrix = matrix4_1.Matrix4.IDENTITY;
        this._projectionMatrix = matrix4_1.Matrix4.IDENTITY;
        this.upVector = undefined;
        this.lookAt = undefined;
        this.scene = undefined;
        this.scene = scene;
    }
    get viewMatrix() {
        return this._viewMatrix;
    }
    get projectionMatrix() {
        return this._projectionMatrix;
    }
    updateMatrix() {
        if (this.upVector && this.lookAt) {
            this._viewMatrix = matrix4_1.Matrix4.lookAt2(this.position, this.lookAt, this.upVector);
        }
    }
}
exports.Camera = Camera;
class PerspectiveCamera extends Camera {
    constructor(scene, fov, aspect, near, far) {
        super(scene);
        this.fovy = fov;
        this.aspect = aspect;
        this.near = near;
        this.far = far;
        this.addUpdateListener((cam) => {
            cam.updateMatrix();
        });
    }
    updateMatrix() {
        super.updateMatrix();
        this._projectionMatrix = matrix4_1.Matrix4.perspective((this.fovy / 180) * Math.PI, this.aspect, this.near, this.far);
    }
}
exports.PerspectiveCamera = PerspectiveCamera;


/***/ }),

/***/ "./src/core/defaultvalue.ts":
/*!**********************************!*\
  !*** ./src/core/defaultvalue.ts ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultValue = void 0;
exports.defaultValue = (a, b) => {
    if (a !== undefined && a !== null) {
        return a;
    }
    return b;
};


/***/ }),

/***/ "./src/core/engine.ts":
/*!****************************!*\
  !*** ./src/core/engine.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Engine = void 0;
const viewer_1 = __webpack_require__(/*! ./viewer */ "./src/core/viewer.ts");
const helper_1 = __webpack_require__(/*! ../utils/helper */ "./src/utils/helper.ts");
const fpsstate_1 = __webpack_require__(/*! ../utils/fpsstate */ "./src/utils/fpsstate.ts");
const renderer_1 = __webpack_require__(/*! ../render/renderer */ "./src/render/renderer.ts");
class Engine {
    constructor() {
        this._viewerList = [];
    }
    static get singleton() {
        if (!Engine._singleton) {
            Engine._singleton = new Engine();
        }
        return Engine._singleton;
    }
    init() {
        this.setEnableStatsPanel(true);
        this._startRenderLoop();
    }
    setEnableStatsPanel(enabled) {
        if (enabled) {
            let statsConstruct = fpsstate_1.Stats;
            this.stats = new statsConstruct();
            document.body.appendChild(this.stats.dom);
        }
    }
    addViewer(canvas, viewer, priority) {
        canvas.addViewer(viewer, priority);
        this._bindSceneChangeToRender(viewer);
    }
    createDefaultView(container) {
        let canvas = document.createElement('canvas');
        canvas.style.position = 'absolute';
        canvas.style.left = '0';
        canvas.style.top = '0';
        canvas.style.width = '100%';
        canvas.style.height = '100%';
        canvas.style.zIndex = '-1';
        container.appendChild(canvas);
        let gl = canvas.getContext('webgl');
        let renderer = new renderer_1.Renderer(gl);
        let viewer = new viewer_1.Viewer({ renderer, container });
        this._viewerList.push(viewer);
        return viewer;
    }
    updateSceneEntityToRenderer(viewer) {
        console.log("scene change......." + viewer.scene.children.length);
        viewer.renderer.clearRenderGroup();
        this._updateNodeToRenderer(viewer, viewer.scene);
    }
    _updateNodeToRenderer(viewer, node) {
        node.children.forEach(childnode => {
            if (childnode.type === "Entity") {
                let ent = childnode;
                console.log("add ent...");
                ent.drawableObjs.forEach(drawableObj => {
                    viewer.renderer.addDrawAbleObj(drawableObj, ent.renderType);
                });
            }
            if (childnode.children && childnode.children.length > 0) {
                this._updateNodeToRenderer(viewer, childnode);
            }
        });
    }
    _bindSceneChangeToRender(viewer) {
        viewer.scene.addOnChangeListener(scene => {
            this.updateSceneEntityToRenderer(viewer);
        });
        this.updateSceneEntityToRenderer(viewer);
    }
    _startRenderLoop() {
        console.log("----start scene render loop----");
        try {
            let requestUpdateFrame = window.requestAnimationFrame;
            let renderMain = (timestamp) => {
                this._renderAFrame(timestamp);
                if (this.stats) {
                    this.stats.update();
                }
                requestUpdateFrame(renderMain);
            };
            requestUpdateFrame(renderMain);
        }
        catch (excp) {
            console.log("EXCP:" + excp);
        }
    }
    _renderAFrame(timestamp) {
        console.log("render a frame----------------------");
        let viewers = this._viewerList;
        for (let index = 0, len = viewers.length; index < len; index++) {
            const element = viewers[index];
            this._renderAView(element);
        }
    }
    _renderAView(viewer) {
        let start = performance.now();
        console.log("viewer name:" + viewer.name);
        viewer.eventBeforeRender.fire();
        let camera = viewer.camera;
        if (camera) {
            camera.updateMatrix();
        }
        viewer.scene.update();
        if (camera) {
            viewer.scene.updatePositionTransform(camera.viewMatrix, camera.projectionMatrix);
        }
        helper_1.Helper.calculateTimestamp(() => {
            viewer.renderer.clear();
            viewer.renderer.render();
        }, "1");
        viewer.eventAfterRender.fire();
        let diffMillSeconds = performance.now() - start;
        let fps = 1000 / diffMillSeconds;
    }
}
exports.Engine = Engine;
Engine.gViewerPriority = 0;


/***/ }),

/***/ "./src/core/entity.ts":
/*!****************************!*\
  !*** ./src/core/entity.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Entity = void 0;
const drawobject_1 = __webpack_require__(/*! ../render/drawobject */ "./src/render/drawobject.ts");
const renderer_1 = __webpack_require__(/*! ../render/renderer */ "./src/render/renderer.ts");
const scenenode_1 = __webpack_require__(/*! ./scenenode */ "./src/core/scenenode.ts");
const builtinprogram_1 = __webpack_require__(/*! ../render/builtinprogram */ "./src/render/builtinprogram.ts");
const matrix4_1 = __webpack_require__(/*! ../math/matrix4 */ "./src/math/matrix4.ts");
class Entity extends scenenode_1.SceneNode {
    constructor(renderType) {
        super();
        this.drawableObjs = [];
        this.mesh = undefined;
        this.renderType = renderType || renderer_1.ERenderGroupType.Opaque;
        this.type = "Entity";
    }
    updatePositionTransform(viewMatrix, projectMatrix) {
        let pvMatrix = matrix4_1.Matrix4.multiply(projectMatrix, viewMatrix);
        let transformMatrix = matrix4_1.Matrix4.multiply(pvMatrix, this.worldMatrix);
        if (this._currentTransform === undefined || !this._currentTransform.equals(transformMatrix)) {
            this._currentTransform = transformMatrix;
            console.log("transformdata:" + transformMatrix.data);
            this.drawableObjs.forEach(drawObj => {
                drawObj.uniforms["transform"] = transformMatrix.data;
            });
        }
    }
    setUniform(uniform, data) {
        this.drawableObjs.forEach(drawObj => {
            drawObj.uniforms[uniform] = data;
        });
    }
    static createEntFromDrawableObj(drawableObj, renderType) {
        let ent = new Entity(renderType);
        ent.drawableObjs.push(drawableObj);
        let indicesVertexProperty = drawableObj.vertexInfo.indices;
        let positionVertexProperty = drawableObj.vertexInfo.position;
        if (indicesVertexProperty) {
            drawableObj.drawArrayCount = indicesVertexProperty.buffer.length / indicesVertexProperty.numComponents;
            drawableObj.drawArrayOffset = 0;
        }
        else {
            drawableObj.drawArrayCount = positionVertexProperty.buffer.length / positionVertexProperty.numComponents;
            drawableObj.drawArrayOffset = 0;
        }
        return ent;
    }
    static createEntFromMesh(mesh) {
        let ent = new Entity();
        mesh.primitives.forEach((primitive, key, map) => {
            console.log("create drawobj");
            let drawObj = new drawobject_1.DrawableObj();
            drawObj.vertexInfo = primitive.geometry.vertexInfo;
            drawObj.drawArrayCount = primitive.geometry.drawCount;
            drawObj.drawArrayOffset = primitive.geometry.drawOffset;
            drawObj.drawType = primitive.drawMode;
            drawObj.programInfo = builtinprogram_1.BuiltInProgram.createNormalProgram();
            ent.drawableObjs.push(drawObj);
        });
        ent.mesh = mesh;
        return ent;
    }
}
exports.Entity = Entity;


/***/ }),

/***/ "./src/core/events.ts":
/*!****************************!*\
  !*** ./src/core/events.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Event = void 0;
class Event {
    constructor() {
        this.events = new Set();
    }
    onFire(callback) {
        if (!this.events.has(callback)) {
            this.events.add(callback);
        }
        console.log('onEvent:', this.events);
    }
    onFireOnce(callback) {
        const oneCall = (a) => __awaiter(this, void 0, void 0, function* () {
            yield callback(a);
            this.off(oneCall);
        });
        this.onFire(oneCall);
    }
    fire(arg) {
        this.events.forEach(cb => {
            cb(arg);
        });
    }
    off(callback) {
        if (this.events.has(callback)) {
            this.events.delete(callback);
        }
    }
    hasEvent(callback) {
        return this.events.has(callback);
    }
}
exports.Event = Event;


/***/ }),

/***/ "./src/core/mesh.ts":
/*!**************************!*\
  !*** ./src/core/mesh.ts ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Mesh = void 0;
class Mesh {
    constructor() {
        this.primitives = new Map();
    }
    addPrimitive(primitive) {
        if (!primitive) {
            return;
        }
        this.primitives.set(primitive, primitive);
    }
    removePrimitive(primitive) {
        if (!primitive) {
            return;
        }
        this.primitives.delete(primitive);
    }
    copy(source) {
        this.primitives.clear();
        source.primitives.forEach(primitive => {
            this.addPrimitive(primitive.clone());
        });
    }
    clone() {
        let ret = new Mesh();
        ret.copy(this);
        return ret;
    }
}
exports.Mesh = Mesh;


/***/ }),

/***/ "./src/core/primitive.ts":
/*!*******************************!*\
  !*** ./src/core/primitive.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Primitive = void 0;
class Primitive {
    constructor(geometry, material, drawMode) {
        this.geometry = geometry;
        this.material = material;
        this.drawMode = drawMode;
    }
    clone() {
        let ret = new Primitive();
        ret.copy(this);
        return ret;
    }
    copy(source) {
        this.geometry = source.geometry.clone();
        this.material = source.material.clone();
        this.drawMode = source.drawMode;
        this.name = source.name;
    }
}
exports.Primitive = Primitive;


/***/ }),

/***/ "./src/core/rendertarget.ts":
/*!**********************************!*\
  !*** ./src/core/rendertarget.ts ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.RenderTarget = void 0;
const glcontext_1 = __webpack_require__(/*! ../render/glcontext */ "./src/render/glcontext.ts");
const texture_1 = __webpack_require__(/*! ./texture */ "./src/core/texture.ts");
const vector4_1 = __webpack_require__(/*! ../math/vector4 */ "./src/math/vector4.ts");
class RenderTarget {
    static createTextureRenderTarget(width, height) {
        try {
            let gl = glcontext_1.GLContext.gl;
            let ret = new RenderTarget();
            ret.rttFramebuffer = gl.createFramebuffer();
            let rttFramebuffer = ret.rttFramebuffer;
            gl.bindFramebuffer(gl.FRAMEBUFFER, rttFramebuffer);
            let glTexture = texture_1.Texture.createEmptyTexture(width, height).texture;
            var renderbuffer = gl.createRenderbuffer();
            gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
            gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, width, height);
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, glTexture, 0);
            gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);
            gl.bindTexture(gl.TEXTURE_2D, null);
            gl.bindRenderbuffer(gl.RENDERBUFFER, null);
            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
            return ret;
        }
        catch (err) {
            console.error("createTextureRenderTarget ERROR:" + err);
            throw err;
        }
    }
    pickColorFromRenderTarget(screenX, ScreenY) {
        let gl = glcontext_1.GLContext.gl;
        let color;
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.rttFramebuffer);
        let pixelData = new Uint8Array(4 * 1);
        gl.readPixels(screenX, ScreenY, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixelData);
        if (pixelData && pixelData.length) {
            let length = 4 * 1;
            color = new vector4_1.Vector4(pixelData[0], pixelData[1], pixelData[2], pixelData[3]);
        }
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        return color;
    }
    beginRenderToTarget() {
        console.log("begin render to FBO");
        let gl = glcontext_1.GLContext.gl;
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.rttFramebuffer);
    }
    endRenderToTarget() {
        console.log("end render to FBO");
        let gl = glcontext_1.GLContext.gl;
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }
}
exports.RenderTarget = RenderTarget;


/***/ }),

/***/ "./src/core/resourcemgr.ts":
/*!*********************************!*\
  !*** ./src/core/resourcemgr.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.ResourceManager = void 0;
class ResourceManager {
    constructor() {
        this.shaderCache = {};
    }
    static getSingleton() {
        if (!ResourceManager.singleton) {
            ResourceManager.singleton = new ResourceManager();
        }
        return ResourceManager.singleton;
    }
    loadShaders(shaderFileList) {
        return new Promise((resolve, reject) => {
            let handleCount = 0;
            let isfinished = () => {
                if (handleCount >= shaderFileList.length) {
                    resolve(this.shaderCache);
                }
            };
            shaderFileList.forEach(fileName => {
                this._fetchResource(fileName).then(resp => {
                    if (resp.ok) {
                        resp.text().then(value => {
                            this.shaderCache[fileName] = value;
                            handleCount += 1;
                            isfinished();
                        });
                    }
                    else {
                        console.log(`load shader false ${fileName}`);
                        handleCount += 1;
                        isfinished();
                    }
                }, reason => {
                    console.log(`load shader false ${fileName}: ${reason}`);
                    handleCount += 1;
                    isfinished();
                }).catch(e => {
                    reject(e);
                });
            });
        });
    }
    _fetchResource(resPath) {
        return fetch(resPath, {
            method: 'get',
            headers: new Headers({
                'Conten-Type': 'text/plain'
            }),
        });
    }
}
exports.ResourceManager = ResourceManager;


/***/ }),

/***/ "./src/core/scene.ts":
/*!***************************!*\
  !*** ./src/core/scene.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Scene = void 0;
const scenenode_1 = __webpack_require__(/*! ./scenenode */ "./src/core/scenenode.ts");
class Scene extends scenenode_1.SceneNode {
    constructor() {
        super();
        this.type = "Scene";
    }
    updatePositionTransform(viewMatrix, projectMatrix, node) {
        let children = node ? node.children : this.children;
        if (children) {
            children.forEach(node => {
                if (node.type === "Entity") {
                    node.updatePositionTransform(viewMatrix, projectMatrix);
                }
                this.updatePositionTransform(viewMatrix, projectMatrix, node);
            });
        }
    }
}
exports.Scene = Scene;


/***/ }),

/***/ "./src/core/scenenode.ts":
/*!*******************************!*\
  !*** ./src/core/scenenode.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.SceneNode = void 0;
const matrix4_1 = __webpack_require__(/*! ../math/matrix4 */ "./src/math/matrix4.ts");
const utils_1 = __webpack_require__(/*! ./utils */ "./src/core/utils.ts");
const vector3_1 = __webpack_require__(/*! ../math/vector3 */ "./src/math/vector3.ts");
const vector4_1 = __webpack_require__(/*! ../math/vector4 */ "./src/math/vector4.ts");
class SceneNode {
    constructor(parent) {
        this.type = "SceneNode";
        this._position = vector3_1.Vector3.ZERO;
        this._quaternion = vector4_1.Vector4.ZERO;
        this._scale = vector3_1.Vector3.ZERO;
        this.matrix = matrix4_1.Matrix4.IDENTITY;
        this.worldMatrix = matrix4_1.Matrix4.IDENTITY;
        this.children = [];
        this.onChange = [];
        this._updateListenerArray = [];
        this.id = SceneNode.nodeID++;
        this.parent = parent;
    }
    get position() {
        return this._position;
    }
    set position(value) {
        this._position = value;
        this._updateTransform();
    }
    get quaternion() {
        return this._quaternion;
    }
    set quaternion(value) {
        this._quaternion = value;
        this._updateTransform();
    }
    get scale() {
        return this._scale;
    }
    set scale(value) {
        this._scale = value;
        this._updateTransform();
    }
    copyFrom(source) {
        let ret = utils_1.Utils.deepCopy(source);
        this.id = source.id;
        this.name = source.name;
        this.position = source.position;
        this.parent = source.parent;
        return ret;
    }
    clone() {
        let ret = new SceneNode();
        ret.copyFrom(this);
        return ret;
    }
    addUpdateListener(listener) {
        if (listener) {
            this._updateListenerArray.push(listener);
        }
    }
    update() {
        this._updateListenerArray.forEach(listener => {
            listener(this);
        });
        let children = this.children;
        if (children && children.length > 0) {
            children.forEach(node => {
                node.update();
            });
        }
    }
    traverse(callback) {
        if (callback) {
            callback(this);
        }
        let children = this.children;
        if (children && children.length > 0) {
            children.forEach(node => {
                node.traverse(callback);
            });
        }
    }
    addNode(node) {
        if (!node) {
            console.error("node isn't a scenenode");
            return;
        }
        console.log("addNode..");
        this.children.push(node);
        node.parent = this;
        if (this.onChange) {
            console.log("onchange..........:" + this.onChange.length);
            this.onChange.forEach(listener => {
                listener(this);
            });
        }
    }
    addOnChangeListener(callback) {
        let index = this.onChange.length;
        this.onChange.push(callback);
        return index;
    }
    removeOnChangeListener(index) {
        this.onChange.slice(index, 1);
    }
    _updateTransform() {
        this.matrix.compose(this._position, this._quaternion, this._scale);
        let parent = this.parent;
        this.worldMatrix = this.matrix;
        while (parent) {
            this.worldMatrix = matrix4_1.Matrix4.multiply(parent.matrix, this.worldMatrix);
            parent = parent.parent;
        }
    }
    applyMatrix(matrix) {
        this.matrix = matrix4_1.Matrix4.multiply(matrix, this.matrix);
        this.matrix.decompose(this.position, this.quaternion, this.scale);
    }
}
exports.SceneNode = SceneNode;
SceneNode.nodeID = 0;


/***/ }),

/***/ "./src/core/texture.ts":
/*!*****************************!*\
  !*** ./src/core/texture.ts ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Texture = void 0;
const glcontext_1 = __webpack_require__(/*! ../render/glcontext */ "./src/render/glcontext.ts");
class Texture {
    constructor(texture) {
        this.texture = texture;
    }
    _isPowerOf2(value) {
        return (value & (value - 1)) === 0;
    }
    static createTexture(image) {
        let gl = glcontext_1.GLContext.gl;
        let texture = gl.createTexture();
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);
        gl.bindTexture(gl.TEXTURE_2D, null);
        return new Texture(texture);
    }
    static createEmptyTexture(width, height, border = 0) {
        let gl = glcontext_1.GLContext.gl;
        let texture = gl.createTexture();
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, width, height, border, gl.RGB, gl.UNSIGNED_BYTE, null);
        gl.bindTexture(gl.TEXTURE_2D, null);
        return new Texture(texture);
    }
}
exports.Texture = Texture;


/***/ }),

/***/ "./src/core/utils.ts":
/*!***************************!*\
  !*** ./src/core/utils.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Utils = void 0;
class Utils {
    static deepCopy(o) {
        if (o instanceof Array) {
            let n = [];
            for (let i = 0; i < o.length; ++i) {
                n[i] = Utils.deepCopy(o[i]);
            }
            return n;
        }
        else if (o instanceof Object) {
            let n = {};
            for (let i in o) {
                n[i] = Utils.deepCopy(o[i]);
            }
            return n;
        }
        else {
            return o;
        }
    }
}
exports.Utils = Utils;


/***/ }),

/***/ "./src/core/viewer.ts":
/*!****************************!*\
  !*** ./src/core/viewer.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Viewer = void 0;
const vector4_1 = __webpack_require__(/*! ../math/vector4 */ "./src/math/vector4.ts");
const camera_1 = __webpack_require__(/*! ./camera */ "./src/core/camera.ts");
const scene_1 = __webpack_require__(/*! ./scene */ "./src/core/scene.ts");
const events_1 = __webpack_require__(/*! ./events */ "./src/core/events.ts");
let defaultOptions = {
    width: 1280,
    height: 720,
    camera: null,
    xoffset: 0,
    yoffset: 0,
};
class Viewer {
    constructor(options) {
        this._renderTarget = undefined;
        this.eventBeforeRender = new events_1.Event();
        this.eventAfterRender = new events_1.Event();
        this.eventMouseMove = new events_1.Event();
        this.eventMouseUp = new events_1.Event();
        this.eventMouseDown = new events_1.Event();
        this.eventMouseWheel = new events_1.Event();
        let opt = {};
        Object.assign(opt, defaultOptions, options);
        this.name = "Viewer" + Viewer.gViewerCount++;
        this.width = opt.width;
        this.height = opt.height;
        this.viewportRect = new vector4_1.Vector4(opt.xoffset, opt.yoffset, opt.width, opt.height);
        this.renderer = opt.renderer;
        this.bindContainer(opt.container);
        if (opt.camera) {
            this.camera = opt.camera;
            this.scene = opt.camera.scene;
        }
        else {
            this.scene = new scene_1.Scene();
            this.camera = new camera_1.PerspectiveCamera(this.scene, 75, this.width / this.height, 1, 10000);
        }
        this._eventMouseMoveHandleRef = this._mousemoveHandler.bind(this);
        this._eventMouseUpHandleRef = this._mouseupHandler.bind(this);
        this._eventMouseDownHandleRef = this._mousedownHandler.bind(this);
        this._eventMouseWheelHandleRef = this._mousewheelHandler.bind(this);
        this.eventBeforeRender.onFire(() => {
            if (this._renderTarget) {
                this._renderTarget.beginRenderToTarget();
            }
        });
        this.eventAfterRender.onFire(() => {
            if (this._renderTarget) {
                this._renderTarget.endRenderToTarget();
            }
        });
    }
    get renderTarget() {
        return this._renderTarget;
    }
    set renderTarget(value) {
        this._renderTarget = value;
    }
    setRenderTarget(value) {
        this.renderTarget = value;
    }
    bindContainer(container) {
        if (this.container) {
            this._removeAllEvents();
        }
        this.container = container;
        this._registerAllEvents();
    }
    _mouseupHandler(opts) {
        this.eventMouseUp.fire(opts);
    }
    _mousedownHandler(opts) {
        this.eventMouseDown.fire(opts);
    }
    _mousemoveHandler(opts) {
        this.eventMouseMove.fire(opts);
    }
    _mousewheelHandler(opts) {
        this.eventMouseWheel.fire(opts);
    }
    _registerAllEvents() {
        this.container.addEventListener('mouseup', this._eventMouseUpHandleRef);
        this.container.addEventListener('mousedown', this._eventMouseDownHandleRef);
        this.container.addEventListener('mousemove', this._eventMouseMoveHandleRef);
        this.container.addEventListener('wheel', this._eventMouseWheelHandleRef);
    }
    _removeAllEvents() {
        this.container.removeEventListener('mouseup', this._eventMouseUpHandleRef);
        this.container.removeEventListener('mousedown', this._eventMouseDownHandleRef);
        this.container.removeEventListener('mousemove', this._eventMouseMoveHandleRef);
        this.container.removeEventListener('wheel', this._eventMouseWheelHandleRef);
    }
}
exports.Viewer = Viewer;
Viewer.gViewerCount = 0;


/***/ }),

/***/ "./src/geometries/boxgeometry.ts":
/*!***************************************!*\
  !*** ./src/geometries/boxgeometry.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.BoxGeometry = void 0;
const geometry_1 = __webpack_require__(/*! ../render/geometry */ "./src/render/geometry.ts");
const vector4_1 = __webpack_require__(/*! ../math/vector4 */ "./src/math/vector4.ts");
class BoxGeometry extends geometry_1.Geometry {
    constructor(options) {
        super();
        let width = options.width || 1;
        let height = options.height || 1;
        let length = options.length || 1;
        let nbFaces = 6;
        let vertices = [1, -1, 1, -1, -1, 1, -1, 1, 1, 1, 1, 1, 1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, 1, -1, 1, -1, -1, 1, -1, 1, 1, 1, 1, -1, 1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, 1, -1, 1, -1, 1, 1, -1, 1, 1, 1, 1, -1, 1, 1, -1, -1, -1, -1, -1, -1, -1, 1];
        let normals = [0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0];
        let indices = [0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23];
        let uvs = [];
        let colors = [];
        let faceUV = options.faceUV || new Array(6);
        let faceColors = options.faceColors;
        for (let f = 0; f < 6; f++) {
            if (faceUV[f] === undefined) {
                faceUV[f] = new vector4_1.Vector4(0, 0, 1, 1);
            }
            if (faceColors && faceColors[f] === undefined) {
                faceColors[f] = new vector4_1.Vector4(1, 1, 1, 1);
            }
        }
        for (let index = 0; index < nbFaces; index++) {
            uvs.push(faceUV[index].z, faceUV[index].w);
            uvs.push(faceUV[index].x, faceUV[index].w);
            uvs.push(faceUV[index].x, faceUV[index].y);
            uvs.push(faceUV[index].z, faceUV[index].y);
            if (faceColors) {
                for (let c = 0; c < 4; c++) {
                    colors.push(faceColors[index].x, faceColors[index].y, faceColors[index].z, faceColors[index].w);
                }
            }
        }
        this.setVertexAttribute("position", {
            buffer: new Float32Array(vertices),
            numComponents: 3,
            sizeofStride: 12,
            offsetofStride: 0,
        });
        this.setVertexAttribute("normal", {
            buffer: new Float32Array(normals),
            numComponents: 3,
            sizeofStride: 12,
            offsetofStride: 0,
        });
        this.setVertexAttribute("texcoord", {
            buffer: new Float32Array(uvs),
            numComponents: 2,
            sizeofStride: 8,
            offsetofStride: 0,
        });
        this.setVertexAttribute("indices", {
            buffer: new Uint16Array(indices),
            numComponents: 1,
            sizeofStride: 2,
            offsetofStride: 0,
        });
        let scaleArray = [width / 2, height / 2, length / 2];
        for (let index = 0; index < vertices.length; index++) {
            vertices[index] = vertices[index] * scaleArray[index % 3];
        }
        if (faceColors) {
            this.setVertexAttribute("color", {
                buffer: new Float32Array(colors),
                numComponents: 4,
                sizeofStride: 16,
                offsetofStride: 0,
            });
        }
    }
}
exports.BoxGeometry = BoxGeometry;


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
var drawobject_1 = __webpack_require__(/*! ./render/drawobject */ "./src/render/drawobject.ts");
Object.defineProperty(exports, "DrawableObj", { enumerable: true, get: function () { return drawobject_1.DrawableObj; } });
Object.defineProperty(exports, "EDrawType", { enumerable: true, get: function () { return drawobject_1.EDrawType; } });
Object.defineProperty(exports, "RenderList", { enumerable: true, get: function () { return drawobject_1.RenderList; } });
Object.defineProperty(exports, "RenderGroup", { enumerable: true, get: function () { return drawobject_1.RenderGroup; } });
var geometry_1 = __webpack_require__(/*! ./render/geometry */ "./src/render/geometry.ts");
Object.defineProperty(exports, "Geometry", { enumerable: true, get: function () { return geometry_1.Geometry; } });
var glcontext_1 = __webpack_require__(/*! ./render/glcontext */ "./src/render/glcontext.ts");
Object.defineProperty(exports, "GLContext", { enumerable: true, get: function () { return glcontext_1.GLContext; } });
var program_1 = __webpack_require__(/*! ./render/program */ "./src/render/program.ts");
Object.defineProperty(exports, "ProgramInfo", { enumerable: true, get: function () { return program_1.ProgramInfo; } });
var renderer_1 = __webpack_require__(/*! ./render/renderer */ "./src/render/renderer.ts");
Object.defineProperty(exports, "Renderer", { enumerable: true, get: function () { return renderer_1.Renderer; } });
Object.defineProperty(exports, "ERenderGroupType", { enumerable: true, get: function () { return renderer_1.ERenderGroupType; } });
var resourcemgr_1 = __webpack_require__(/*! ./core/resourcemgr */ "./src/core/resourcemgr.ts");
Object.defineProperty(exports, "ResourceManager", { enumerable: true, get: function () { return resourcemgr_1.ResourceManager; } });
var matrix4_1 = __webpack_require__(/*! ./math/matrix4 */ "./src/math/matrix4.ts");
Object.defineProperty(exports, "Matrix4", { enumerable: true, get: function () { return matrix4_1.Matrix4; } });
var vector3_1 = __webpack_require__(/*! ./math/vector3 */ "./src/math/vector3.ts");
Object.defineProperty(exports, "Vector3", { enumerable: true, get: function () { return vector3_1.Vector3; } });
var vector4_1 = __webpack_require__(/*! ./math/vector4 */ "./src/math/vector4.ts");
Object.defineProperty(exports, "Vector4", { enumerable: true, get: function () { return vector4_1.Vector4; } });
var scene_1 = __webpack_require__(/*! ./core/scene */ "./src/core/scene.ts");
Object.defineProperty(exports, "Scene", { enumerable: true, get: function () { return scene_1.Scene; } });
var scenenode_1 = __webpack_require__(/*! ./core/scenenode */ "./src/core/scenenode.ts");
Object.defineProperty(exports, "SceneNode", { enumerable: true, get: function () { return scenenode_1.SceneNode; } });
var entity_1 = __webpack_require__(/*! ./core/entity */ "./src/core/entity.ts");
Object.defineProperty(exports, "Entity", { enumerable: true, get: function () { return entity_1.Entity; } });
var engine_1 = __webpack_require__(/*! ./core/engine */ "./src/core/engine.ts");
Object.defineProperty(exports, "Engine", { enumerable: true, get: function () { return engine_1.Engine; } });
var viewer_1 = __webpack_require__(/*! ./core/viewer */ "./src/core/viewer.ts");
Object.defineProperty(exports, "Viewer", { enumerable: true, get: function () { return viewer_1.Viewer; } });
var camera_1 = __webpack_require__(/*! ./core/camera */ "./src/core/camera.ts");
Object.defineProperty(exports, "Camera", { enumerable: true, get: function () { return camera_1.Camera; } });
var camera_2 = __webpack_require__(/*! ./core/camera */ "./src/core/camera.ts");
Object.defineProperty(exports, "PerspectiveCamera", { enumerable: true, get: function () { return camera_2.PerspectiveCamera; } });
var rendertarget_1 = __webpack_require__(/*! ./core/rendertarget */ "./src/core/rendertarget.ts");
Object.defineProperty(exports, "RenderTarget", { enumerable: true, get: function () { return rendertarget_1.RenderTarget; } });
var boxgeometry_1 = __webpack_require__(/*! ./geometries/boxgeometry */ "./src/geometries/boxgeometry.ts");
Object.defineProperty(exports, "BoxGeometry", { enumerable: true, get: function () { return boxgeometry_1.BoxGeometry; } });
var texture_1 = __webpack_require__(/*! ./core/texture */ "./src/core/texture.ts");
Object.defineProperty(exports, "Texture", { enumerable: true, get: function () { return texture_1.Texture; } });
var gltfloader_1 = __webpack_require__(/*! ./thirdpart/loaders/gltfloader */ "./src/thirdpart/loaders/gltfloader.ts");
Object.defineProperty(exports, "GLTFLoader", { enumerable: true, get: function () { return gltfloader_1.GLTFLoader; } });
__exportStar(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module '@/thirdpart/hdr/index'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())), exports);


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mirror = __importStar(__webpack_require__(/*! ./index */ "./src/index.ts"));
window.Mi = mirror;
console.log("set window property:", window.Mi);


/***/ }),

/***/ "./src/math/matrix4.ts":
/*!*****************************!*\
  !*** ./src/math/matrix4.ts ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Matrix4 = void 0;
const defaultvalue_1 = __webpack_require__(/*! ../core/defaultvalue */ "./src/core/defaultvalue.ts");
const vector3_1 = __webpack_require__(/*! ./vector3 */ "./src/math/vector3.ts");
class Matrix4 {
    constructor(column0Row0, column0Row1, column0Row2, column0Row3, column1Row0, column1Row1, column1Row2, column1Row3, column2Row0, column2Row1, column2Row2, column2Row3, column3Row0, column3Row1, column3Row2, column3Row3) {
        this._dataArray = [];
        let datas = this._dataArray;
        datas[0] = defaultvalue_1.defaultValue(column0Row0, 1.0);
        datas[1] = defaultvalue_1.defaultValue(column0Row1, 0.0);
        datas[2] = defaultvalue_1.defaultValue(column0Row2, 0.0);
        datas[3] = defaultvalue_1.defaultValue(column0Row3, 0.0);
        datas[4] = defaultvalue_1.defaultValue(column1Row0, 0.0);
        datas[5] = defaultvalue_1.defaultValue(column1Row1, 1.0);
        datas[6] = defaultvalue_1.defaultValue(column1Row2, 0.0);
        datas[7] = defaultvalue_1.defaultValue(column1Row3, 0.0);
        datas[8] = defaultvalue_1.defaultValue(column2Row0, 0.0);
        datas[9] = defaultvalue_1.defaultValue(column2Row1, 0.0);
        datas[10] = defaultvalue_1.defaultValue(column2Row2, 1.0);
        datas[11] = defaultvalue_1.defaultValue(column2Row3, 0.0);
        datas[12] = defaultvalue_1.defaultValue(column3Row0, 0.0);
        datas[13] = defaultvalue_1.defaultValue(column3Row1, 0.0);
        datas[14] = defaultvalue_1.defaultValue(column3Row2, 0.0);
        datas[15] = defaultvalue_1.defaultValue(column3Row3, 1.0);
    }
    static get IDENTITY() {
        return new Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    }
    ;
    get data() {
        return this._dataArray;
    }
    copy(matrix4) {
        let datas = this._dataArray;
        let matrixDatas = matrix4._dataArray;
        datas[0] = matrixDatas[0];
        datas[1] = matrixDatas[1];
        datas[2] = matrixDatas[2];
        datas[3] = matrixDatas[3];
        datas[4] = matrixDatas[4];
        datas[5] = matrixDatas[5];
        datas[6] = matrixDatas[6];
        datas[7] = matrixDatas[7];
        datas[8] = matrixDatas[8];
        datas[9] = matrixDatas[9];
        datas[10] = matrixDatas[10];
        datas[11] = matrixDatas[11];
        datas[12] = matrixDatas[12];
        datas[13] = matrixDatas[13];
        datas[14] = matrixDatas[14];
        datas[15] = matrixDatas[15];
        return this;
    }
    clone() {
        let ret = new Matrix4();
        ret.copy(this);
        return ret;
    }
    equals(matrix) {
        if (!matrix) {
            return false;
        }
        var te = this._dataArray;
        var me = matrix._dataArray;
        for (var i = 0; i < 16; i++) {
            if (te[i] !== me[i])
                return false;
        }
        return true;
    }
    determinant() {
        let te = this._dataArray;
        let n11 = te[0], n12 = te[4], n13 = te[8], n14 = te[12];
        let n21 = te[1], n22 = te[5], n23 = te[9], n24 = te[13];
        let n31 = te[2], n32 = te[6], n33 = te[10], n34 = te[14];
        let n41 = te[3], n42 = te[7], n43 = te[11], n44 = te[15];
        return (n41 * (+n14 * n23 * n32
            - n13 * n24 * n32
            - n14 * n22 * n33
            + n12 * n24 * n33
            + n13 * n22 * n34
            - n12 * n23 * n34) +
            n42 * (+n11 * n23 * n34
                - n11 * n24 * n33
                + n14 * n21 * n33
                - n13 * n21 * n34
                + n13 * n24 * n31
                - n14 * n23 * n31) +
            n43 * (+n11 * n24 * n32
                - n11 * n22 * n34
                - n14 * n21 * n32
                + n12 * n21 * n34
                + n14 * n22 * n31
                - n12 * n24 * n31) +
            n44 * (-n13 * n22 * n31
                - n11 * n23 * n32
                + n11 * n22 * n33
                + n13 * n21 * n32
                - n12 * n21 * n33
                + n12 * n23 * n31));
    }
    compose(position, quaternion, scale) {
        let te = this._dataArray;
        let x = quaternion.x, y = quaternion.y, z = quaternion.z, w = quaternion.w;
        let x2 = x + x, y2 = y + y, z2 = z + z;
        let xx = x * x2, xy = x * y2, xz = x * z2;
        let yy = y * y2, yz = y * z2, zz = z * z2;
        let wx = w * x2, wy = w * y2, wz = w * z2;
        let sx = scale.x, sy = scale.y, sz = scale.z;
        te[0] = (1 - (yy + zz)) * sx;
        te[1] = (xy + wz) * sx;
        te[2] = (xz - wy) * sx;
        te[3] = 0;
        te[4] = (xy - wz) * sy;
        te[5] = (1 - (xx + zz)) * sy;
        te[6] = (yz + wx) * sy;
        te[7] = 0;
        te[8] = (xz + wy) * sz;
        te[9] = (yz - wx) * sz;
        te[10] = (1 - (xx + yy)) * sz;
        te[11] = 0;
        te[12] = position.x;
        te[13] = position.y;
        te[14] = position.z;
        te[15] = 1;
        return this;
    }
    decompose(position, quaternion, scale) {
        let matrix = new Matrix4();
        let vector = vector3_1.Vector3.ZERO;
        let te = this._dataArray;
        let sx = vector.set(te[0], te[1], te[2]).size();
        let sy = vector.set(te[4], te[5], te[6]).size();
        let sz = vector.set(te[8], te[9], te[10]).size();
        let det = this.determinant();
        if (det < 0)
            sx = -sx;
        position.x = te[12];
        position.y = te[13];
        position.z = te[14];
        matrix.copy(this);
        var invSX = 1 / sx;
        var invSY = 1 / sy;
        var invSZ = 1 / sz;
        matrix.data[0] *= invSX;
        matrix.data[1] *= invSX;
        matrix.data[2] *= invSX;
        matrix.data[4] *= invSY;
        matrix.data[5] *= invSY;
        matrix.data[6] *= invSY;
        matrix.data[8] *= invSZ;
        matrix.data[9] *= invSZ;
        matrix.data[10] *= invSZ;
        scale.x = sx;
        scale.y = sy;
        scale.z = sz;
        return this;
    }
    static fromArray(array, result) {
        let ret = defaultvalue_1.defaultValue(result, new Matrix4);
        let retDatas = ret._dataArray;
        retDatas[0] = array[0];
        retDatas[1] = array[1];
        retDatas[2] = array[2];
        retDatas[3] = array[3];
        retDatas[4] = array[4];
        retDatas[5] = array[5];
        retDatas[6] = array[6];
        retDatas[7] = array[7];
        retDatas[8] = array[8];
        retDatas[9] = array[9];
        retDatas[10] = array[10];
        retDatas[11] = array[11];
        retDatas[12] = array[12];
        retDatas[13] = array[13];
        retDatas[14] = array[14];
        retDatas[15] = array[15];
        return result;
    }
    static multiply(a, b) {
        let aDatas = a._dataArray;
        let left0 = aDatas[0];
        let left1 = aDatas[1];
        let left2 = aDatas[2];
        let left3 = aDatas[3];
        let left4 = aDatas[4];
        let left5 = aDatas[5];
        let left6 = aDatas[6];
        let left7 = aDatas[7];
        let left8 = aDatas[8];
        let left9 = aDatas[9];
        let left10 = aDatas[10];
        let left11 = aDatas[11];
        let left12 = aDatas[12];
        let left13 = aDatas[13];
        let left14 = aDatas[14];
        let left15 = aDatas[15];
        let bDatas = b._dataArray;
        let right0 = bDatas[0];
        let right1 = bDatas[1];
        let right2 = bDatas[2];
        let right3 = bDatas[3];
        let right4 = bDatas[4];
        let right5 = bDatas[5];
        let right6 = bDatas[6];
        let right7 = bDatas[7];
        let right8 = bDatas[8];
        let right9 = bDatas[9];
        let right10 = bDatas[10];
        let right11 = bDatas[11];
        let right12 = bDatas[12];
        let right13 = bDatas[13];
        let right14 = bDatas[14];
        let right15 = bDatas[15];
        let col0Row0 = left0 * right0 + left4 * right1 + left8 * right2 + left12 * right3;
        let col0Row1 = left1 * right0 + left5 * right1 + left9 * right2 + left13 * right3;
        let col0Row2 = left2 * right0 + left6 * right1 + left10 * right2 + left14 * right3;
        let col0Row3 = left3 * right0 + left7 * right1 + left11 * right2 + left15 * right3;
        let col1Row0 = left0 * right4 + left4 * right5 + left8 * right6 + left12 * right7;
        let col1Row1 = left1 * right4 + left5 * right5 + left9 * right6 + left13 * right7;
        let col1Row2 = left2 * right4 + left6 * right5 + left10 * right6 + left14 * right7;
        let col1Row3 = left3 * right4 + left7 * right5 + left11 * right6 + left15 * right7;
        let col2Row0 = left0 * right8 + left4 * right9 + left8 * right10 + left12 * right11;
        let col2Row1 = left1 * right8 + left5 * right9 + left9 * right10 + left13 * right11;
        let col2Row2 = left2 * right8 + left6 * right9 + left10 * right10 + left14 * right11;
        let col2Row3 = left3 * right8 + left7 * right9 + left11 * right10 + left15 * right11;
        let col3Row0 = left0 * right12 + left4 * right13 + left8 * right14 + left12 * right15;
        let col3Row1 = left1 * right12 + left5 * right13 + left9 * right14 + left13 * right15;
        let col3Row2 = left2 * right12 + left6 * right13 + left10 * right14 + left14 * right15;
        let col3Row3 = left3 * right12 + left7 * right13 + left11 * right14 + left15 * right15;
        return new Matrix4(col0Row0, col0Row1, col0Row2, col0Row3, col1Row0, col1Row1, col1Row2, col1Row3, col2Row0, col2Row1, col2Row2, col2Row3, col3Row0, col3Row1, col3Row2, col3Row3);
    }
    static add(left, right) {
        let leftDatas = left._dataArray;
        let rightDatas = right._dataArray;
        let result = new Matrix4;
        let retDatas = result._dataArray;
        retDatas[0] = leftDatas[0] + rightDatas[0];
        retDatas[1] = leftDatas[1] + rightDatas[1];
        retDatas[2] = leftDatas[2] + rightDatas[2];
        retDatas[3] = leftDatas[3] + rightDatas[3];
        retDatas[4] = leftDatas[4] + rightDatas[4];
        retDatas[5] = leftDatas[5] + rightDatas[5];
        retDatas[6] = leftDatas[6] + rightDatas[6];
        retDatas[7] = leftDatas[7] + rightDatas[7];
        retDatas[8] = leftDatas[8] + rightDatas[8];
        retDatas[9] = leftDatas[9] + rightDatas[9];
        retDatas[10] = leftDatas[10] + rightDatas[10];
        retDatas[11] = leftDatas[11] + rightDatas[11];
        retDatas[12] = leftDatas[12] + rightDatas[12];
        retDatas[13] = leftDatas[13] + rightDatas[13];
        retDatas[14] = leftDatas[14] + rightDatas[14];
        retDatas[15] = leftDatas[15] + rightDatas[15];
        return result;
    }
    static subtract(left, right) {
        let leftDatas = left._dataArray;
        let rightDatas = right._dataArray;
        let result = new Matrix4;
        let retDatas = result._dataArray;
        retDatas[0] = leftDatas[0] - rightDatas[0];
        retDatas[1] = leftDatas[1] - rightDatas[1];
        retDatas[2] = leftDatas[2] - rightDatas[2];
        retDatas[3] = leftDatas[3] - rightDatas[3];
        retDatas[4] = leftDatas[4] - rightDatas[4];
        retDatas[5] = leftDatas[5] - rightDatas[5];
        retDatas[6] = leftDatas[6] - rightDatas[6];
        retDatas[7] = leftDatas[7] - rightDatas[7];
        retDatas[8] = leftDatas[8] - rightDatas[8];
        retDatas[9] = leftDatas[9] - rightDatas[9];
        retDatas[10] = leftDatas[10] - rightDatas[10];
        retDatas[11] = leftDatas[11] - rightDatas[11];
        retDatas[12] = leftDatas[12] - rightDatas[12];
        retDatas[13] = leftDatas[13] - rightDatas[13];
        retDatas[14] = leftDatas[14] - rightDatas[14];
        retDatas[15] = leftDatas[15] - rightDatas[15];
        return result;
    }
    static multiplyByScale(left, scale) {
        let scaleX = scale.x;
        let scaleY = scale.y;
        let scaleZ = scale.z;
        let result = new Matrix4;
        let retDatas = result._dataArray;
        let leftDatas = left._dataArray;
        retDatas[0] = scaleX * leftDatas[0];
        retDatas[1] = scaleX * leftDatas[1];
        retDatas[2] = scaleX * leftDatas[2];
        retDatas[3] = 0.0;
        retDatas[4] = scaleY * leftDatas[4];
        retDatas[5] = scaleY * leftDatas[5];
        retDatas[6] = scaleY * leftDatas[6];
        retDatas[7] = 0.0;
        retDatas[8] = scaleZ * leftDatas[8];
        retDatas[9] = scaleZ * leftDatas[9];
        retDatas[10] = scaleZ * leftDatas[10];
        retDatas[11] = 0.0;
        retDatas[12] = leftDatas[12];
        retDatas[13] = leftDatas[13];
        retDatas[14] = leftDatas[14];
        retDatas[15] = 1.0;
        return result;
    }
    static negate(matrix) {
        let result = new Matrix4;
        let retDatas = result._dataArray;
        let matrixDatas = matrix._dataArray;
        retDatas[0] = -matrixDatas[0];
        retDatas[1] = -matrixDatas[1];
        retDatas[2] = -matrixDatas[2];
        retDatas[3] = -matrixDatas[3];
        retDatas[4] = -matrixDatas[4];
        retDatas[5] = -matrixDatas[5];
        retDatas[6] = -matrixDatas[6];
        retDatas[7] = -matrixDatas[7];
        retDatas[8] = -matrixDatas[8];
        retDatas[9] = -matrixDatas[9];
        retDatas[10] = -matrixDatas[10];
        retDatas[11] = -matrixDatas[11];
        retDatas[12] = -matrixDatas[12];
        retDatas[13] = -matrixDatas[13];
        retDatas[14] = -matrixDatas[14];
        retDatas[15] = -matrixDatas[15];
        return result;
    }
    static lookat(eye, target, up) {
        let vecZ = new vector3_1.Vector3;
        let vecX = new vector3_1.Vector3;
        let vecY = new vector3_1.Vector3;
        let result = new Matrix4;
        let retDatas = result._dataArray;
        vector3_1.Vector3.sub(eye, target, vecZ);
        if (vector3_1.Vector3.sizeSquare(vecZ) === 0) {
            vecZ.z = 1;
        }
        vecZ.normalize();
        vector3_1.Vector3.cross(up, vecZ, vecX);
        if (vector3_1.Vector3.sizeSquare(vecX) === 0) {
            if (Math.abs(up.z) === 1) {
                vecZ.x += 0.0001;
            }
            else {
                vecZ.z += 0.0001;
            }
            vecZ.normalize();
            vector3_1.Vector3.cross(up, vecZ, vecX);
        }
        vecX.normalize();
        vector3_1.Vector3.cross(vecZ, vecX, vecY);
        retDatas[0] = vecX.x;
        retDatas[4] = vecY.x;
        retDatas[8] = vecZ.x;
        retDatas[1] = vecX.y;
        retDatas[5] = vecY.y;
        retDatas[9] = vecZ.y;
        retDatas[2] = vecX.z;
        retDatas[6] = vecY.z;
        retDatas[10] = vecZ.z;
        return result;
    }
    static lookAt2(eye, target, up, result) {
        let ret = defaultvalue_1.defaultValue(result, new Matrix4);
        var x0, x1, x2, y0, y1, y2, z0, z1, z2, len;
        var eyex = eye.x;
        var eyey = eye.y;
        var eyez = eye.z;
        var upx = up.x;
        var upy = up.y;
        var upz = up.z;
        var centerx = target.x;
        var centery = target.y;
        var centerz = target.z;
        var EPSILON = 0.000001;
        if (Math.abs(eyex - centerx) < EPSILON && Math.abs(eyey - centery) < EPSILON && Math.abs(eyez - centerz) < EPSILON) {
            return Matrix4.IDENTITY;
        }
        z0 = eyex - centerx;
        z1 = eyey - centery;
        z2 = eyez - centerz;
        len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
        z0 *= len;
        z1 *= len;
        z2 *= len;
        x0 = upy * z2 - upz * z1;
        x1 = upz * z0 - upx * z2;
        x2 = upx * z1 - upy * z0;
        len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
        if (!len) {
            x0 = 0;
            x1 = 0;
            x2 = 0;
        }
        else {
            len = 1 / len;
            x0 *= len;
            x1 *= len;
            x2 *= len;
        }
        y0 = z1 * x2 - z2 * x1;
        y1 = z2 * x0 - z0 * x2;
        y2 = z0 * x1 - z1 * x0;
        len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
        if (!len) {
            y0 = 0;
            y1 = 0;
            y2 = 0;
        }
        else {
            len = 1 / len;
            y0 *= len;
            y1 *= len;
            y2 *= len;
        }
        let retDatas = ret.data;
        retDatas[0] = x0;
        retDatas[1] = y0;
        retDatas[2] = z0;
        retDatas[3] = 0;
        retDatas[4] = x1;
        retDatas[5] = y1;
        retDatas[6] = z1;
        retDatas[7] = 0;
        retDatas[8] = x2;
        retDatas[9] = y2;
        retDatas[10] = z2;
        retDatas[11] = 0;
        retDatas[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
        retDatas[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
        retDatas[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
        retDatas[15] = 1;
        return ret;
    }
    static perspective(fovy, aspect, near, far) {
        let result = new Matrix4;
        let retDatas = result._dataArray;
        var f = 1.0 / Math.tan(fovy / 2), nf;
        retDatas[0] = f / aspect;
        retDatas[1] = 0;
        retDatas[2] = 0;
        retDatas[3] = 0;
        retDatas[4] = 0;
        retDatas[5] = f;
        retDatas[6] = 0;
        retDatas[7] = 0;
        retDatas[8] = 0;
        retDatas[9] = 0;
        retDatas[11] = -1;
        retDatas[12] = 0;
        retDatas[13] = 0;
        retDatas[15] = 0;
        if (far != null && far !== Infinity) {
            nf = 1 / (near - far);
            retDatas[10] = (far + near) * nf;
            retDatas[14] = 2 * far * near * nf;
        }
        else {
            retDatas[10] = -1;
            retDatas[14] = -2 * near;
        }
        return result;
    }
}
exports.Matrix4 = Matrix4;


/***/ }),

/***/ "./src/math/vector3.ts":
/*!*****************************!*\
  !*** ./src/math/vector3.ts ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Vector3 = void 0;
const defaultvalue_1 = __webpack_require__(/*! ../core/defaultvalue */ "./src/core/defaultvalue.ts");
class Vector3 {
    constructor(x = 0, y = 0, z = 0) {
        this._x = x;
        this._y = y;
        this._z = z;
    }
    static get ZERO() {
        return new Vector3(0, 0, 0);
    }
    get x() { return this._x; }
    get y() { return this._y; }
    get z() { return this._z; }
    set x(val) { this._x = val; }
    ;
    set y(val) { this._y = val; }
    ;
    set z(val) { this._z = val; }
    ;
    tostring() {
        return `[${this._x},${this._y},${this._z}]`;
    }
    set(x, y, z) {
        this._x = x || 0;
        this._y = y || 0;
        this._z = z || 0;
        return this;
    }
    clone() {
        return new Vector3(this._x, this._y, this._z);
    }
    static add(left, right, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector3(0, 0, 0));
        ret._x = left._x + right._x;
        ret._y = left._y + right._y;
        ret._z = left._z + right._z;
        return ret;
    }
    toArray() {
        let ret = [];
        ret[0] = this._x;
        ret[1] = this._y;
        ret[2] = this._z;
        return ret;
    }
    static fromArray(array, offset, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector3(0, 0, 0));
        if (offset === undefined)
            offset = 0;
        ret._x = array[offset];
        ret._y = array[offset + 1];
        ret._z = array[offset + 2];
        return ret;
    }
    static cross(left, right, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector3(0, 0, 0));
        var leftX = left.x;
        var leftY = left.y;
        var leftZ = left.z;
        var rightX = right.x;
        var rightY = right.y;
        var rightZ = right.z;
        ret._x = leftY * rightZ - leftZ * rightY;
        ret._y = leftZ * rightX - leftX * rightZ;
        ret._z = leftX * rightY - leftY * rightX;
        return ret;
    }
    static addScalar(left, s, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector3(0, 0, 0));
        ret._x = left._x + s;
        ret._y = left._y + s;
        ret._z = left._z + s;
        return ret;
    }
    static sub(left, right, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector3(0, 0, 0));
        ret._x = left._x - right._x;
        ret._y = left._y - right._y;
        ret._z = left._z - right._z;
        return ret;
    }
    static subScalar(left, s, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector3(0, 0, 0));
        ret._x = left._x - s;
        ret._y = left._y - s;
        ret._z = left._z - s;
        return ret;
    }
    static multiply(left, right, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector3(0, 0, 0));
        ret._x = left._x * right._x;
        ret._y = left._y * right._y;
        ret._z = left._z * right._z;
        return ret;
    }
    static multiplyScalar(left, s, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector3(0, 0, 0));
        ret._x = left._x * s;
        ret._y = left._y * s;
        ret._z = left._z * s;
        return ret;
    }
    static divide(left, t, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector3(0, 0, 0));
        ret._x = left._x / t._x;
        ret._y = left._y / t._y;
        ret._z = left._z / t._z;
        return ret;
    }
    divideScalar(s) {
        this._x = this._x / s;
        this._y = this._y / s;
        this._z = this._z / s;
        return this;
    }
    static negate(vec3, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector3(0, 0, 0));
        ret._x = -vec3._x;
        ret._y = -vec3._y;
        ret._z = -vec3._z;
        return ret;
    }
    static dot(left, right) {
        return left._x * right._x + left._y * right._y + left._z * right._z;
    }
    static sizeSquare(vec3) {
        return vec3._x * vec3._x + vec3._y * vec3._y + vec3._z * vec3._z;
    }
    size() {
        return Math.sqrt(this._x * this._x + this._y * this._y + this._z * this._z);
    }
    normalize() {
        return this.divideScalar(this.size() || 1);
    }
    static distanceToSquare(left, right) {
        var dx = left._x - right._x, dy = left._y - right._y, dz = left._z - right._z;
        return dx * dx + dy * dy + dz * dz;
    }
    static distanceTo(left, right) {
        return Math.sqrt(Vector3.distanceToSquare(left, right));
    }
}
exports.Vector3 = Vector3;


/***/ }),

/***/ "./src/math/vector4.ts":
/*!*****************************!*\
  !*** ./src/math/vector4.ts ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Vector4 = void 0;
const defaultvalue_1 = __webpack_require__(/*! ../core/defaultvalue */ "./src/core/defaultvalue.ts");
class Vector4 {
    constructor(x = 0, y = 0, z = 0, w = 0) {
        this._x = x;
        this._y = y;
        this._z = z;
        this._w = w;
    }
    static get ZERO() {
        return new Vector4(0, 0, 0, 0);
    }
    get x() { return this._x; }
    get y() { return this._y; }
    get z() { return this._z; }
    get w() { return this._w; }
    set x(val) { this._x = val; }
    ;
    set y(val) { this._y = val; }
    ;
    set z(val) { this._z = val; }
    ;
    set w(val) { this._w = val; }
    ;
    set(x, y, z, w) {
        this._x = x || 0;
        this._y = y || 0;
        this._z = z || 0;
        this._w = w || 0;
    }
    clone() {
        return new Vector4(this._x, this._y, this._z, this._w);
    }
    static add(left, right, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector4(0, 0, 0, 0));
        ret._x = left._x + right._x;
        ret._y = left._y + right._y;
        ret._z = left._z + right._z;
        ret._w = left._w + right._w;
        return ret;
    }
    static addScalar(left, s, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector4(0, 0, 0, 0));
        ret._x = left._x + s;
        ret._y = left._y + s;
        ret._z = left._z + s;
        ret._w = left._w + s;
        return ret;
    }
    static sub(left, right, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector4(0, 0, 0, 0));
        ret._x = left._x - right._x;
        ret._y = left._y - right._y;
        ret._z = left._z - right._z;
        ret._w = left._w - right._w;
        return ret;
    }
    static subScalar(left, s, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector4(0, 0, 0, 0));
        ret._x = left._x - s;
        ret._y = left._y - s;
        ret._z = left._z - s;
        ret._w = left._w - s;
        return ret;
    }
    static multiplyScalar(left, s, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector4(0, 0, 0, 0));
        ret._x = left._x * s;
        ret._y = left._y * s;
        ret._z = left._z * s;
        ret._w = left._w * s;
        return ret;
    }
    static divideScalar(left, s, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector4(0, 0, 0, 0));
        ret._x = left._x / s;
        ret._y = left._y / s;
        ret._z = left._z / s;
        ret._w = left._w / s;
        return ret;
    }
    static negate(vec4, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector4(0, 0, 0, 0));
        ret._x = -vec4._x;
        ret._y = -vec4._y;
        ret._z = -vec4._z;
        ret._w = -vec4._w;
        return ret;
    }
    static dot(left, right) {
        return left._x * right._x + left._y * right._y + left._z * right._z + left._w * right._w;
    }
    static sizeSquare(vec4) {
        return vec4._x * vec4._x + vec4._y * vec4._y + vec4._z * vec4._z + vec4._w * vec4._w;
    }
    static size(vec4) {
        return Math.sqrt(vec4._x * vec4._x + vec4._y * vec4._y + vec4._z * vec4._z + vec4._w * vec4._w);
    }
    static normalize(vec4, result) {
        return Vector4.divideScalar(vec4, Vector4.size(vec4) || 1, result);
    }
    static distanceToSquare(left, right) {
        var dx = left._x - right._x, dy = left._y - right._y, dz = left._z - right._z, dw = left._w - right._w;
        return dx * dx + dy * dy + dz * dz + dw * dw;
    }
    static distanceTo(left, right) {
        return Math.sqrt(Vector4.distanceToSquare(left, right));
    }
    static fromArray(array, offset, result) {
        let ret = defaultvalue_1.defaultValue(result, new Vector4(0, 0, 0, 0));
        if (offset === undefined)
            offset = 0;
        ret._x = array[offset];
        ret._y = array[offset + 1];
        ret._z = array[offset + 2];
        ret._z = array[offset + 3];
        return ret;
    }
}
exports.Vector4 = Vector4;


/***/ }),

/***/ "./src/render/builtinprogram.ts":
/*!**************************************!*\
  !*** ./src/render/builtinprogram.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuiltInProgram = void 0;
const program_1 = __webpack_require__(/*! ./program */ "./src/render/program.ts");
const normal_vertex_1 = __importDefault(__webpack_require__(/*! ./shaders/normal_vertex */ "./src/render/shaders/normal_vertex.ts"));
const normal_fragment_1 = __importDefault(__webpack_require__(/*! ./shaders/normal_fragment */ "./src/render/shaders/normal_fragment.ts"));
const lambert_vs_1 = __importDefault(__webpack_require__(/*! ./shaders/lambert_vs */ "./src/render/shaders/lambert_vs.ts"));
const lambert_fs_1 = __importDefault(__webpack_require__(/*! ./shaders/lambert_fs */ "./src/render/shaders/lambert_fs.ts"));
class BuiltInProgram {
    static createNormalProgram() {
        return new program_1.ProgramInfo(normal_vertex_1.default, normal_fragment_1.default);
    }
    static createLambertProgram() {
        return new program_1.ProgramInfo(lambert_vs_1.default, lambert_fs_1.default);
    }
}
exports.BuiltInProgram = BuiltInProgram;


/***/ }),

/***/ "./src/render/drawobject.ts":
/*!**********************************!*\
  !*** ./src/render/drawobject.ts ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.RenderGroup = exports.RenderList = exports.DrawableObj = exports.EDrawType = void 0;
const glcontext_1 = __webpack_require__(/*! ./glcontext */ "./src/render/glcontext.ts");
var EDrawType;
(function (EDrawType) {
    EDrawType[EDrawType["Points"] = 0] = "Points";
    EDrawType[EDrawType["Lines"] = 1] = "Lines";
    EDrawType[EDrawType["Lines_Loop"] = 2] = "Lines_Loop";
    EDrawType[EDrawType["Lines_Strip"] = 3] = "Lines_Strip";
    EDrawType[EDrawType["Triangles"] = 4] = "Triangles";
    EDrawType[EDrawType["Triangles_Strip"] = 5] = "Triangles_Strip";
    EDrawType[EDrawType["Triangle_Fan"] = 6] = "Triangle_Fan";
})(EDrawType = exports.EDrawType || (exports.EDrawType = {}));
class DrawableObj {
    constructor() {
        this.uniforms = {};
    }
    bindIBO() {
        glcontext_1.GLContext.bindIBOFromVertexInfo(this.vertexInfo);
    }
}
exports.DrawableObj = DrawableObj;
class RenderList {
    constructor() {
        this.renderObjs = [];
    }
}
exports.RenderList = RenderList;
class RenderGroup {
    constructor(id, priority) {
        this.id = id;
        this.priority = priority;
        this.renderListArray = [];
    }
    clear() {
        this.renderListArray = [];
    }
}
exports.RenderGroup = RenderGroup;


/***/ }),

/***/ "./src/render/geometry.ts":
/*!********************************!*\
  !*** ./src/render/geometry.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Geometry = exports.AttributeInfo = void 0;
const gldatatype_1 = __webpack_require__(/*! ./gldatatype */ "./src/render/gldatatype.ts");
class AttributeInfo {
    constructor(name, options) {
        this.name = name;
        this.buffer = options.buffer;
        this.isConstantToShader = options.isConstantToShader || false;
        this.numComponents = options.numComponents;
        this.sizeofStride = options.sizeofStride;
        this.offsetofStride = options.offsetofStride;
        this.normalized = options.normalized || false;
        this._checkBufferType();
    }
    copy(source) {
        this.name = source.name;
        if (source.buffertype) {
            this.buffertype = source.buffertype;
        }
        else {
            this.buffertype = Float32Array;
        }
        this.buffer = new this.buffertype(source.buffer.buffer.slice(0));
        this.isConstantToShader = source.isConstantToShader;
        this.numComponents = source.numComponents;
        this.sizeofStride = source.sizeofStride;
        this.offsetofStride = source.offsetofStride;
        this.normalized = source.normalized;
    }
    clone() {
        let ret = {};
        ret.copy(this);
        return ret;
    }
    _checkBufferType() {
        if (!this.buffer) {
            this.buffertype = undefined;
        }
        if (this.buffer instanceof Float32Array) {
            this.buffertype = Float32Array;
            this.glType = gldatatype_1.EGLDataType.FLOAT;
        }
        else if (this.buffer instanceof Int8Array) {
            this.buffertype = Int8Array;
            this.glType = gldatatype_1.EGLDataType.BYTE;
        }
        else if (this.buffer instanceof Uint8Array) {
            this.buffertype = Uint8Array;
            this.glType = gldatatype_1.EGLDataType.UNSIGNED_BYTE;
        }
        else if (this.buffer instanceof Int16Array) {
            this.buffertype = Int16Array;
            this.glType = gldatatype_1.EGLDataType.SHORT;
        }
        else if (this.buffer instanceof Uint16Array) {
            this.buffertype = Uint16Array;
            this.glType = gldatatype_1.EGLDataType.UNSIGNED_SHORT;
        }
        else if (this.buffer instanceof Int32Array) {
            this.buffertype = Int32Array;
            this.glType = gldatatype_1.EGLDataType.INT;
        }
        else if (this.buffer instanceof Uint32Array) {
            this.buffertype = Uint32Array;
            this.glType = gldatatype_1.EGLDataType.UNSIGNED_INT;
        }
        else if (this.buffer instanceof Float64Array) {
            this.buffertype = Float64Array;
            console.warn('Geometry Attributes: Unsupported data buffer format: Float64Array.');
        }
        else {
            console.warn('Geometry Attributes: Invalid buffer data');
            this.buffertype = undefined;
        }
    }
}
exports.AttributeInfo = AttributeInfo;
class Geometry {
    constructor() {
        this._vertexInfo = {};
    }
    clone() {
        let ret = new Geometry();
        ret.copy(this);
        return ret;
    }
    copy(source) {
        this._vertexInfo.color = source.vertexInfo.color;
        this._vertexInfo.indices = source.vertexInfo.indices;
        this._vertexInfo.normal = source.vertexInfo.normal;
        this._vertexInfo.position = source.vertexInfo.position;
        this._vertexInfo.texcoord = source.vertexInfo.texcoord;
    }
    setVertexAttribute(name, options) {
        let attribute = new AttributeInfo(name, options);
        this._vertexInfo[name] = attribute;
    }
    get vertexInfo() {
        return this._vertexInfo;
    }
}
exports.Geometry = Geometry;


/***/ }),

/***/ "./src/render/glcontext.ts":
/*!*********************************!*\
  !*** ./src/render/glcontext.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.GLContext = void 0;
const gldatatype_1 = __webpack_require__(/*! ./gldatatype */ "./src/render/gldatatype.ts");
class GLContext {
    static init(cavID) {
        const canvas = document.querySelector(`#${cavID}`);
        GLContext.canvas = canvas;
        GLContext.gl = canvas.getContext('webgl');
    }
    static glBindBuffer(gl, glTarget, glBuffer, typedArray, drawtype) {
        let bufferType = (glTarget === gldatatype_1.GLBingBufferTarget.ARRAY_BUFFER) ? gl.ARRAY_BUFFER : gl.ELEMENT_ARRAY_BUFFER;
        gl.bindBuffer(bufferType, glBuffer);
        gl.bufferData(bufferType, typedArray, drawtype);
    }
    static createBufferFromTypedArray(gl, type, typedArray, drawtype) {
        const glBuffer = gl.createBuffer();
        GLContext.bindBuffer(gl, glBuffer, type, typedArray, drawtype);
        return glBuffer;
    }
    static bindBuffer(gl, glBuffer, type, typedArray, drawtype) {
        let glDrawType = gl.STATIC_DRAW;
        if (drawtype === gldatatype_1.GLDrawType.DYNAMIC_DRAW) {
            glDrawType = gl.DYNAMIC_DRAW;
        }
        if (drawtype === gldatatype_1.GLDrawType.STREAM_DRAW) {
            glDrawType = gl.STREAM_DRAW;
        }
        GLContext.glBindBuffer(gl, type, glBuffer, typedArray, glDrawType);
    }
    static bindIBOFromVertexInfo(vertexInfo) {
        let vertexStruct = vertexInfo;
        let gl = GLContext.gl;
        if (vertexInfo.hasOwnProperty("indices")) {
            let indicesProperty = vertexStruct["indices"];
            if (!indicesProperty.glBuffer) {
                indicesProperty.glBuffer =
                    GLContext.createBufferFromTypedArray(gl, gl.ELEMENT_ARRAY_BUFFER, indicesProperty.buffer);
            }
            else {
                GLContext.bindBuffer(gl, indicesProperty.glBuffer, gl.ELEMENT_ARRAY_BUFFER, indicesProperty.buffer, gl.STATIC_DRAW);
            }
        }
    }
}
exports.GLContext = GLContext;


/***/ }),

/***/ "./src/render/gldatatype.ts":
/*!**********************************!*\
  !*** ./src/render/gldatatype.ts ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.GLDataType = exports.EGLDataType = exports.GLBingBufferTarget = exports.GLDrawType = void 0;
let gl;
var GLDrawType;
(function (GLDrawType) {
    GLDrawType[GLDrawType["STATIC_DRAW"] = 0] = "STATIC_DRAW";
    GLDrawType[GLDrawType["DYNAMIC_DRAW"] = 1] = "DYNAMIC_DRAW";
    GLDrawType[GLDrawType["STREAM_DRAW"] = 2] = "STREAM_DRAW";
})(GLDrawType = exports.GLDrawType || (exports.GLDrawType = {}));
var GLBingBufferTarget;
(function (GLBingBufferTarget) {
    GLBingBufferTarget[GLBingBufferTarget["ARRAY_BUFFER"] = 0] = "ARRAY_BUFFER";
    GLBingBufferTarget[GLBingBufferTarget["ELEMENT_ARRAY_BUFFER"] = 1] = "ELEMENT_ARRAY_BUFFER";
})(GLBingBufferTarget = exports.GLBingBufferTarget || (exports.GLBingBufferTarget = {}));
var EGLDataType;
(function (EGLDataType) {
    EGLDataType[EGLDataType["BYTE"] = 5120] = "BYTE";
    EGLDataType[EGLDataType["UNSIGNED_BYTE"] = 5121] = "UNSIGNED_BYTE";
    EGLDataType[EGLDataType["SHORT"] = 5122] = "SHORT";
    EGLDataType[EGLDataType["UNSIGNED_SHORT"] = 5123] = "UNSIGNED_SHORT";
    EGLDataType[EGLDataType["INT"] = 5124] = "INT";
    EGLDataType[EGLDataType["UNSIGNED_INT"] = 5125] = "UNSIGNED_INT";
    EGLDataType[EGLDataType["FLOAT"] = 5126] = "FLOAT";
    EGLDataType[EGLDataType["UNSIGNED_SHORT_4_4_4_4"] = 32819] = "UNSIGNED_SHORT_4_4_4_4";
    EGLDataType[EGLDataType["UNSIGNED_SHORT_5_5_5_1"] = 32820] = "UNSIGNED_SHORT_5_5_5_1";
    EGLDataType[EGLDataType["UNSIGNED_SHORT_5_6_5"] = 33635] = "UNSIGNED_SHORT_5_6_5";
    EGLDataType[EGLDataType["HALF_FLOAT"] = 5131] = "HALF_FLOAT";
    EGLDataType[EGLDataType["UNSIGNED_INT_2_10_10_10_REV"] = 33640] = "UNSIGNED_INT_2_10_10_10_REV";
    EGLDataType[EGLDataType["UNSIGNED_INT_10F_11F_11F_REV"] = 35899] = "UNSIGNED_INT_10F_11F_11F_REV";
    EGLDataType[EGLDataType["UNSIGNED_INT_5_9_9_9_REV"] = 35902] = "UNSIGNED_INT_5_9_9_9_REV";
    EGLDataType[EGLDataType["FLOAT_32_UNSIGNED_INT_24_8_REV"] = 36269] = "FLOAT_32_UNSIGNED_INT_24_8_REV";
    EGLDataType[EGLDataType["UNSIGNED_INT_24_8"] = 34042] = "UNSIGNED_INT_24_8";
    EGLDataType[EGLDataType["FLOAT_VEC2"] = 35664] = "FLOAT_VEC2";
    EGLDataType[EGLDataType["FLOAT_VEC3"] = 35665] = "FLOAT_VEC3";
    EGLDataType[EGLDataType["FLOAT_VEC4"] = 35666] = "FLOAT_VEC4";
    EGLDataType[EGLDataType["INT_VEC2"] = 35667] = "INT_VEC2";
    EGLDataType[EGLDataType["INT_VEC3"] = 35668] = "INT_VEC3";
    EGLDataType[EGLDataType["INT_VEC4"] = 35669] = "INT_VEC4";
    EGLDataType[EGLDataType["BOOL"] = 35670] = "BOOL";
    EGLDataType[EGLDataType["BOOL_VEC2"] = 35671] = "BOOL_VEC2";
    EGLDataType[EGLDataType["BOOL_VEC3"] = 35672] = "BOOL_VEC3";
    EGLDataType[EGLDataType["BOOL_VEC4"] = 35673] = "BOOL_VEC4";
    EGLDataType[EGLDataType["FLOAT_MAT2"] = 35674] = "FLOAT_MAT2";
    EGLDataType[EGLDataType["FLOAT_MAT3"] = 35675] = "FLOAT_MAT3";
    EGLDataType[EGLDataType["FLOAT_MAT4"] = 35676] = "FLOAT_MAT4";
    EGLDataType[EGLDataType["SAMPLER_2D"] = 35678] = "SAMPLER_2D";
    EGLDataType[EGLDataType["SAMPLER_CUBE"] = 35680] = "SAMPLER_CUBE";
    EGLDataType[EGLDataType["SAMPLER_3D"] = 35679] = "SAMPLER_3D";
    EGLDataType[EGLDataType["SAMPLER_2D_SHADOW"] = 35682] = "SAMPLER_2D_SHADOW";
    EGLDataType[EGLDataType["FLOAT_MAT2x3"] = 35685] = "FLOAT_MAT2x3";
    EGLDataType[EGLDataType["FLOAT_MAT2x4"] = 35686] = "FLOAT_MAT2x4";
    EGLDataType[EGLDataType["FLOAT_MAT3x2"] = 35687] = "FLOAT_MAT3x2";
    EGLDataType[EGLDataType["FLOAT_MAT3x4"] = 35688] = "FLOAT_MAT3x4";
    EGLDataType[EGLDataType["FLOAT_MAT4x2"] = 35689] = "FLOAT_MAT4x2";
    EGLDataType[EGLDataType["FLOAT_MAT4x3"] = 35690] = "FLOAT_MAT4x3";
    EGLDataType[EGLDataType["SAMPLER_2D_ARRAY"] = 36289] = "SAMPLER_2D_ARRAY";
    EGLDataType[EGLDataType["SAMPLER_2D_ARRAY_SHADOW"] = 36292] = "SAMPLER_2D_ARRAY_SHADOW";
    EGLDataType[EGLDataType["SAMPLER_CUBE_SHADOW"] = 36293] = "SAMPLER_CUBE_SHADOW";
    EGLDataType[EGLDataType["UNSIGNED_INT_VEC2"] = 36294] = "UNSIGNED_INT_VEC2";
    EGLDataType[EGLDataType["UNSIGNED_INT_VEC3"] = 36295] = "UNSIGNED_INT_VEC3";
    EGLDataType[EGLDataType["UNSIGNED_INT_VEC4"] = 36296] = "UNSIGNED_INT_VEC4";
    EGLDataType[EGLDataType["INT_SAMPLER_2D"] = 36298] = "INT_SAMPLER_2D";
    EGLDataType[EGLDataType["INT_SAMPLER_3D"] = 36299] = "INT_SAMPLER_3D";
    EGLDataType[EGLDataType["INT_SAMPLER_CUBE"] = 36300] = "INT_SAMPLER_CUBE";
    EGLDataType[EGLDataType["INT_SAMPLER_2D_ARRAY"] = 36303] = "INT_SAMPLER_2D_ARRAY";
    EGLDataType[EGLDataType["UNSIGNED_INT_SAMPLER_2D"] = 36306] = "UNSIGNED_INT_SAMPLER_2D";
    EGLDataType[EGLDataType["UNSIGNED_INT_SAMPLER_3D"] = 36307] = "UNSIGNED_INT_SAMPLER_3D";
    EGLDataType[EGLDataType["UNSIGNED_INT_SAMPLER_CUBE"] = 36308] = "UNSIGNED_INT_SAMPLER_CUBE";
    EGLDataType[EGLDataType["UNSIGNED_INT_SAMPLER_2D_ARRAY"] = 36311] = "UNSIGNED_INT_SAMPLER_2D_ARRAY";
    EGLDataType[EGLDataType["TEXTURE_2D"] = 3553] = "TEXTURE_2D";
    EGLDataType[EGLDataType["TEXTURE_CUBE_MAP"] = 34067] = "TEXTURE_CUBE_MAP";
    EGLDataType[EGLDataType["TEXTURE_3D"] = 32879] = "TEXTURE_3D";
    EGLDataType[EGLDataType["TEXTURE_2D_ARRAY"] = 35866] = "TEXTURE_2D_ARRAY";
})(EGLDataType = exports.EGLDataType || (exports.EGLDataType = {}));
class GLDataType {
    static getGLTypeForTypedArray(typedArray) {
        if (typedArray instanceof Int8Array) {
            return EGLDataType.BYTE;
        }
        if (typedArray instanceof Uint8Array) {
            return EGLDataType.UNSIGNED_BYTE;
        }
        if (typedArray instanceof Uint8ClampedArray) {
            return EGLDataType.UNSIGNED_BYTE;
        }
        if (typedArray instanceof Int16Array) {
            return EGLDataType.SHORT;
        }
        if (typedArray instanceof Uint16Array) {
            return EGLDataType.UNSIGNED_SHORT;
        }
        if (typedArray instanceof Int32Array) {
            return EGLDataType.INT;
        }
        if (typedArray instanceof Uint32Array) {
            return EGLDataType.UNSIGNED_INT;
        }
        if (typedArray instanceof Float32Array) {
            return EGLDataType.FLOAT;
        }
        throw new Error('unsupported typed array type');
    }
    static getGLTypeForTypedArrayType(typedArrayType) {
        if (typedArrayType === Int8Array) {
            return EGLDataType.BYTE;
        }
        if (typedArrayType === Uint8Array) {
            return EGLDataType.UNSIGNED_BYTE;
        }
        if (typedArrayType === Uint8ClampedArray) {
            return EGLDataType.UNSIGNED_BYTE;
        }
        if (typedArrayType === Int16Array) {
            return EGLDataType.SHORT;
        }
        if (typedArrayType === Uint16Array) {
            return EGLDataType.UNSIGNED_SHORT;
        }
        if (typedArrayType === Int32Array) {
            return EGLDataType.INT;
        }
        if (typedArrayType === Uint32Array) {
            return EGLDataType.UNSIGNED_INT;
        }
        if (typedArrayType === Float32Array) {
            return EGLDataType.FLOAT;
        }
        throw new Error('unsupported typed array type');
    }
}
exports.GLDataType = GLDataType;


/***/ }),

/***/ "./src/render/program.ts":
/*!*******************************!*\
  !*** ./src/render/program.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Attributes = exports.Attribute = exports.Uniform = exports.ESetterType = exports.Program = exports.ProgramInfo = void 0;
const gldatatype_1 = __webpack_require__(/*! ./gldatatype */ "./src/render/gldatatype.ts");
var EShaderType;
(function (EShaderType) {
    EShaderType[EShaderType["VERTEX_SHADER"] = 0] = "VERTEX_SHADER";
    EShaderType[EShaderType["FRAGMENT_SHADER"] = 1] = "FRAGMENT_SHADER";
})(EShaderType || (EShaderType = {}));
class ProgramInfo {
    constructor(vsSrc, fsSrc) {
        this.vertexShaderSource = vsSrc;
        this.fragmentShaderSource = fsSrc;
    }
}
exports.ProgramInfo = ProgramInfo;
class Program {
    constructor(gl, id, name, program, vertexShader, fragmentShader) {
        this.usedTimes = 0;
        this.id = id;
        this.name = name;
        this.glProgram = program;
        this.glVertexShader = vertexShader;
        this.glFragmentShader = fragmentShader;
        this.uniforms = new Uniforms(gl, program);
        this.attributes = new Attributes(gl, program);
        console.log("program bingo!" + this.uniforms.map);
    }
    static createProgram(gl, programInfo) {
        console.log("create vs");
        let vertexShader = Program._createShader(gl, EShaderType.VERTEX_SHADER, programInfo.vertexShaderSource);
        console.log("create fs");
        let fragmentShader = Program._createShader(gl, EShaderType.FRAGMENT_SHADER, programInfo.fragmentShaderSource);
        let program = Program._createProgram(gl, [vertexShader, fragmentShader]);
        let ret = new Program(gl, "", "", program, vertexShader, fragmentShader);
        programInfo.program = ret;
        return ret;
    }
    static _createShader(gl, type, source) {
        let shader;
        if (type === EShaderType.VERTEX_SHADER) {
            shader = gl.createShader(gl.VERTEX_SHADER);
        }
        else if (type === EShaderType.FRAGMENT_SHADER) {
            shader = gl.createShader(gl.FRAGMENT_SHADER);
        }
        if (!shader) {
            console.error("Create shader false with invalid shader type ! ");
            return null;
        }
        gl.shaderSource(shader, source);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            console.error('error occured compiling the shaders:' + gl.getShaderInfoLog(shader));
            gl.deleteShader(shader);
            return null;
        }
        return shader;
    }
    static _createProgram(gl, shaders) {
        const program = gl.createProgram();
        shaders.forEach((shader) => {
            gl.attachShader(program, shader);
        });
        gl.linkProgram(program);
        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            const lastError = gl.getProgramInfoLog(program);
            console.log("Error in program linking:" + lastError);
            gl.deleteProgram(program);
            return null;
        }
        return program;
    }
}
exports.Program = Program;
Program.programIDCount = 0;
var ESetterType;
(function (ESetterType) {
    ESetterType[ESetterType["NormalSetter"] = 0] = "NormalSetter";
    ESetterType[ESetterType["SamplerSetter"] = 1] = "SamplerSetter";
})(ESetterType = exports.ESetterType || (exports.ESetterType = {}));
class Setter {
    constructor() {
        this.type = ESetterType.NormalSetter;
    }
}
class Setter1f extends Setter {
    setValue(gl, addr, value) {
        gl.uniform1f(addr, value);
    }
}
class Setter1fv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform1fv(addr, value);
    }
}
class Setter2fv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform2fv(addr, value);
    }
}
class Setter3fv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform3fv(addr, value);
    }
}
class Setter4fv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform4fv(addr, value);
    }
}
class Setter1i extends Setter {
    setValue(gl, addr, value) {
        gl.uniform1i(addr, value);
    }
}
class Setter1iv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform1iv(addr, value);
    }
}
class Setter2iv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform2iv(addr, value);
    }
}
class Setter3iv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform3iv(addr, value);
    }
}
class Setter4iv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform4iv(addr, value);
    }
}
class Setter1ui extends Setter {
    setValue(gl, addr, value) {
        gl.uniform1ui(addr, value);
    }
}
class Setter1uiv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform1uiv(addr, value);
    }
}
class Setter2uiv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform2uiv(addr, value);
    }
}
class Setter3uiv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform3uiv(addr, value);
    }
}
class Setter4uiv extends Setter {
    setValue(gl, addr, value) {
        gl.uniform4uiv(addr, value);
    }
}
class SetterMatrix2fv extends Setter {
    setValue(gl, addr, value) {
        gl.uniformMatrix2fv(addr, false, value);
    }
}
class SetterMatrix3fv extends Setter {
    setValue(gl, addr, value) {
        gl.uniformMatrix3fv(addr, false, value);
    }
}
class SetterMatrix4fv extends Setter {
    setValue(gl, addr, value) {
        gl.uniformMatrix4fv(addr, false, value);
    }
}
class SetterMatrix23f extends Setter {
    setValue(gl, addr, value) {
        gl.uniformMatrix2x3fv(addr, false, value);
    }
}
class SetterMatrix32f extends Setter {
    setValue(gl, addr, value) {
        gl.uniformMatrix3x2fv(addr, false, value);
    }
}
class SetterMatrix24f extends Setter {
    setValue(gl, addr, value) {
        gl.uniformMatrix3x2fv(addr, false, value);
    }
}
class SetterMatrix42f extends Setter {
    setValue(gl, addr, value) {
        gl.uniformMatrix4x2fv(addr, false, value);
    }
}
class SetterMatrix34f extends Setter {
    setValue(gl, addr, value) {
        gl.uniformMatrix3x4fv(addr, false, value);
    }
}
class SetterMatrix43f extends Setter {
    setValue(gl, addr, value) {
        gl.uniformMatrix4x3fv(addr, false, value);
    }
}
class SetterSampler extends Setter {
    constructor(bindPoint) {
        super();
        this.bindPoint = bindPoint;
        this.type = ESetterType.SamplerSetter;
    }
    setValue(gl, addr, value, param) {
        let textureUnit = param || 0;
        gl.activeTexture(gl.TEXTURE0 + textureUnit);
        gl.bindTexture(this.bindPoint, value);
        gl.uniform1i(addr, textureUnit);
    }
}
class Uniforms {
    constructor(gl, program) {
        this.map = {};
        this.seq = [];
        this._textureCount = 0;
        let n = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);
        for (let i = 0; i < n; ++i) {
            let info = gl.getActiveUniform(program, i);
            let addr = gl.getUniformLocation(program, info.name);
            this._parseUniform(info, addr);
            console.log("parse uniform:" + info.name + "|" + addr);
        }
    }
    _parseUniform(info, addr) {
        var id = info.name | 0;
        this._addUniform(id, info.name, addr, info.type);
        console.log("add uniform:" + info.name + " | " + info.type);
    }
    _addUniform(id, name, addr, uniformType) {
        let uniform = new Uniform(id, addr, Uniforms.typeMap[uniformType]);
        if (uniform.setter.type === ESetterType.SamplerSetter) {
            uniform.textureUnit = this._textureCount;
            this._textureCount++;
        }
        this.map[name] = uniform;
        this.seq.push(uniform);
    }
}
Uniforms.typeMap = {
    [gldatatype_1.EGLDataType.FLOAT]: new Setter1f(),
    [gldatatype_1.EGLDataType.FLOAT_VEC2]: new Setter2fv(),
    [gldatatype_1.EGLDataType.FLOAT_VEC3]: new Setter3fv(),
    [gldatatype_1.EGLDataType.FLOAT_VEC4]: new Setter4fv(),
    [gldatatype_1.EGLDataType.INT]: new Setter1i(),
    [gldatatype_1.EGLDataType.INT_VEC2]: new Setter2iv(),
    [gldatatype_1.EGLDataType.INT_VEC3]: new Setter3iv(),
    [gldatatype_1.EGLDataType.INT_VEC4]: new Setter4iv(),
    [gldatatype_1.EGLDataType.UNSIGNED_INT]: new Setter1ui(),
    [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC2]: new Setter2uiv(),
    [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC3]: new Setter3uiv(),
    [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC4]: new Setter4uiv(),
    [gldatatype_1.EGLDataType.BOOL]: new Setter1ui(),
    [gldatatype_1.EGLDataType.BOOL_VEC2]: new Setter2uiv(),
    [gldatatype_1.EGLDataType.BOOL_VEC3]: new Setter3uiv(),
    [gldatatype_1.EGLDataType.BOOL_VEC4]: new Setter4uiv(),
    [gldatatype_1.EGLDataType.FLOAT_MAT2]: new SetterMatrix2fv(),
    [gldatatype_1.EGLDataType.FLOAT_MAT3]: new SetterMatrix3fv(),
    [gldatatype_1.EGLDataType.FLOAT_MAT4]: new SetterMatrix4fv(),
    [gldatatype_1.EGLDataType.FLOAT_MAT2x3]: new SetterMatrix23f(),
    [gldatatype_1.EGLDataType.FLOAT_MAT2x4]: new SetterMatrix24f(),
    [gldatatype_1.EGLDataType.FLOAT_MAT3x2]: new SetterMatrix32f(),
    [gldatatype_1.EGLDataType.FLOAT_MAT3x4]: new SetterMatrix34f(),
    [gldatatype_1.EGLDataType.FLOAT_MAT4x2]: new SetterMatrix42f(),
    [gldatatype_1.EGLDataType.FLOAT_MAT4x3]: new SetterMatrix43f(),
    [gldatatype_1.EGLDataType.SAMPLER_2D]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_2D),
    [gldatatype_1.EGLDataType.SAMPLER_CUBE]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_CUBE_MAP),
    [gldatatype_1.EGLDataType.SAMPLER_3D]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_3D),
    [gldatatype_1.EGLDataType.SAMPLER_2D_SHADOW]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_2D),
    [gldatatype_1.EGLDataType.SAMPLER_2D_ARRAY]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_2D_ARRAY),
    [gldatatype_1.EGLDataType.SAMPLER_2D_ARRAY_SHADOW]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_2D_ARRAY),
    [gldatatype_1.EGLDataType.INT_SAMPLER_2D]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_2D),
    [gldatatype_1.EGLDataType.INT_SAMPLER_3D]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_3D),
    [gldatatype_1.EGLDataType.INT_SAMPLER_CUBE]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_CUBE_MAP),
    [gldatatype_1.EGLDataType.INT_SAMPLER_2D_ARRAY]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_2D_ARRAY),
    [gldatatype_1.EGLDataType.UNSIGNED_INT_SAMPLER_2D]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_2D),
    [gldatatype_1.EGLDataType.UNSIGNED_INT_SAMPLER_3D]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_3D),
    [gldatatype_1.EGLDataType.UNSIGNED_INT_SAMPLER_CUBE]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_CUBE_MAP),
    [gldatatype_1.EGLDataType.UNSIGNED_INT_SAMPLER_2D_ARRAY]: new SetterSampler(gldatatype_1.EGLDataType.TEXTURE_2D_ARRAY),
};
class Uniform {
    constructor(id, addr, setter) {
        this.id = id;
        this.addr = addr;
        this.setter = setter;
    }
}
exports.Uniform = Uniform;
class AttribFloatSetter extends Setter {
    setValue(gl, addr, value) {
        let attrib = value;
        if (attrib.isConstantToShader === true) {
            gl.disableVertexAttribArray(addr);
            let setValue = attrib.buffer;
            switch (setValue.length) {
                case 4:
                    gl.vertexAttrib4fv(addr, setValue.buffer);
                    break;
                case 3:
                    gl.vertexAttrib3fv(addr, setValue.buffer);
                    break;
                case 2:
                    gl.vertexAttrib2fv(addr, setValue.buffer);
                    break;
                case 1:
                    gl.vertexAttrib1fv(addr, setValue.buffer);
                    break;
                default:
                    throw new Error('the length of a float constant value must be between 1 and 4!');
            }
        }
        else {
            if (!attrib.glBuffer) {
                attrib.glBuffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                gl.bufferData(gl.ARRAY_BUFFER, attrib.buffer, gl.STATIC_DRAW);
            }
            else {
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
            }
            gl.enableVertexAttribArray(addr);
            gl.vertexAttribPointer(addr, attrib.numComponents, gl.FLOAT, attrib.normalized || false, attrib.sizeofStride || 0, attrib.offsetofStride || 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }
    }
}
class AttribIntSetter extends Setter {
    setValue(gl, addr, value) {
        let attrib = value;
        if (attrib.isConstantToShader !== true) {
            if (!attrib.glBuffer) {
                attrib.glBuffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                gl.bufferData(gl.ARRAY_BUFFER, attrib.buffer, gl.STATIC_DRAW);
            }
            else {
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
            }
            gl.enableVertexAttribArray(addr);
            gl.vertexAttribIPointer(addr, attrib.numComponents, attrib.glType || gl.INT, attrib.sizeofStride || 0, attrib.offsetofStride || 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }
    }
}
class AttribUIntSetter extends Setter {
    setValue(gl, addr, value) {
        let attrib = value;
        if (attrib.isConstantToShader !== true) {
            if (!attrib.glBuffer) {
                attrib.glBuffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                gl.bufferData(gl.ARRAY_BUFFER, attrib.buffer, gl.STATIC_DRAW);
            }
            else {
                gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
            }
            gl.enableVertexAttribArray(addr);
            gl.vertexAttribIPointer(addr, attrib.numComponents, attrib.glType || gl.UNSIGNED_INT, attrib.sizeofStride || 0, attrib.offsetofStride || 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }
    }
}
class Attribute {
    constructor(id, addr, type) {
        this.id = id;
        this.addr = addr;
        this.setter = Attributes.typeMap[type];
        this.type = type;
    }
}
exports.Attribute = Attribute;
class Attributes {
    constructor(gl, program) {
        this.map = {};
        this.seq = [];
        const numAttribs = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);
        for (let i = 0; i < numAttribs; ++i) {
            const attribInfo = gl.getActiveAttrib(program, i);
            if (this._isBuiltIn(attribInfo)) {
                continue;
            }
            const index = gl.getAttribLocation(program, attribInfo.name);
            const id = attribInfo.name | 0;
            const attribute = new Attribute(id, index, attribInfo.type);
            this.map[attribInfo.name] = attribute;
            this.seq.push(attribute);
            console.log("parse attribute:" + attribInfo.name + "|" + index);
        }
    }
    _isBuiltIn(info) {
        const name = info.name;
        return name.startsWith("gl_") || name.startsWith("webgl_");
    }
}
exports.Attributes = Attributes;
Attributes.typeMap = {
    [gldatatype_1.EGLDataType.FLOAT]: new AttribFloatSetter(),
    [gldatatype_1.EGLDataType.FLOAT_VEC2]: new AttribFloatSetter(),
    [gldatatype_1.EGLDataType.FLOAT_VEC3]: new AttribFloatSetter(),
    [gldatatype_1.EGLDataType.FLOAT_VEC4]: new AttribFloatSetter(),
    [gldatatype_1.EGLDataType.INT]: new AttribIntSetter(),
    [gldatatype_1.EGLDataType.INT_VEC2]: new AttribIntSetter(),
    [gldatatype_1.EGLDataType.INT_VEC3]: new AttribIntSetter(),
    [gldatatype_1.EGLDataType.INT_VEC4]: new AttribIntSetter(),
    [gldatatype_1.EGLDataType.UNSIGNED_INT]: new AttribUIntSetter(),
    [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC2]: new AttribUIntSetter(),
    [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC3]: new AttribUIntSetter(),
    [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC4]: new AttribUIntSetter(),
    [gldatatype_1.EGLDataType.BOOL]: new AttribIntSetter(),
    [gldatatype_1.EGLDataType.BOOL_VEC2]: new AttribIntSetter(),
    [gldatatype_1.EGLDataType.BOOL_VEC3]: new AttribIntSetter(),
    [gldatatype_1.EGLDataType.BOOL_VEC4]: new AttribIntSetter(),
};


/***/ }),

/***/ "./src/render/renderer.ts":
/*!********************************!*\
  !*** ./src/render/renderer.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Renderer = exports.ERenderGroupType = void 0;
const drawobject_1 = __webpack_require__(/*! ./drawobject */ "./src/render/drawobject.ts");
const program_1 = __webpack_require__(/*! ./program */ "./src/render/program.ts");
const renderstate_1 = __webpack_require__(/*! ./renderstate */ "./src/render/renderstate.ts");
const helper_1 = __webpack_require__(/*! ../utils/helper */ "./src/utils/helper.ts");
var ERenderGroupType;
(function (ERenderGroupType) {
    ERenderGroupType[ERenderGroupType["BackGround"] = 0] = "BackGround";
    ERenderGroupType[ERenderGroupType["Opaque"] = 1] = "Opaque";
    ERenderGroupType[ERenderGroupType["Transparence"] = 2] = "Transparence";
    ERenderGroupType[ERenderGroupType["UILayout"] = 3] = "UILayout";
})(ERenderGroupType = exports.ERenderGroupType || (exports.ERenderGroupType = {}));
class Renderer {
    constructor(gl) {
        this.calldraw = 0;
        this.gl = gl;
        this._setupRenderGroups();
        this._state = new renderstate_1.State(gl);
        this._state.enable(renderstate_1.EEnabledCapability.DEPTH_TEST);
        this._state.enable(renderstate_1.EEnabledCapability.CULL_FACE);
    }
    clearRenderGroup() {
        for (const key in this.renderGroups) {
            if (this.renderGroups.hasOwnProperty(key)) {
                this.renderGroups[key].clear();
            }
        }
    }
    addDrawAbleObj(drawObj, renderType) {
        if (this.renderGroups.hasOwnProperty(renderType)) {
            const renderGroup = this.renderGroups[renderType];
            if (renderGroup.renderListArray.length === 0) {
                renderGroup.renderListArray.push(new drawobject_1.RenderList());
            }
            renderGroup.renderListArray[0].renderObjs.push(drawObj);
        }
    }
    clear() {
        let gl = this.gl;
        gl.clearColor(0.3, 0.3, 0.3, 1.0);
        gl.clearDepth(1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        this.calldraw = 0;
    }
    render() {
        let count = 0;
        for (const key in this.renderGroups) {
            if (this.renderGroups.hasOwnProperty(key)) {
                const renderGroup = this.renderGroups[key];
                renderGroup.renderListArray.forEach(renderList => {
                    this._drawObjects(renderList);
                    count += renderList.renderObjs.length;
                });
            }
        }
    }
    clearColorBuffer() {
        this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    }
    viewport(vp) {
        this.gl.viewport(vp.x, vp.y, vp.z, vp.w);
        this._viewport = vp.clone();
    }
    _setupRenderGroups() {
        this.renderGroups = {};
        this.renderGroups[ERenderGroupType.BackGround] = new drawobject_1.RenderGroup(ERenderGroupType.BackGround, 500);
        this.renderGroups[ERenderGroupType.Opaque] = new drawobject_1.RenderGroup(ERenderGroupType.Opaque, 400);
        this.renderGroups[ERenderGroupType.Transparence] = new drawobject_1.RenderGroup(ERenderGroupType.Transparence, 300);
        this.renderGroups[ERenderGroupType.UILayout] = new drawobject_1.RenderGroup(ERenderGroupType.UILayout, 100);
    }
    _drawObject(drawObj) {
        if (!drawObj.vertexInfo && !drawObj.programInfo) {
            throw new Error("there is invalid vertexInfo or programInfo in vertexInfo");
        }
        drawObj.bindIBO();
        if (this.lastUsedProgramInfo !== drawObj.programInfo) {
            if (!drawObj.programInfo.program) {
                console.log("create program");
                program_1.Program.createProgram(this.gl, drawObj.programInfo);
            }
            console.log("setup program");
            this.lastUsedProgramInfo = drawObj.programInfo;
            this.gl.useProgram(drawObj.programInfo.program.glProgram);
        }
        let vertexInfo = drawObj.vertexInfo;
        let program = drawObj.programInfo.program;
        for (const key in program.attributes.map) {
            if (program.attributes.map.hasOwnProperty(key)) {
                if (vertexInfo.hasOwnProperty(key)) {
                    const attribInfo = vertexInfo[key];
                    const attribute = program.attributes.map[key];
                    attribute.setter.setValue(this.gl, attribute.addr, attribInfo);
                }
                else {
                    console.warn("vertexInfo isn't has property:" + key);
                }
            }
        }
        let uniforms = drawObj.uniforms;
        for (const key in program.uniforms.map) {
            if (program.uniforms.map.hasOwnProperty(key)) {
                if (uniforms && uniforms.hasOwnProperty(key)) {
                    const uniformValue = uniforms[key];
                    const uniform = program.uniforms.map[key];
                    if (uniform.setter.type === program_1.ESetterType.SamplerSetter) {
                        uniform.setter.setValue(this.gl, uniform.addr, uniformValue, uniform.textureUnit);
                    }
                    else {
                        uniform.setter.setValue(this.gl, uniform.addr, uniformValue);
                    }
                }
                else {
                    console.warn("drawobj uniforms isn't has:" + key);
                }
            }
        }
        let glDrawType = this.gl.TRIANGLES;
        if (drawObj.drawType === drawobject_1.EDrawType.Points) {
            glDrawType = this.gl.POINTS;
        }
        else if (drawObj.drawType === drawobject_1.EDrawType.Lines) {
            glDrawType = this.gl.LINES;
        }
        if (!drawObj.vertexInfo.indices) {
            this.gl.drawArrays(glDrawType, drawObj.drawArrayOffset, drawObj.drawArrayCount);
        }
        else {
            this.gl.drawElements(glDrawType, drawObj.drawArrayCount, drawObj.vertexInfo.indices.glType | this.gl.UNSIGNED_BYTE, drawObj.drawArrayOffset);
        }
    }
    _drawObjects(renderList) {
        helper_1.Helper.calculateTimestamp(() => {
            renderList.renderObjs.forEach(renderObj => {
                this._drawObject(renderObj);
            });
        }, "2");
    }
}
exports.Renderer = Renderer;


/***/ }),

/***/ "./src/render/renderstate.ts":
/*!***********************************!*\
  !*** ./src/render/renderstate.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.State = exports.EEnabledCapability = void 0;
var EEnabledCapability;
(function (EEnabledCapability) {
    EEnabledCapability[EEnabledCapability["BLEND"] = 0] = "BLEND";
    EEnabledCapability[EEnabledCapability["CULL_FACE"] = 1] = "CULL_FACE";
    EEnabledCapability[EEnabledCapability["DEPTH_TEST"] = 2] = "DEPTH_TEST";
    EEnabledCapability[EEnabledCapability["DITHER"] = 3] = "DITHER";
    EEnabledCapability[EEnabledCapability["POLYGON_OFFSET_FILL"] = 4] = "POLYGON_OFFSET_FILL";
    EEnabledCapability[EEnabledCapability["SAMPLE_ALPHA_TO_COVERAGE"] = 5] = "SAMPLE_ALPHA_TO_COVERAGE";
    EEnabledCapability[EEnabledCapability["SAMPLE_COVERAGE"] = 6] = "SAMPLE_COVERAGE";
    EEnabledCapability[EEnabledCapability["SCISSOR_TEST"] = 7] = "SCISSOR_TEST";
    EEnabledCapability[EEnabledCapability["STENCIL_TEST"] = 8] = "STENCIL_TEST";
})(EEnabledCapability = exports.EEnabledCapability || (exports.EEnabledCapability = {}));
class State {
    constructor(gl) {
        this.gl = gl;
        this.enabledCapabilities = {};
        this._initEnabledCapabilitieConvertor();
    }
    _initEnabledCapabilitieConvertor() {
        if (this._enabledCapabilitieNameMap) {
            return;
        }
        this._enabledCapabilitieNameMap = {};
        let keys = Object.keys(EEnabledCapability);
        keys.forEach(key => {
            let keynum = EEnabledCapability[key];
            switch (keynum) {
                case EEnabledCapability.BLEND:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.BLEND;
                    break;
                case EEnabledCapability.CULL_FACE:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.CULL_FACE;
                    break;
                case EEnabledCapability.DEPTH_TEST:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.DEPTH_TEST;
                    break;
                case EEnabledCapability.DITHER:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.DITHER;
                    break;
                case EEnabledCapability.POLYGON_OFFSET_FILL:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.POLYGON_OFFSET_FILL;
                    break;
                case EEnabledCapability.SAMPLE_ALPHA_TO_COVERAGE:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.SAMPLE_ALPHA_TO_COVERAGE;
                    break;
                case EEnabledCapability.SAMPLE_COVERAGE:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.SAMPLE_COVERAGE;
                    break;
                case EEnabledCapability.SCISSOR_TEST:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.SCISSOR_TEST;
                    break;
                case EEnabledCapability.STENCIL_TEST:
                    this._enabledCapabilitieNameMap[keynum] = this.gl.STENCIL_TEST;
                    break;
            }
        });
    }
    enable(id) {
        let glKey = this._enabledCapabilitieNameMap[id];
        if (this.enabledCapabilities[glKey] !== true) {
            this.gl.enable(glKey);
            this.enabledCapabilities[glKey] = true;
        }
    }
    disable(id) {
        let glKey = this._enabledCapabilitieNameMap[id];
        if (this.enabledCapabilities[glKey] !== false) {
            this.gl.disable(glKey);
            this.enabledCapabilities[glKey] = false;
        }
    }
}
exports.State = State;


/***/ }),

/***/ "./src/render/shaders/lambert_fs.ts":
/*!******************************************!*\
  !*** ./src/render/shaders/lambert_fs.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.default = `
precision mediump float;

varying vec4 fragColor;
void main() {
    gl_FragColor = fragColor;
}
`;


/***/ }),

/***/ "./src/render/shaders/lambert_vs.ts":
/*!******************************************!*\
  !*** ./src/render/shaders/lambert_vs.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.default = `
attribute vec4 position;
attribute vec4 normal;
uniform mat4 transform;
uniform vec3 lightColor;        //入射光颜色
uniform vec3 lightDir;          //入射光方向
uniform vec3 lightColorAmbient; // 环境光颜色
varying vec4 fragColor;         //这里作为顶点光源计算颜色输出到fs

void main() {
    gl_Position = transform * position;
    vec4 aColor = vec4(1.0,1.0,1.0,1.0);
    //lambert光照模型计算
    vec3 normalizeNormal = normalize(vec3(normal));// 归一化法向量
    float cos = max(dot(lightDir, normalizeNormal), 0.0);// 计算入射角余弦值
    vec3 diffuse = lightColor * vec3(aColor) * cos;// 计算漫反射颜色
    vec3 ambient = lightColorAmbient * aColor.rgb;// 计算环境光反射颜色
    fragColor = vec4(diffuse + ambient, aColor.a); // outColor = ambient + diffuse
}
`;


/***/ }),

/***/ "./src/render/shaders/normal_fragment.ts":
/*!***********************************************!*\
  !*** ./src/render/shaders/normal_fragment.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.default = `
precision mediump float;
void main() {
    gl_FragColor = vec4(1.0,1.0,1.0,1.0);  // green
}
`;


/***/ }),

/***/ "./src/render/shaders/normal_vertex.ts":
/*!*********************************************!*\
  !*** ./src/render/shaders/normal_vertex.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.default = `
attribute vec4 position;
uniform mat4 transform;
void main() {
    gl_Position = transform * position;
}
`;


/***/ }),

/***/ "./src/thirdpart/loaders/fileloader.ts":
/*!*********************************************!*\
  !*** ./src/thirdpart/loaders/fileloader.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.FileLoader = void 0;
const mapcache_1 = __webpack_require__(/*! ./mapcache */ "./src/thirdpart/loaders/mapcache.ts");
const loadingmanager_1 = __webpack_require__(/*! ./loadingmanager */ "./src/thirdpart/loaders/loadingmanager.ts");
class FileLoader extends loadingmanager_1.LoadingManager {
    constructor() {
        super();
        this._cache = new mapcache_1.MapCache();
        this._loading = {};
    }
    static get singleton() {
        if (FileLoader._singleton === undefined) {
            FileLoader._singleton = new FileLoader();
        }
        return FileLoader._singleton;
    }
    setPath(path) {
        this._path = path;
    }
    load(url, onLoad, onProgress, onError) {
        let loadUrl = url || "";
        if (this._path) {
            loadUrl = this._path + loadUrl;
        }
        let cached = this._cache.get(url);
        if (cached !== undefined) {
            this.itemStart(url);
            setTimeout(() => {
                if (onLoad)
                    onLoad(cached);
                this.itemEnd(url);
            }, 0);
            return cached;
        }
        if (this._loading[url] != undefined) {
            this._loading[url].push({
                onLoad: onLoad,
                onProgress: onProgress,
                onErr: onError
            });
            return;
        }
        let dataUriRegex = /^data:(.*?)(;base64)?,(.*)$/;
        let dataUriRegexResult = url.match(dataUriRegex);
        this._loading[url] = [];
        this._loading[url].push({
            onLoad: onLoad,
            onProgress: onProgress,
            onErr: onError
        });
        this._fetchResource(url).then(resp => {
            let callbacks = this._loading[url];
            delete this._loading[url];
            if (resp.ok === true) {
                console.log("fileloader load ok......");
                callbacks.forEach(callback => {
                    if (callback.onLoad) {
                        callback.onLoad(resp);
                    }
                });
                this.itemEnd(url);
            }
            else {
                this.itemError(url);
                this.itemEnd(url);
            }
        });
        this.itemStart(url);
    }
    _fetchResource(url) {
        return fetch(url);
    }
}
exports.FileLoader = FileLoader;


/***/ }),

/***/ "./src/thirdpart/loaders/gltfloader.ts":
/*!*********************************************!*\
  !*** ./src/thirdpart/loaders/gltfloader.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.GLTFLoader = void 0;
const mapcache_1 = __webpack_require__(/*! ./mapcache */ "./src/thirdpart/loaders/mapcache.ts");
const fileloader_1 = __webpack_require__(/*! ./fileloader */ "./src/thirdpart/loaders/fileloader.ts");
const loadingmanager_1 = __webpack_require__(/*! ./loadingmanager */ "./src/thirdpart/loaders/loadingmanager.ts");
const loaderutils_1 = __webpack_require__(/*! ./loaderutils */ "./src/thirdpart/loaders/loaderutils.ts");
const geometry_1 = __webpack_require__(/*! ../../render/geometry */ "./src/render/geometry.ts");
const drawobject_1 = __webpack_require__(/*! ../../render/drawobject */ "./src/render/drawobject.ts");
const mesh_1 = __webpack_require__(/*! ../../core/mesh */ "./src/core/mesh.ts");
const primitive_1 = __webpack_require__(/*! ../../core/primitive */ "./src/core/primitive.ts");
const scenenode_1 = __webpack_require__(/*! ../../core/scenenode */ "./src/core/scenenode.ts");
const entity_1 = __webpack_require__(/*! ../../core/entity */ "./src/core/entity.ts");
const scene_1 = __webpack_require__(/*! ../../core/scene */ "./src/core/scene.ts");
const matrix4_1 = __webpack_require__(/*! ../../math/matrix4 */ "./src/math/matrix4.ts");
const vector3_1 = __webpack_require__(/*! ../../math/vector3 */ "./src/math/vector3.ts");
const vector4_1 = __webpack_require__(/*! ../../math/vector4 */ "./src/math/vector4.ts");
const BINARY_EXTENSION_BUFFER_NAME = 'binary_glTF';
const BINARY_EXTENSION_HEADER_MAGIC = 'glTF';
const BINARY_EXTENSION_HEADER_LENGTH = 12;
const BINARY_EXTENSION_CHUNK_TYPES = { JSON: 0x4E4F534A, BIN: 0x004E4942 };
const EXTENSIONS = {
    KHR_BINARY_GLTF: 'KHR_binary_glTF',
    KHR_DRACO_MESH_COMPRESSION: 'KHR_draco_mesh_compression',
    KHR_LIGHTS_PUNCTUAL: 'KHR_lights_punctual',
    KHR_MATERIALS_PBR_SPECULAR_GLOSSINESS: 'KHR_materials_pbrSpecularGlossiness',
    KHR_MATERIALS_UNLIT: 'KHR_materials_unlit',
    KHR_TEXTURE_TRANSFORM: 'KHR_texture_transform',
    MSFT_TEXTURE_DDS: 'MSFT_texture_dds'
};
let ATTRIBUTES = {
    POSITION: 'position',
    NORMAL: 'normal',
    TANGENT: 'tangent',
    TEXCOORD_0: 'uv',
    TEXCOORD_1: 'uv2',
    COLOR_0: 'color',
    WEIGHTS_0: 'skinWeight',
    JOINTS_0: 'skinIndex',
};
let WEBGL_COMPONENT_TYPES = {
    5120: Int8Array,
    5121: Uint8Array,
    5122: Int16Array,
    5123: Uint16Array,
    5125: Uint32Array,
    5126: Float32Array
};
let WEBGL_TYPE_SIZES = {
    'SCALAR': 1,
    'VEC2': 2,
    'VEC3': 3,
    'VEC4': 4,
    'MAT2': 4,
    'MAT3': 9,
    'MAT4': 16
};
let WEBGL_CONSTANTS = {
    FLOAT: 5126,
    FLOAT_MAT3: 35675,
    FLOAT_MAT4: 35676,
    FLOAT_VEC2: 35664,
    FLOAT_VEC3: 35665,
    FLOAT_VEC4: 35666,
    LINEAR: 9729,
    REPEAT: 10497,
    SAMPLER_2D: 35678,
    POINTS: 0,
    LINES: 1,
    LINE_LOOP: 2,
    LINE_STRIP: 3,
    TRIANGLES: 4,
    TRIANGLE_STRIP: 5,
    TRIANGLE_FAN: 6,
    UNSIGNED_BYTE: 5121,
    UNSIGNED_SHORT: 5123
};
class GLTFLoader extends loadingmanager_1.LoadingManager {
    constructor() {
        super();
        this._cache = new mapcache_1.MapCache();
    }
    static get singleton() {
        if (GLTFLoader._singleton === undefined) {
            GLTFLoader._singleton = new GLTFLoader();
        }
        return GLTFLoader._singleton;
    }
    load(url, onLoad, onProgress, onError) {
        let loader = fileloader_1.FileLoader.singleton;
        if (!this.resourcePath) {
            this.resourcePath = loaderutils_1.LoaderUtils.extractUrlBase(url);
        }
        console.log("start load ...");
        let _onError = (e) => {
            if (onError) {
                onError(e);
            }
            else {
                console.error(e);
            }
            this.itemError(url);
            this.itemEnd(url);
        };
        loader.load(url, (resp) => {
            console.log(`load gltf[${url}] : ${resp.ok} }`);
            try {
                resp.arrayBuffer().then((buffer) => {
                    this.parse(buffer, (gltf) => {
                        onLoad(gltf);
                    }, _onError);
                });
            }
            catch (e) {
                console.error("gltfloader exception :" + e);
            }
        });
    }
    parse(data, onLoad, onError) {
        let content;
        let extensions = {};
        if (typeof data === 'string') {
            content = data;
        }
        else {
            var magic = loaderutils_1.LoaderUtils.decodeText(new Uint8Array(data, 0, 4));
            if (magic === BINARY_EXTENSION_HEADER_MAGIC) {
                console.log("it's glb file data stream");
                try {
                    extensions[EXTENSIONS.KHR_BINARY_GLTF] = new GLTFBinaryExtension(data);
                }
                catch (error) {
                    if (onError)
                        onError(error);
                    return;
                }
                content = extensions[EXTENSIONS.KHR_BINARY_GLTF].content;
            }
            else {
                console.log("it's gltf file data stream");
                content = loaderutils_1.LoaderUtils.decodeText(new Uint8Array(data));
            }
        }
        var json = JSON.parse(content);
        if (json.asset === undefined || json.asset.version[0] < 2) {
            console.error("gltf Unsupported asset");
            if (onError)
                onError(('THREE.GLTFLoader: Unsupported asset. glTF versions >=2.0 are supported. Use LegacyGLTFLoader instead.'));
            return;
        }
        if (json.extensionsUsed) {
            console.log("need extensions!!!");
        }
        var parser = new GLTFParser(json, extensions, { path: this.resourcePath });
        parser.parse((scene, scenes, cameras, animations, json) => {
            let glTF = {
                scene: scene,
                scenes: scenes,
                cameras: cameras,
                animations: animations,
                asset: json.asset,
                parser: parser,
                userData: {}
            };
            console.log("gltf parser ok..............:" + glTF.scene);
            onLoad(glTF);
        }, onError);
    }
}
exports.GLTFLoader = GLTFLoader;
class GLTFBinaryExtension {
    constructor(data) {
        this.name = EXTENSIONS.KHR_BINARY_GLTF;
        this.content = null;
        this.body = null;
        var headerView = new DataView(data, 0, BINARY_EXTENSION_HEADER_LENGTH);
        this.header = {
            magic: loaderutils_1.LoaderUtils.decodeText(new Uint8Array(data.slice(0, 4))),
            version: headerView.getUint32(4, true),
            length: headerView.getUint32(8, true)
        };
        if (this.header.magic !== BINARY_EXTENSION_HEADER_MAGIC) {
            throw new Error('THREE.GLTFLoader: Unsupported glTF-Binary header.');
        }
        else if (this.header.version < 2.0) {
            throw new Error('THREE.GLTFLoader: Legacy binary file detected. Use LegacyGLTFLoader instead.');
        }
        var chunkView = new DataView(data, BINARY_EXTENSION_HEADER_LENGTH);
        var chunkIndex = 0;
        while (chunkIndex < chunkView.byteLength) {
            var chunkLength = chunkView.getUint32(chunkIndex, true);
            chunkIndex += 4;
            var chunkType = chunkView.getUint32(chunkIndex, true);
            chunkIndex += 4;
            if (chunkType === BINARY_EXTENSION_CHUNK_TYPES.JSON) {
                var contentArray = new Uint8Array(data, BINARY_EXTENSION_HEADER_LENGTH + chunkIndex, chunkLength);
                this.content = loaderutils_1.LoaderUtils.decodeText(contentArray);
            }
            else if (chunkType === BINARY_EXTENSION_CHUNK_TYPES.BIN) {
                var byteOffset = BINARY_EXTENSION_HEADER_LENGTH + chunkIndex;
                this.body = data.slice(byteOffset, byteOffset + chunkLength);
            }
            chunkIndex += chunkLength;
        }
        if (this.content === null) {
            throw new Error('THREE.GLTFLoader: JSON content not found.');
        }
    }
}
class GLTFParser {
    constructor(json, extensions, options) {
        this.primitiveCache = {};
        this.json = json || {};
        this.extensions = extensions || {};
        this.options = options || {};
        this.cache = new mapcache_1.MapCache();
        this.fileLoader = fileloader_1.FileLoader.singleton;
    }
    parse(onLoad, onError) {
        var json = this.json;
        this.markDefs();
        this.getMultiDependencies([
            'scene',
            'animation',
            'camera'
        ]).then((dependencies) => {
            var scenes = dependencies.scenes || [];
            var scene = scenes[json.scene || 0];
            var animations = dependencies.animations || [];
            var cameras = dependencies.cameras || [];
            console.log("load All ok .......:" + scenes + "|" + scene);
            onLoad(scene, scenes, cameras, animations, json);
        }).catch(onError);
    }
    markDefs() {
        let nodeDefs = this.json.nodes || [];
        let skinDefs = this.json.skins || [];
        let meshDefs = this.json.meshes || [];
        let meshReferences = {};
        let meshUses = {};
        for (let skinIndex = 0, skinLength = skinDefs.length; skinIndex < skinLength; skinIndex++) {
            let joints = skinDefs[skinIndex].joints;
            for (let i = 0, il = joints.length; i < il; i++) {
                nodeDefs[joints[i]].isBone = true;
            }
        }
        for (let nodeIndex = 0, nodeLength = nodeDefs.length; nodeIndex < nodeLength; nodeIndex++) {
            let nodeDef = nodeDefs[nodeIndex];
            if (nodeDef.mesh !== undefined) {
                if (meshReferences[nodeDef.mesh] === undefined) {
                    meshReferences[nodeDef.mesh] = meshUses[nodeDef.mesh] = 0;
                }
                meshReferences[nodeDef.mesh]++;
                if (nodeDef.skin !== undefined) {
                    meshDefs[nodeDef.mesh].isSkinnedMesh = true;
                }
            }
        }
        this.json.meshReferences = meshReferences;
        this.json.meshUses = meshUses;
    }
    getMultiDependencies(types) {
        let results = {};
        let pending = [];
        for (let i = 0, il = types.length; i < il; i++) {
            let type = types[i];
            let promise = this.getDependencies(type);
            let value = promise.then((value) => {
                let key = type + (type === 'mesh' ? 'es' : 's');
                console.log("getDependencies...." + key + " | " + value);
                results[key] = value;
            });
            pending.push(value);
        }
        return Promise.all(pending).then(() => {
            console.log(" getMultiDependencies ... ok :" + Object.getOwnPropertyNames(results));
            return results;
        });
    }
    getDependencies(type) {
        let dependencies = this.cache.get(type);
        if (!dependencies) {
            let defs = this.json[type + (type === 'mesh' ? 'es' : 's')] || [];
            dependencies = Promise.all(defs.map((def, index) => {
                return this.getDependency(type, index);
            }));
            this.cache.add(type, dependencies);
        }
        return dependencies;
    }
    getDependency(type, index) {
        let cacheKey = type + ':' + index;
        let dependency = this.cache.get(cacheKey);
        if (!dependency) {
            switch (type) {
                case 'scene':
                    console.log("load scene....");
                    dependency = this.loadScene(index);
                    break;
                case 'node':
                    console.log("load node....");
                    dependency = this.loadNode(index);
                    break;
                case 'mesh':
                    console.log("load mesh....");
                    dependency = this.loadMesh(index);
                    break;
                case 'accessor':
                    console.log("load accessor....");
                    dependency = this.loadAccessor(index);
                    break;
                case 'bufferView':
                    console.log("load bufferView....");
                    dependency = this.loadBufferView(index);
                    break;
                case 'buffer':
                    console.log("load buffer....");
                    dependency = this.loadBuffer(index);
                    break;
                default:
                    throw new Error('Unknown type: ' + type);
            }
            this.cache.add(cacheKey, dependency);
        }
        return dependency;
    }
    resolveURL(url, path) {
        if (typeof url !== 'string' || url === '')
            return '';
        if (/^(https?:)?\/\//i.test(url))
            return url;
        if (/^data:.*,.*$/i.test(url))
            return url;
        if (/^blob:.*$/i.test(url))
            return url;
        return path + url;
    }
    loadBuffer(index) {
        var bufferDef = this.json.buffers[index];
        var loader = this.fileLoader;
        if (bufferDef.type && bufferDef.type !== 'arraybuffer') {
            throw new Error('THREE.GLTFLoader: ' + bufferDef.type + ' buffer type is not supported.');
        }
        if (bufferDef.uri === undefined && index === 0) {
            return Promise.resolve(this.extensions[EXTENSIONS.KHR_BINARY_GLTF].body);
        }
        var options = this.options;
        return new Promise((resolve, reject) => {
            loader.load(this.resolveURL(bufferDef.uri, options.path), (resp) => {
                console.log("loadbuffer done ");
                let arraybuffer = resp.arrayBuffer();
                arraybuffer.then((buffer) => {
                    console.log("loadbuffer ok");
                    resolve(buffer);
                }).catch((reson) => {
                    console.log("loadbuffer false:" + reson);
                });
            }, undefined, () => {
                reject(new Error('THREE.GLTFLoader: Failed to load buffer "' + bufferDef.uri + '".'));
            });
        });
    }
    loadBufferView(index) {
        var bufferViewDef = this.json.bufferViews[index];
        return this.getDependency('buffer', bufferViewDef.buffer).then((buffer) => {
            console.log("loadbufferview ok");
            var byteLength = bufferViewDef.byteLength || 0;
            var byteOffset = bufferViewDef.byteOffset || 0;
            return buffer.slice(byteOffset, byteOffset + byteLength);
        });
    }
    loadNode(nodeIndex) {
        var nodeDef = this.json.nodes[nodeIndex];
        var meshReferences = this.json.meshReferences;
        var meshUses = this.json.meshUses;
        let parseNode = () => {
            if (nodeDef.isBone === true) {
            }
            else if (nodeDef.mesh !== undefined) {
                return this.getDependency('mesh', nodeDef.mesh).then((mesh) => {
                    console.log("mesh load ok.....!");
                    var node;
                    if (meshReferences[nodeDef.mesh] > 1) {
                        var instanceNum = meshUses[nodeDef.mesh]++;
                        node = mesh.clone();
                    }
                    else {
                        node = mesh;
                    }
                    let ent = entity_1.Entity.createEntFromMesh(node);
                    return ent;
                });
            }
            else if (nodeDef.camera !== undefined) {
            }
            else if (nodeDef.extensions
                && nodeDef.extensions[EXTENSIONS.KHR_LIGHTS_PUNCTUAL]
                && nodeDef.extensions[EXTENSIONS.KHR_LIGHTS_PUNCTUAL].light !== undefined) {
            }
            return Promise.resolve(new scenenode_1.SceneNode());
        };
        return parseNode().then((node) => {
            console.log("Entity create ok .................");
            if (nodeDef.matrix !== undefined) {
                let matrix = matrix4_1.Matrix4.fromArray(nodeDef.matrix);
                node.applyMatrix(matrix);
            }
            else {
                if (nodeDef.translation !== undefined) {
                }
                if (nodeDef.rotation !== undefined) {
                    node.quaternion = vector4_1.Vector4.fromArray(nodeDef.rotation, 0);
                }
                if (nodeDef.scale !== undefined) {
                    node.scale = vector3_1.Vector3.fromArray(nodeDef.scale, 0);
                }
            }
            return node;
        });
    }
    loadMesh(meshIndex) {
        let meshDef = this.json.meshes[meshIndex];
        let primitives = meshDef.primitives;
        return this.loadGeometries(primitives).then((geometries) => {
            console.log("geometries load ok ,geometries count:" + geometries.length);
            let submeshes = [];
            for (let i = 0, il = geometries.length; i < il; i++) {
                let geometry = geometries[i];
                let primitive = primitives[i];
                let submesh;
                let material = undefined;
                switch (primitive.mode) {
                    case WEBGL_CONSTANTS.TRIANGLES:
                        submesh = new primitive_1.Primitive(geometry, material, drawobject_1.EDrawType.Triangles);
                        break;
                    case WEBGL_CONSTANTS.TRIANGLE_STRIP:
                        submesh = new primitive_1.Primitive(geometry, material, drawobject_1.EDrawType.Triangles_Strip);
                        break;
                    case WEBGL_CONSTANTS.TRIANGLE_FAN:
                        submesh = new primitive_1.Primitive(geometry, material, drawobject_1.EDrawType.Triangle_Fan);
                        break;
                    default:
                        throw new Error("GLTFLoader : primitive mode unsupported " + primitive.mode);
                }
                submesh.name = meshDef.name || ('mesh_' + meshIndex);
                submeshes.push(submesh);
            }
            let mesh = new mesh_1.Mesh();
            for (let i = 0, il = submeshes.length; i < il; i++) {
                mesh.addPrimitive(submeshes[i]);
            }
            return mesh;
        });
    }
    loadAccessor(index) {
        var accessorDef = this.json.accessors[index];
        if (accessorDef.bufferView === undefined && accessorDef.sparse === undefined) {
            return Promise.resolve(null);
        }
        var pendingBufferViews = [];
        if (accessorDef.bufferView !== undefined) {
            pendingBufferViews.push(this.getDependency('bufferView', accessorDef.bufferView));
        }
        else {
            pendingBufferViews.push(null);
        }
        if (accessorDef.sparse !== undefined) {
            pendingBufferViews.push(this.getDependency('bufferView', accessorDef.sparse.indices.bufferView));
            pendingBufferViews.push(this.getDependency('bufferView', accessorDef.sparse.values.bufferView));
        }
        return Promise.all(pendingBufferViews).then((bufferViews) => {
            console.log("load all buffer views ok....");
            let bufferView = bufferViews[0];
            let itemSize = WEBGL_TYPE_SIZES[accessorDef.type];
            let TypedArray = WEBGL_COMPONENT_TYPES[accessorDef.componentType];
            let elementBytes = TypedArray.BYTES_PER_ELEMENT;
            let itemBytes = elementBytes * itemSize;
            let byteOffset = accessorDef.byteOffset || 0;
            let byteStride = accessorDef.bufferView !== undefined ? this.json.bufferViews[accessorDef.bufferView].byteStride : undefined;
            let normalized = accessorDef.normalized === true;
            let array;
            let bufferAttribute;
            if (byteStride && byteStride !== itemBytes) {
            }
            else {
                if (bufferView === null) {
                    array = new TypedArray(accessorDef.count * itemSize);
                }
                else {
                    array = new TypedArray(bufferView, byteOffset, accessorDef.count * itemSize);
                }
                bufferAttribute = new geometry_1.AttributeInfo("", {
                    buffer: array,
                    numComponents: itemSize,
                    sizeofStride: itemBytes,
                    offsetofStride: 0,
                    normalized: normalized,
                });
            }
            if (accessorDef.sparse !== undefined) {
            }
            return bufferAttribute;
        });
    }
    loadGeometries(primitives) {
        console.log("load geometries");
        let cache = this.primitiveCache;
        let isMultiPass = this.isMultiPassGeometry(primitives);
        let originalPrimitives;
        if (isMultiPass) {
            originalPrimitives = primitives;
            primitives = [primitives[0]];
        }
        let createDracoPrimitive = (primitive) => {
            return this.extensions[EXTENSIONS.KHR_DRACO_MESH_COMPRESSION]
                .decodePrimitive(primitive, this)
                .then((geometry) => {
                return this.addPrimitiveAttributes(geometry, primitive);
            });
        };
        let pending = [];
        for (var i = 0, il = primitives.length; i < il; i++) {
            var primitive = primitives[i];
            var cacheKey = this.createPrimitiveKey(primitive);
            var cached = cache[cacheKey];
            if (cached) {
                pending.push(cached.promise);
            }
            else {
                var geometryPromise;
                if (primitive.extensions && primitive.extensions[EXTENSIONS.KHR_DRACO_MESH_COMPRESSION]) {
                    console.log("create draco geometry");
                    geometryPromise = createDracoPrimitive(primitive);
                }
                else {
                    console.log("create new geometry");
                    geometryPromise = this.addPrimitiveAttributes(new geometry_1.Geometry(), primitive);
                }
                cache[cacheKey] = { primitive: primitive, promise: geometryPromise };
                pending.push(geometryPromise);
            }
        }
        return Promise.all(pending).then((geometries) => {
            console.log("all geometry load ok..........");
            return geometries;
        });
    }
    addPrimitiveAttributes(geometry, primitiveDef) {
        let attributes = primitiveDef.attributes;
        var pending = [];
        let assignAttributeAccessor = (accessorIndex, attributeName) => {
            return this.getDependency('accessor', accessorIndex)
                .then((accessor) => {
                console.log("load accessor ok:" + attributeName + " | " + accessor.buffer.length);
                accessor.name = attributeName;
                geometry.setVertexAttribute(attributeName, accessor);
                if (primitiveDef.indices) {
                    if (attributeName === 'indices') {
                        geometry.drawCount = accessor.buffer.length / accessor.numComponents;
                        geometry.drawOffset = 0;
                    }
                }
                else {
                    if (attributeName === 'position') {
                        geometry.drawCount = accessor.buffer.length / accessor.numComponents;
                        geometry.drawOffset = 0;
                    }
                }
            });
        };
        for (var gltfAttributeName in attributes) {
            var threeAttributeName = ATTRIBUTES[gltfAttributeName];
            if (!threeAttributeName)
                continue;
            if (threeAttributeName in geometry.vertexInfo)
                continue;
            console.log("setup attribute:" + gltfAttributeName);
            pending.push(assignAttributeAccessor(attributes[gltfAttributeName], threeAttributeName));
        }
        if (primitiveDef.indices !== undefined) {
            console.log("setup attribute indices");
            pending.push(assignAttributeAccessor(primitiveDef.indices, 'indices'));
        }
        return Promise.all(pending).then(() => {
            console.log("geometry attribute load ok..........");
            return geometry;
        });
    }
    isMultiPassGeometry(primitives) {
        if (primitives.length < 2)
            return false;
        var primitive0 = primitives[0];
        var targets0 = primitive0.targets || [];
        if (primitive0.indices === undefined)
            return false;
        for (var i = 1, il = primitives.length; i < il; i++) {
            var primitive = primitives[i];
            if (primitive0.mode !== primitive.mode)
                return false;
            if (primitive.indices === undefined)
                return false;
            if (primitive.extensions && primitive.extensions[EXTENSIONS.KHR_DRACO_MESH_COMPRESSION])
                return false;
            if (!this.isObjectEqual(primitive0.attributes, primitive.attributes))
                return false;
            var targets = primitive.targets || [];
            if (targets0.length !== targets.length)
                return false;
            for (var j = 0, jl = targets0.length; j < jl; j++) {
                if (!this.isObjectEqual(targets0[j], targets[j]))
                    return false;
            }
        }
        return true;
    }
    isObjectEqual(a, b) {
        if (Object.keys(a).length !== Object.keys(b).length)
            return false;
        for (var key in a) {
            if (a[key] !== b[key])
                return false;
        }
        return true;
    }
    createPrimitiveKey(primitiveDef) {
        var dracoExtension = primitiveDef.extensions && primitiveDef.extensions[EXTENSIONS.KHR_DRACO_MESH_COMPRESSION];
        var geometryKey;
        if (dracoExtension) {
            geometryKey = 'draco:' + dracoExtension.bufferView
                + ':' + dracoExtension.indices
                + ':' + this.createAttributesKey(dracoExtension.attributes);
        }
        else {
            geometryKey = primitiveDef.indices + ':' + this.createAttributesKey(primitiveDef.attributes) + ':' + primitiveDef.mode;
        }
        return geometryKey;
    }
    createAttributesKey(attributes) {
        var attributesKey = '';
        var keys = Object.keys(attributes).sort();
        for (var i = 0, il = keys.length; i < il; i++) {
            attributesKey += keys[i] + ':' + attributes[keys[i]] + ';';
        }
        return attributesKey;
    }
    loadScene(sceneIndex) {
        let buildNodeHierachy = (nodeId, parentObject, json, parser) => {
            var nodeDef = json.nodes[nodeId];
            return this.getDependency('node', nodeId).then((node) => {
                console.log("node load ok..............:" + nodeId);
                parentObject.addNode(node);
                var pending = [];
                if (nodeDef.children) {
                    var children = nodeDef.children;
                    for (var i = 0, il = children.length; i < il; i++) {
                        var child = children[i];
                        pending.push(buildNodeHierachy(child, node, json, parser));
                    }
                }
                return Promise.all(pending);
            });
        };
        var json = this.json;
        var extensions = this.extensions;
        var sceneDef = this.json.scenes[sceneIndex];
        var parser = this;
        var scene = new scene_1.Scene();
        if (sceneDef.name !== undefined)
            scene.name = sceneDef.name;
        var nodeIds = sceneDef.nodes || [];
        var pending = [];
        for (var i = 0, il = nodeIds.length; i < il; i++) {
            pending.push(buildNodeHierachy(nodeIds[i], scene, json, parser));
        }
        return Promise.all(pending).then(function () {
            console.log("scene load ok..........");
            return scene;
        });
    }
}


/***/ }),

/***/ "./src/thirdpart/loaders/loaderutils.ts":
/*!**********************************************!*\
  !*** ./src/thirdpart/loaders/loaderutils.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.LoaderUtils = void 0;
class LoaderUtils {
    static decodeText(array) {
        if (typeof TextDecoder !== 'undefined') {
            return new TextDecoder().decode(array);
        }
        var s = '';
        for (var i = 0, il = array.length; i < il; i++) {
            s += String.fromCharCode(array[i]);
        }
        return decodeURIComponent(escape(s));
    }
    static extractUrlBase(url) {
        var index = url.lastIndexOf('/');
        if (index === -1)
            return './';
        return url.substr(0, index + 1);
    }
}
exports.LoaderUtils = LoaderUtils;


/***/ }),

/***/ "./src/thirdpart/loaders/loadingmanager.ts":
/*!*************************************************!*\
  !*** ./src/thirdpart/loaders/loadingmanager.ts ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadingManager = void 0;
class LoadingManager {
    constructor(onStart, onLoad, onProgress, onError) {
        this.onStart = onStart;
        this.onLoad = onLoad;
        this.onProgress = onProgress;
        this.onError = onError;
        this.itemsTotal = 0;
        this.itemsLoaded = 0;
        this.isLoading = false;
    }
    load(url, onLoad, onProgress, onError) { }
    itemStart(url) {
        this.itemsTotal++;
        if (this.isLoading === false) {
            if (this.onStart) {
                this.onStart(url, this.itemsLoaded, this.itemsTotal);
            }
        }
        this.isLoading = true;
    }
    itemEnd(url) {
        this.itemsLoaded++;
        if (this.onProgress) {
            this.onProgress(url, this.itemsLoaded, this.itemsTotal);
        }
        if (this.itemsLoaded === this.itemsTotal) {
            this.isLoading = false;
            if (this.onLoad) {
                this.onLoad();
            }
        }
    }
    itemError(url) {
        if (this.onError) {
            this.onError(url);
        }
    }
}
exports.LoadingManager = LoadingManager;


/***/ }),

/***/ "./src/thirdpart/loaders/mapcache.ts":
/*!*******************************************!*\
  !*** ./src/thirdpart/loaders/mapcache.ts ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.MapCache = void 0;
class MapCache {
    constructor() {
        this.objects = {};
    }
    get(key) {
        return this.objects[key];
    }
    add(key, object) {
        this.objects[key] = object;
    }
    remove(key) {
        delete this.objects[key];
    }
    clear() {
        this.objects = {};
    }
}
exports.MapCache = MapCache;


/***/ }),

/***/ "./src/utils/fpsstate.ts":
/*!*******************************!*\
  !*** ./src/utils/fpsstate.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Stats = void 0;
exports.Stats = function () {
    function h(a) { c.appendChild(a.dom); return a; }
    function k(a) {
        for (var d = 0; d < c.children.length; d++)
            c.children[d].style.display = d === a ? "block" : "none";
        l = a;
    }
    var l = 0, c = document.createElement("div");
    c.style.cssText = "position:fixed;top:0;left:0;cursor:pointer;opacity:0.9;z-index:10000";
    c.addEventListener("click", function (a) {
        a.preventDefault();
        k(++l % c.children.length);
    }, !1);
    var g = (performance || Date).now(), e = g, a = 0, r = h(new exports.Stats.Panel("FPS", "#0ff", "#002")), f = h(new exports.Stats.Panel("MS", "#0f0", "#020"));
    if (self.performance && self.performance.memory)
        var t = h(new exports.Stats.Panel("MB", "#f08", "#201"));
    k(0);
    return {
        REVISION: 16, dom: c, addPanel: h, showPanel: k,
        begin: function () {
            g = (performance || Date).now();
        },
        end: function () {
            a++;
            var c = (performance || Date).now();
            f.update(c - g, 200);
            if (c > e + 1E3 && (r.update(1E3 * a / (c - e), 100), e = c, a = 0, t)) {
                var d = performance.memory;
                t.update(d.usedJSHeapSize / 1048576, d.jsHeapSizeLimit / 1048576);
            }
            return c;
        },
        update: function () {
            g = this.end();
        },
        domElement: c,
        setMode: k
    };
};
exports.Stats.Panel = function (h, k, l) {
    var c = Infinity, g = 0, e = Math.round, a = e(window.devicePixelRatio || 1), r = 80 * a, f = 48 * a, t = 3 * a, u = 2 * a, d = 3 * a, m = 15 * a, n = 74 * a, p = 30 * a, q = document.createElement("canvas");
    q.width = r;
    q.height = f;
    q.style.cssText = "width:80px;height:48px";
    var b = q.getContext("2d");
    b.font = "bold " + 9 * a + "px Helvetica,Arial,sans-serif";
    b.textBaseline = "top";
    b.fillStyle = l;
    b.fillRect(0, 0, r, f);
    b.fillStyle = k;
    b.fillText(h, t, u);
    b.fillRect(d, m, n, p);
    b.fillStyle = l;
    b.globalAlpha = .9;
    b.fillRect(d, m, n, p);
    return {
        dom: q,
        update: (f, v) => {
            c = Math.min(c, f);
            g = Math.max(g, f);
            b.fillStyle = l;
            b.globalAlpha = 1;
            b.fillRect(0, 0, r, m);
            b.fillStyle = k;
            b.fillText(e(f) + " " + h + " (" + e(c) + "-" + e(g) + ")", t, u);
            b.drawImage(q, d + a, m, n - a, p, d, m, n - a, p);
            b.fillRect(d + n - a, m, a, p);
            b.fillStyle = l;
            b.globalAlpha = .9;
            b.fillRect(d + n - a, m, a, e((1 - f / v) * p));
        }
    };
};


/***/ }),

/***/ "./src/utils/helper.ts":
/*!*****************************!*\
  !*** ./src/utils/helper.ts ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Helper = void 0;
class Helper {
    static calculateTimestamp(callback, flag) {
        let start = performance.now();
        callback();
        let diffMillSeconds = performance.now() - start;
        flag = flag || "";
    }
}
exports.Helper = Helper;


/***/ })

/******/ });
//# sourceMappingURL=main.js.map