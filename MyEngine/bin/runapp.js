/**
 * 已弃用，此JS直接调用引擎中入口函数
 */


require.config({
    baseUrl:"./bin/",
    paths:{
        "main":"App"
    }
});

var gApp;

require(['main'], function (mainModule) {
    mainModule.main();
})
