/**
 * 此脚本是当引擎源码直接由Typescript编译为AMD方式JS模块时，js端的引用示例
 */
require.config({
    baseUrl:"./bin/",
    paths:{
        "mirror":"App"
    }
});

require([
    'mirror'
], function(mirror) {
    'use strict';
    /*global self,module*/
    if (typeof window !== 'undefined') {
        window.Mi = mirror.default;
    } else if (typeof self !== 'undefined') {
        self.Mi = mirror.default;
    } else if(typeof module !== 'undefined') {
        module.exports = mirror.default;
    } else {
        console.log('Unable to load Mi.');
    }

    main();

}, undefined, true);

let main=()=>{

    console.log("main run ...");
    
    window.Mi.GLContext.init('canvas');
    let gl = Mi.GLContext.gl;
    let render = new Mi.Renderer(gl);

    let vs = "./shaders/test1_v.glsl";
    let fs = "./shaders/test1_f.glsl";
    let shaders = [vs,fs];

    //load resource
    Mi.ResourceManager.getSingleton().loadShaders(shaders).then( shadercache =>{
        let shaderMap = shadercache;
        console.log("===>" + JSON.stringify(shaderMap));

        let drawObj = new Mi.DrawableObj;
        drawObj.drawType = Mi.EDrawType.Triangles;
        //set program infomation
        drawObj.programInfo =  new Mi.ProgramInfo(shaderMap[vs],shaderMap[fs]);

        //set geometry infomation
        let vertices1 = new Float32Array([
            -0.5, -0.5,
            0, 0.5,
            0.5, -0.5
        ]);
        let geometry = new Mi.Geometry(gl);
        geometry.addAttribute("position",{
            buffer:vertices1,
            numComponents:2,
            sizeofStride:8,
            offsetofStride:0
        });
        drawObj.vertexInfo = geometry.vertexInfo;
        drawObj.drawArrayOffset = 0;
        drawObj.drawArrayCount = 3;

        render.addDrawAbleObj(drawObj,Mi.ERenderGroupType.Opaque);

        console.log("begin draw ..... " );
        //draw
        render.render();

        console.log("end draw ..... " );

    })
    
    console.log("main done!");
}

