define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * 所有资源在这里preload
     */
    class ResourceManager {
        constructor() {
            /**
             * 缓存shader资源
             */
            this.shaderCache = {};
        }
        static getSingleton() {
            if (!ResourceManager.singleton) {
                ResourceManager.singleton = new ResourceManager();
            }
            return ResourceManager.singleton;
        }
        /**
         * 异步加载所有shader资源
         * @param shaderFileList
         */
        loadShaders(shaderFileList) {
            return new Promise((resolve, reject) => {
                let handleCount = 0;
                let isfinished = () => {
                    if (handleCount >= shaderFileList.length) {
                        resolve(this.shaderCache);
                    }
                };
                shaderFileList.forEach(fileName => {
                    this._fetchResource(fileName).then(resp => {
                        if (resp.ok) {
                            resp.text().then(value => {
                                this.shaderCache[fileName] = value;
                                handleCount += 1;
                                isfinished();
                            });
                        }
                        else {
                            console.log(`load shader false ${fileName}`);
                            handleCount += 1;
                            isfinished();
                        }
                    }, reason => {
                        console.log(`load shader false ${fileName}: ${reason}`);
                        handleCount += 1;
                        isfinished();
                    }).catch(e => {
                        reject(e);
                    });
                });
            });
        }
        /**
         * fetch方式请求一个文本资源
         * @param resPath
         */
        _fetchResource(resPath) {
            return fetch(resPath, {
                method: 'get',
                headers: new Headers({
                    'Conten-Type': 'text/plain'
                }),
            });
        }
    }
    exports.default = ResourceManager;
});
