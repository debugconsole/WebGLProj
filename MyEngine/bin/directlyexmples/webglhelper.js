//--------------------------------------------------------------------
// 辅助方法：初始化着色器程序
function initProgram(gl, vsource, fsource) {
    let vShader = initShader(gl, gl.VERTEX_SHADER, vsource);
    let fShader = initShader(gl, gl.FRAGMENT_SHADER, fsource);
    // 创建WebGL程序
    let program = gl.createProgram();
    gl.attachShader(program, vShader);
    gl.attachShader(program, fShader);
    gl.linkProgram(program);
    // 判断是否创建成功
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        alert('unable to initialize!');
        return;
    }
    gl.useProgram(program);
    return program;
}

// 辅助方法：初始化着色器
function initShader(gl, type, source) {
    let shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert('error occured compiling the shaders:' + gl.getShaderInfoLog(shader));
        return null;
    }
    return shader;
}


//--------------------------------------------------------------------
// 是否一个对象有意义
function defined(value) {
    return value !== undefined && value !== null;
}

//--------------------------------------------------------------------
//辅助方法：requestAnimationFrame 与 渲染循环

var requestUpdateFrame = window.requestAnimationFrame;

(function() {

    // look for vendor prefixed function
    if (!defined(requestUpdateFrame)) {
        var vendors = ['webkit', 'moz', 'ms', 'o'];
        var i = 0;
        var len = vendors.length;
        while (i < len && !defined(requestUpdateFrame)) {
            requestUpdateFrame = window[vendors[i] + 'RequestAnimationFrame'];
            ++i;
        }
    }

    // build an requestUpdateFrame based on setTimeout
    if (!defined(requestUpdateFrame)) {
        var msPerFrame = 1000.0 / 60.0;
        var lastFrameTime = 0;
        requestUpdateFrame = function(callback) {
            var currentTime = getTimestamp();

            // schedule the callback to target 60fps, 16.7ms per frame,
            // accounting for the time taken by the callback
            var delay = Math.max(msPerFrame - (currentTime - lastFrameTime), 0);
            lastFrameTime = currentTime + delay;

            return setTimeout(function() {
                callback(lastFrameTime);
            }, delay);
        };
    }
})();
