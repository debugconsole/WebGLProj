"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
define("render/gldatatype", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    let gl;
    var GLDrawType;
    (function (GLDrawType) {
        GLDrawType[GLDrawType["STATIC_DRAW"] = 0] = "STATIC_DRAW";
        GLDrawType[GLDrawType["DYNAMIC_DRAW"] = 1] = "DYNAMIC_DRAW";
        GLDrawType[GLDrawType["STREAM_DRAW"] = 2] = "STREAM_DRAW";
    })(GLDrawType = exports.GLDrawType || (exports.GLDrawType = {}));
    var GLBingBufferTarget;
    (function (GLBingBufferTarget) {
        GLBingBufferTarget[GLBingBufferTarget["ARRAY_BUFFER"] = 0] = "ARRAY_BUFFER";
        GLBingBufferTarget[GLBingBufferTarget["ELEMENT_ARRAY_BUFFER"] = 1] = "ELEMENT_ARRAY_BUFFER";
    })(GLBingBufferTarget = exports.GLBingBufferTarget || (exports.GLBingBufferTarget = {}));
    var EGLDataType;
    (function (EGLDataType) {
        EGLDataType[EGLDataType["BYTE"] = 5120] = "BYTE";
        EGLDataType[EGLDataType["UNSIGNED_BYTE"] = 5121] = "UNSIGNED_BYTE";
        EGLDataType[EGLDataType["SHORT"] = 5122] = "SHORT";
        EGLDataType[EGLDataType["UNSIGNED_SHORT"] = 5123] = "UNSIGNED_SHORT";
        EGLDataType[EGLDataType["INT"] = 5124] = "INT";
        EGLDataType[EGLDataType["UNSIGNED_INT"] = 5125] = "UNSIGNED_INT";
        EGLDataType[EGLDataType["FLOAT"] = 5126] = "FLOAT";
        EGLDataType[EGLDataType["UNSIGNED_SHORT_4_4_4_4"] = 32819] = "UNSIGNED_SHORT_4_4_4_4";
        EGLDataType[EGLDataType["UNSIGNED_SHORT_5_5_5_1"] = 32820] = "UNSIGNED_SHORT_5_5_5_1";
        EGLDataType[EGLDataType["UNSIGNED_SHORT_5_6_5"] = 33635] = "UNSIGNED_SHORT_5_6_5";
        EGLDataType[EGLDataType["HALF_FLOAT"] = 5131] = "HALF_FLOAT";
        EGLDataType[EGLDataType["UNSIGNED_INT_2_10_10_10_REV"] = 33640] = "UNSIGNED_INT_2_10_10_10_REV";
        EGLDataType[EGLDataType["UNSIGNED_INT_10F_11F_11F_REV"] = 35899] = "UNSIGNED_INT_10F_11F_11F_REV";
        EGLDataType[EGLDataType["UNSIGNED_INT_5_9_9_9_REV"] = 35902] = "UNSIGNED_INT_5_9_9_9_REV";
        EGLDataType[EGLDataType["FLOAT_32_UNSIGNED_INT_24_8_REV"] = 36269] = "FLOAT_32_UNSIGNED_INT_24_8_REV";
        EGLDataType[EGLDataType["UNSIGNED_INT_24_8"] = 34042] = "UNSIGNED_INT_24_8";
        EGLDataType[EGLDataType["FLOAT_VEC2"] = 35664] = "FLOAT_VEC2";
        EGLDataType[EGLDataType["FLOAT_VEC3"] = 35665] = "FLOAT_VEC3";
        EGLDataType[EGLDataType["FLOAT_VEC4"] = 35666] = "FLOAT_VEC4";
        EGLDataType[EGLDataType["INT_VEC2"] = 35667] = "INT_VEC2";
        EGLDataType[EGLDataType["INT_VEC3"] = 35668] = "INT_VEC3";
        EGLDataType[EGLDataType["INT_VEC4"] = 35669] = "INT_VEC4";
        EGLDataType[EGLDataType["BOOL"] = 35670] = "BOOL";
        EGLDataType[EGLDataType["BOOL_VEC2"] = 35671] = "BOOL_VEC2";
        EGLDataType[EGLDataType["BOOL_VEC3"] = 35672] = "BOOL_VEC3";
        EGLDataType[EGLDataType["BOOL_VEC4"] = 35673] = "BOOL_VEC4";
        EGLDataType[EGLDataType["FLOAT_MAT2"] = 35674] = "FLOAT_MAT2";
        EGLDataType[EGLDataType["FLOAT_MAT3"] = 35675] = "FLOAT_MAT3";
        EGLDataType[EGLDataType["FLOAT_MAT4"] = 35676] = "FLOAT_MAT4";
        EGLDataType[EGLDataType["SAMPLER_2D"] = 35678] = "SAMPLER_2D";
        EGLDataType[EGLDataType["SAMPLER_CUBE"] = 35680] = "SAMPLER_CUBE";
        EGLDataType[EGLDataType["SAMPLER_3D"] = 35679] = "SAMPLER_3D";
        EGLDataType[EGLDataType["SAMPLER_2D_SHADOW"] = 35682] = "SAMPLER_2D_SHADOW";
        EGLDataType[EGLDataType["FLOAT_MAT2x3"] = 35685] = "FLOAT_MAT2x3";
        EGLDataType[EGLDataType["FLOAT_MAT2x4"] = 35686] = "FLOAT_MAT2x4";
        EGLDataType[EGLDataType["FLOAT_MAT3x2"] = 35687] = "FLOAT_MAT3x2";
        EGLDataType[EGLDataType["FLOAT_MAT3x4"] = 35688] = "FLOAT_MAT3x4";
        EGLDataType[EGLDataType["FLOAT_MAT4x2"] = 35689] = "FLOAT_MAT4x2";
        EGLDataType[EGLDataType["FLOAT_MAT4x3"] = 35690] = "FLOAT_MAT4x3";
        EGLDataType[EGLDataType["SAMPLER_2D_ARRAY"] = 36289] = "SAMPLER_2D_ARRAY";
        EGLDataType[EGLDataType["SAMPLER_2D_ARRAY_SHADOW"] = 36292] = "SAMPLER_2D_ARRAY_SHADOW";
        EGLDataType[EGLDataType["SAMPLER_CUBE_SHADOW"] = 36293] = "SAMPLER_CUBE_SHADOW";
        EGLDataType[EGLDataType["UNSIGNED_INT_VEC2"] = 36294] = "UNSIGNED_INT_VEC2";
        EGLDataType[EGLDataType["UNSIGNED_INT_VEC3"] = 36295] = "UNSIGNED_INT_VEC3";
        EGLDataType[EGLDataType["UNSIGNED_INT_VEC4"] = 36296] = "UNSIGNED_INT_VEC4";
        EGLDataType[EGLDataType["INT_SAMPLER_2D"] = 36298] = "INT_SAMPLER_2D";
        EGLDataType[EGLDataType["INT_SAMPLER_3D"] = 36299] = "INT_SAMPLER_3D";
        EGLDataType[EGLDataType["INT_SAMPLER_CUBE"] = 36300] = "INT_SAMPLER_CUBE";
        EGLDataType[EGLDataType["INT_SAMPLER_2D_ARRAY"] = 36303] = "INT_SAMPLER_2D_ARRAY";
        EGLDataType[EGLDataType["UNSIGNED_INT_SAMPLER_2D"] = 36306] = "UNSIGNED_INT_SAMPLER_2D";
        EGLDataType[EGLDataType["UNSIGNED_INT_SAMPLER_3D"] = 36307] = "UNSIGNED_INT_SAMPLER_3D";
        EGLDataType[EGLDataType["UNSIGNED_INT_SAMPLER_CUBE"] = 36308] = "UNSIGNED_INT_SAMPLER_CUBE";
        EGLDataType[EGLDataType["UNSIGNED_INT_SAMPLER_2D_ARRAY"] = 36311] = "UNSIGNED_INT_SAMPLER_2D_ARRAY";
        EGLDataType[EGLDataType["TEXTURE_2D"] = 3553] = "TEXTURE_2D";
        EGLDataType[EGLDataType["TEXTURE_CUBE_MAP"] = 34067] = "TEXTURE_CUBE_MAP";
        EGLDataType[EGLDataType["TEXTURE_3D"] = 32879] = "TEXTURE_3D";
        EGLDataType[EGLDataType["TEXTURE_2D_ARRAY"] = 35866] = "TEXTURE_2D_ARRAY";
    })(EGLDataType = exports.EGLDataType || (exports.EGLDataType = {}));
    class GLDataType {
        static getGLTypeForTypedArray(typedArray) {
            if (typedArray instanceof Int8Array) {
                return EGLDataType.BYTE;
            }
            if (typedArray instanceof Uint8Array) {
                return EGLDataType.UNSIGNED_BYTE;
            }
            if (typedArray instanceof Uint8ClampedArray) {
                return EGLDataType.UNSIGNED_BYTE;
            }
            if (typedArray instanceof Int16Array) {
                return EGLDataType.SHORT;
            }
            if (typedArray instanceof Uint16Array) {
                return EGLDataType.UNSIGNED_SHORT;
            }
            if (typedArray instanceof Int32Array) {
                return EGLDataType.INT;
            }
            if (typedArray instanceof Uint32Array) {
                return EGLDataType.UNSIGNED_INT;
            }
            if (typedArray instanceof Float32Array) {
                return EGLDataType.FLOAT;
            }
            throw new Error('unsupported typed array type');
        }
        static getGLTypeForTypedArrayType(typedArrayType) {
            if (typedArrayType === Int8Array) {
                return EGLDataType.BYTE;
            }
            if (typedArrayType === Uint8Array) {
                return EGLDataType.UNSIGNED_BYTE;
            }
            if (typedArrayType === Uint8ClampedArray) {
                return EGLDataType.UNSIGNED_BYTE;
            }
            if (typedArrayType === Int16Array) {
                return EGLDataType.SHORT;
            }
            if (typedArrayType === Uint16Array) {
                return EGLDataType.UNSIGNED_SHORT;
            }
            if (typedArrayType === Int32Array) {
                return EGLDataType.INT;
            }
            if (typedArrayType === Uint32Array) {
                return EGLDataType.UNSIGNED_INT;
            }
            if (typedArrayType === Float32Array) {
                return EGLDataType.FLOAT;
            }
            throw new Error('unsupported typed array type');
        }
    }
    exports.GLDataType = GLDataType;
});
define("render/vertexbuffer", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Geometry {
        constructor(gl) {
            this._vertexInfo = {};
            this._gl = gl;
        }
        addAttribute(name, options) {
            let attribute = {
                name: name,
                buffer: options.buffer,
                isConstantToShader: false,
                numComponents: options.numComponents,
                sizeofStride: options.sizeofStride,
                offsetofStride: options.offsetofStride,
                normalized: options.normalized || false,
            };
            if (!attribute.glBuffer) {
                if (!this._glbufferMap) {
                    this._glbufferMap = new WeakMap();
                }
                if (!this._glbufferMap.has(options.buffer)) {
                    attribute.glBuffer = this._gl.createBuffer();
                    this._gl.bindBuffer(this._gl.ARRAY_BUFFER, attribute.glBuffer);
                    this._gl.bufferData(this._gl.ARRAY_BUFFER, options.buffer, this._gl.STATIC_DRAW);
                    this._glbufferMap.set(options.buffer, attribute.glBuffer);
                    this._gl.bindBuffer(this._gl.ARRAY_BUFFER, null);
                }
                else {
                    attribute.glBuffer = this._glbufferMap.get(options.buffer);
                }
            }
            this._vertexInfo[name] = attribute;
        }
        get vertexInfo() {
            return this._vertexInfo;
        }
    }
    exports.Geometry = Geometry;
});
define("render/program", ["require", "exports", "render/gldatatype"], function (require, exports, gldatatype_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EShaderType;
    (function (EShaderType) {
        EShaderType[EShaderType["VERTEX_SHADER"] = 0] = "VERTEX_SHADER";
        EShaderType[EShaderType["FRAGMENT_SHADER"] = 1] = "FRAGMENT_SHADER";
    })(EShaderType || (EShaderType = {}));
    class ProgramInfo {
        constructor(vsSrc, fsSrc) {
            this.vertexShaderSource = vsSrc;
            this.fragmentShaderSource = fsSrc;
        }
    }
    exports.ProgramInfo = ProgramInfo;
    class Program {
        constructor(gl, id, name, program, vertexShader, fragmentShader) {
            this.usedTimes = 0;
            this.id = id;
            this.name = name;
            this.glProgram = program;
            this.glVertexShader = vertexShader;
            this.glFragmentShader = fragmentShader;
            this.uniforms = new Uniforms(gl, program);
            this.attributes = new Attributes(gl, program);
        }
        static createProgram(gl, programInfo) {
            let vertexShader = Program._createShader(gl, EShaderType.VERTEX_SHADER, programInfo.vertexShaderSource);
            let fragmentShader = Program._createShader(gl, EShaderType.FRAGMENT_SHADER, programInfo.fragmentShaderSource);
            let program = Program._createProgram(gl, [vertexShader, fragmentShader]);
            let ret = new Program(gl, "", "", program, vertexShader, fragmentShader);
            programInfo.program = ret;
            return ret;
        }
        static _createShader(gl, type, source) {
            let shader;
            if (type === EShaderType.VERTEX_SHADER) {
                shader = gl.createShader(gl.VERTEX_SHADER);
            }
            else if (type === EShaderType.FRAGMENT_SHADER) {
                shader = gl.createShader(gl.FRAGMENT_SHADER);
            }
            if (!shader) {
                console.error("Create shader false with invalid shader type ! ");
                return null;
            }
            gl.shaderSource(shader, source);
            gl.compileShader(shader);
            if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
                console.error('error occured compiling the shaders:' + gl.getShaderInfoLog(shader));
                gl.deleteShader(shader);
                return null;
            }
            return shader;
        }
        static _createProgram(gl, shaders) {
            const program = gl.createProgram();
            shaders.forEach((shader) => {
                gl.attachShader(program, shader);
            });
            gl.linkProgram(program);
            if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
                const lastError = gl.getProgramInfoLog(program);
                console.log("Error in program linking:" + lastError);
                gl.deleteProgram(program);
                return null;
            }
            return program;
        }
    }
    Program.programIDCount = 0;
    exports.Program = Program;
    class Setter {
    }
    class Setter1f extends Setter {
        setValue(gl, addr, value) {
            gl.uniform1f(addr, value);
        }
    }
    class Setter1fv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform1fv(addr, value);
        }
    }
    class Setter2fv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform2fv(addr, value);
        }
    }
    class Setter3fv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform3fv(addr, value);
        }
    }
    class Setter4fv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform4fv(addr, value);
        }
    }
    class Setter1i extends Setter {
        setValue(gl, addr, value) {
            gl.uniform1i(addr, value);
        }
    }
    class Setter1iv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform1iv(addr, value);
        }
    }
    class Setter2iv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform2iv(addr, value);
        }
    }
    class Setter3iv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform3iv(addr, value);
        }
    }
    class Setter4iv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform4iv(addr, value);
        }
    }
    class Setter1ui extends Setter {
        setValue(gl, addr, value) {
            gl.uniform1ui(addr, value);
        }
    }
    class Setter1uiv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform1uiv(addr, value);
        }
    }
    class Setter2uiv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform2uiv(addr, value);
        }
    }
    class Setter3uiv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform3uiv(addr, value);
        }
    }
    class Setter4uiv extends Setter {
        setValue(gl, addr, value) {
            gl.uniform4uiv(addr, value);
        }
    }
    class SetterMatrix2fv extends Setter {
        setValue(gl, addr, value) {
            gl.uniformMatrix2fv(addr, false, value);
        }
    }
    class SetterMatrix3fv extends Setter {
        setValue(gl, addr, value) {
            gl.uniformMatrix3fv(addr, false, value);
        }
    }
    class SetterMatrix4fv extends Setter {
        setValue(gl, addr, value) {
            gl.uniformMatrix4fv(addr, false, value);
        }
    }
    class SetterMatrix23f extends Setter {
        setValue(gl, addr, value) {
            gl.uniformMatrix2x3fv(addr, false, value);
        }
    }
    class SetterMatrix32f extends Setter {
        setValue(gl, addr, value) {
            gl.uniformMatrix3x2fv(addr, false, value);
        }
    }
    class SetterMatrix24f extends Setter {
        setValue(gl, addr, value) {
            gl.uniformMatrix3x2fv(addr, false, value);
        }
    }
    class SetterMatrix42f extends Setter {
        setValue(gl, addr, value) {
            gl.uniformMatrix4x2fv(addr, false, value);
        }
    }
    class SetterMatrix34f extends Setter {
        setValue(gl, addr, value) {
            gl.uniformMatrix3x4fv(addr, false, value);
        }
    }
    class SetterMatrix43f extends Setter {
        setValue(gl, addr, value) {
            gl.uniformMatrix4x3fv(addr, false, value);
        }
    }
    class Uniforms {
        constructor(gl, program) {
            this.map = {};
            this.seq = [];
            let n = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);
            for (let i = 0; i < n; ++i) {
                let info = gl.getActiveUniform(program, i);
                let addr = gl.getUniformLocation(program, info.name);
                this._parseUniform(info, addr);
                console.log("parse uniform:" + info.name);
            }
        }
        _parseUniform(info, addr) {
            var id = info.name | 0;
            this._addUniform(id, info.name, addr, info.type);
        }
        _addUniform(id, name, addr, uniformType) {
            let uniform = new Uniform(id, addr, Uniforms.typeMap[uniformType]);
            this.map[name] = uniform;
            this.seq.push(uniform);
        }
    }
    Uniforms.typeMap = {
        [gldatatype_1.EGLDataType.FLOAT]: new Setter1f(),
        [gldatatype_1.EGLDataType.FLOAT_VEC2]: new Setter2fv(),
        [gldatatype_1.EGLDataType.FLOAT_VEC3]: new Setter3fv(),
        [gldatatype_1.EGLDataType.FLOAT_VEC4]: new Setter4fv(),
        [gldatatype_1.EGLDataType.INT]: new Setter1i(),
        [gldatatype_1.EGLDataType.INT_VEC2]: new Setter2iv(),
        [gldatatype_1.EGLDataType.INT_VEC3]: new Setter3iv(),
        [gldatatype_1.EGLDataType.INT_VEC4]: new Setter4iv(),
        [gldatatype_1.EGLDataType.UNSIGNED_INT]: new Setter1ui(),
        [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC2]: new Setter2uiv(),
        [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC3]: new Setter3uiv(),
        [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC4]: new Setter4uiv(),
        [gldatatype_1.EGLDataType.BOOL]: new Setter1f(),
        [gldatatype_1.EGLDataType.BOOL_VEC2]: new Setter1f(),
        [gldatatype_1.EGLDataType.BOOL_VEC3]: new Setter1f(),
        [gldatatype_1.EGLDataType.BOOL_VEC4]: new Setter1f(),
        [gldatatype_1.EGLDataType.FLOAT_MAT2]: new Setter1f(),
        [gldatatype_1.EGLDataType.FLOAT_MAT3]: new Setter1f(),
        [gldatatype_1.EGLDataType.FLOAT_MAT4]: new Setter1f(),
        [gldatatype_1.EGLDataType.FLOAT_MAT2x3]: new Setter1f(),
        [gldatatype_1.EGLDataType.FLOAT_MAT2x4]: new Setter1f(),
        [gldatatype_1.EGLDataType.FLOAT_MAT3x2]: new Setter1f(),
        [gldatatype_1.EGLDataType.FLOAT_MAT3x4]: new Setter1f(),
        [gldatatype_1.EGLDataType.FLOAT_MAT4x2]: new Setter1f(),
        [gldatatype_1.EGLDataType.FLOAT_MAT4x3]: new Setter1f(),
    };
    class Uniform {
        constructor(id, addr, setter) {
            this.id = id;
            this.addr = addr;
            this.setter = setter;
        }
    }
    exports.Uniform = Uniform;
    class AttribFloatSetter extends Setter {
        setValue(gl, addr, value) {
            let attrib = value;
            if (attrib.isConstantToShader === true) {
                gl.disableVertexAttribArray(addr);
                let setValue = attrib.buffer;
                switch (setValue.length) {
                    case 4:
                        gl.vertexAttrib4fv(addr, setValue.buffer);
                        break;
                    case 3:
                        gl.vertexAttrib3fv(addr, setValue.buffer);
                        break;
                    case 2:
                        gl.vertexAttrib2fv(addr, setValue.buffer);
                        break;
                    case 1:
                        gl.vertexAttrib1fv(addr, setValue.buffer);
                        break;
                    default:
                        throw new Error('the length of a float constant value must be between 1 and 4!');
                }
            }
            else {
                if (!attrib.glBuffer) {
                    attrib.glBuffer = gl.createBuffer();
                    gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                    gl.bufferData(gl.ARRAY_BUFFER, attrib.buffer, gl.STATIC_DRAW);
                }
                else {
                    gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                }
                gl.enableVertexAttribArray(addr);
                gl.vertexAttribPointer(addr, attrib.numComponents, gl.FLOAT, attrib.normalized || false, attrib.sizeofStride || 0, attrib.offsetofStride || 0);
                gl.bindBuffer(gl.ARRAY_BUFFER, null);
            }
        }
    }
    class AttribIntSetter extends Setter {
        setValue(gl, addr, value) {
            let attrib = value;
            if (attrib.isConstantToShader !== true) {
                if (!attrib.glBuffer) {
                    attrib.glBuffer = gl.createBuffer();
                    gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                    gl.bufferData(gl.ARRAY_BUFFER, attrib.buffer, gl.STATIC_DRAW);
                }
                else {
                    gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                }
                gl.enableVertexAttribArray(addr);
                gl.vertexAttribIPointer(addr, attrib.numComponents, attrib.glType || gl.INT, attrib.sizeofStride || 0, attrib.offsetofStride || 0);
                gl.bindBuffer(gl.ARRAY_BUFFER, null);
            }
        }
    }
    class AttribUIntSetter extends Setter {
        setValue(gl, addr, value) {
            let attrib = value;
            if (attrib.isConstantToShader !== true) {
                if (!attrib.glBuffer) {
                    attrib.glBuffer = gl.createBuffer();
                    gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                    gl.bufferData(gl.ARRAY_BUFFER, attrib.buffer, gl.STATIC_DRAW);
                }
                else {
                    gl.bindBuffer(gl.ARRAY_BUFFER, attrib.glBuffer);
                }
                gl.enableVertexAttribArray(addr);
                gl.vertexAttribIPointer(addr, attrib.numComponents, attrib.glType || gl.UNSIGNED_INT, attrib.sizeofStride || 0, attrib.offsetofStride || 0);
                gl.bindBuffer(gl.ARRAY_BUFFER, null);
            }
        }
    }
    class Attribute {
        constructor(id, addr, type) {
            this.id = id;
            this.addr = addr;
            this.setter = Attributes.typeMap[type];
            this.type = type;
        }
    }
    exports.Attribute = Attribute;
    class Attributes {
        constructor(gl, program) {
            this.map = {};
            this.seq = [];
            const numAttribs = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);
            for (let i = 0; i < numAttribs; ++i) {
                const attribInfo = gl.getActiveAttrib(program, i);
                if (this._isBuiltIn(attribInfo)) {
                    continue;
                }
                const index = gl.getAttribLocation(program, attribInfo.name);
                const id = attribInfo.name | 0;
                const attribute = new Attribute(id, index, attribInfo.type);
                this.map[attribInfo.name] = attribute;
                this.seq.push(attribute);
                console.log("parse attribute:" + attribInfo.name + "|" + id);
            }
        }
        _isBuiltIn(info) {
            const name = info.name;
            return name.startsWith("gl_") || name.startsWith("webgl_");
        }
    }
    Attributes.typeMap = {
        [gldatatype_1.EGLDataType.FLOAT]: new AttribFloatSetter(),
        [gldatatype_1.EGLDataType.FLOAT_VEC2]: new AttribFloatSetter(),
        [gldatatype_1.EGLDataType.FLOAT_VEC3]: new AttribFloatSetter(),
        [gldatatype_1.EGLDataType.FLOAT_VEC4]: new AttribFloatSetter(),
        [gldatatype_1.EGLDataType.INT]: new AttribIntSetter(),
        [gldatatype_1.EGLDataType.INT_VEC2]: new AttribIntSetter(),
        [gldatatype_1.EGLDataType.INT_VEC3]: new AttribIntSetter(),
        [gldatatype_1.EGLDataType.INT_VEC4]: new AttribIntSetter(),
        [gldatatype_1.EGLDataType.UNSIGNED_INT]: new AttribUIntSetter(),
        [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC2]: new AttribUIntSetter(),
        [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC3]: new AttribUIntSetter(),
        [gldatatype_1.EGLDataType.UNSIGNED_INT_VEC4]: new AttribUIntSetter(),
        [gldatatype_1.EGLDataType.BOOL]: new AttribIntSetter(),
        [gldatatype_1.EGLDataType.BOOL_VEC2]: new AttribIntSetter(),
        [gldatatype_1.EGLDataType.BOOL_VEC3]: new AttribIntSetter(),
        [gldatatype_1.EGLDataType.BOOL_VEC4]: new AttribIntSetter(),
    };
    exports.Attributes = Attributes;
});
define("render/drawobject", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EDrawType;
    (function (EDrawType) {
        EDrawType[EDrawType["Points"] = 0] = "Points";
        EDrawType[EDrawType["Lines"] = 1] = "Lines";
        EDrawType[EDrawType["Triangles"] = 2] = "Triangles";
    })(EDrawType = exports.EDrawType || (exports.EDrawType = {}));
    class DrawableObj {
    }
    exports.DrawableObj = DrawableObj;
    class RenderList {
        constructor() {
            this.renderObjs = [];
        }
    }
    exports.RenderList = RenderList;
    class RenderGroup {
        constructor(id, priority) {
            this.id = id;
            this.priority = priority;
            this.renderListArray = [];
        }
    }
    exports.RenderGroup = RenderGroup;
});
define("render/glcontext", ["require", "exports", "render/gldatatype"], function (require, exports, gldatatype_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class GLContext {
        static init(cavID) {
            const canvas = document.querySelector(`#${cavID}`);
            GLContext.gl = canvas.getContext('webgl');
        }
        static glBindBuffer(gl, glTarget, glBuffer, typedArray, drawtype) {
            let bufferType = (glTarget === gldatatype_2.GLBingBufferTarget.ARRAY_BUFFER) ? gl.ARRAY_BUFFER : gl.ELEMENT_ARRAY_BUFFER;
            gl.bindBuffer(bufferType, glBuffer);
            gl.bufferData(bufferType, typedArray, drawtype);
        }
        static createBufferFromTypedArray(gl, type, typedArray, drawtype) {
            const glBuffer = gl.createBuffer();
            let glDrawType = gl.STATIC_DRAW;
            if (drawtype === gldatatype_2.GLDrawType.DYNAMIC_DRAW) {
                glDrawType = gl.DYNAMIC_DRAW;
            }
            if (drawtype === gldatatype_2.GLDrawType.STREAM_DRAW) {
                glDrawType = gl.STREAM_DRAW;
            }
            GLContext.glBindBuffer(gl, type, glBuffer, typedArray, glDrawType);
        }
        static createWebGLBuffersFromVertexInfo(gl, vertexInfo) {
            let vertexStruct = vertexInfo;
            Object.keys(vertexStruct).forEach((component) => {
                const type = component === "indices" ? gl.ELEMENT_ARRAY_BUFFER : gl.ARRAY_BUFFER;
                GLContext.createBufferFromTypedArray(gl, type, vertexStruct[component]);
            });
        }
    }
    exports.GLContext = GLContext;
});
define("render/renderer", ["require", "exports", "render/drawobject", "render/program"], function (require, exports, drawobject_1, program_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ERenderGroupType;
    (function (ERenderGroupType) {
        ERenderGroupType[ERenderGroupType["BackGround"] = 0] = "BackGround";
        ERenderGroupType[ERenderGroupType["Opaque"] = 1] = "Opaque";
        ERenderGroupType[ERenderGroupType["Transparence"] = 2] = "Transparence";
        ERenderGroupType[ERenderGroupType["UI_Layout"] = 3] = "UI_Layout";
    })(ERenderGroupType = exports.ERenderGroupType || (exports.ERenderGroupType = {}));
    class Renderer {
        constructor(gl) {
            this.gl = gl;
            this._setupRenderGroups();
        }
        _setupRenderGroups() {
            this.renderGroups = {};
            this.renderGroups[ERenderGroupType.BackGround] = new drawobject_1.RenderGroup(ERenderGroupType.BackGround, 500);
            this.renderGroups[ERenderGroupType.Opaque] = new drawobject_1.RenderGroup(ERenderGroupType.Opaque, 400);
            this.renderGroups[ERenderGroupType.Transparence] = new drawobject_1.RenderGroup(ERenderGroupType.Transparence, 300);
            this.renderGroups[ERenderGroupType.UI_Layout] = new drawobject_1.RenderGroup(ERenderGroupType.UI_Layout, 100);
        }
        render() {
            for (const key in this.renderGroups) {
                if (this.renderGroups.hasOwnProperty(key)) {
                    const renderGroup = this.renderGroups[key];
                    renderGroup.renderListArray.forEach(renderList => {
                        this._drawObjects(renderList);
                    });
                }
            }
        }
        _drawObject(drawObj) {
            if (this.lastUsedProgramInfo !== drawObj.programInfo) {
                if (!drawObj.programInfo.program) {
                    console.log("create program");
                    program_1.Program.createProgram(this.gl, drawObj.programInfo);
                }
                this.lastUsedProgramInfo = drawObj.programInfo;
                this.gl.useProgram(drawObj.programInfo.program.glProgram);
            }
            let vertexInfo = drawObj.vertexInfo;
            let program = drawObj.programInfo.program;
            for (const key in program.attributes.map) {
                if (program.attributes.map.hasOwnProperty(key)) {
                    if (vertexInfo.hasOwnProperty(key)) {
                        console.log("set attribute :" + key);
                        const attribInfo = vertexInfo[key];
                        const attribute = program.attributes.map[key];
                        attribute.setter.setValue(this.gl, attribute.addr, attribInfo);
                    }
                }
            }
            let uniforms = drawObj.uniforms;
            for (const key in program.uniforms.map) {
                if (program.uniforms.map.hasOwnProperty(key)) {
                    if (uniforms.hasOwnProperty(key)) {
                        console.log("set uniform :" + key);
                        const uniformValue = uniforms[key];
                        const uniform = program.uniforms.map[key];
                        uniform.setter.setValue(this.gl, uniform.addr, uniformValue);
                    }
                }
            }
            this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
            this.gl.clear(this.gl.COLOR_BUFFER_BIT);
            let glDrawType = this.gl.TRIANGLES;
            if (drawObj.drawType === drawobject_1.EDrawType.Points) {
                glDrawType = this.gl.POINTS;
            }
            else if (drawObj.drawType === drawobject_1.EDrawType.Lines) {
                glDrawType = this.gl.LINES;
            }
            if (!drawObj.vertexInfo.indices) {
                console.log("drawArrays:" + drawObj.drawType + "|" + drawObj.drawArrayOffset + "|" + drawObj.drawArrayCount);
                this.gl.drawArrays(glDrawType, drawObj.drawArrayOffset, drawObj.drawArrayCount);
            }
            else {
                this.gl.drawElements(glDrawType, drawObj.drawArrayOffset, this.gl.UNSIGNED_SHORT, drawObj.drawArrayCount);
            }
        }
        _drawObjects(renderList) {
            renderList.renderObjs.forEach(renderObj => {
                this._drawObject(renderObj);
            });
        }
        addDrawAbleObj(drawObj, renderType) {
            if (this.renderGroups.hasOwnProperty(renderType)) {
                const renderGroup = this.renderGroups[renderType];
                if (renderGroup.renderListArray.length === 0) {
                    renderGroup.renderListArray.push(new drawobject_1.RenderList());
                }
                renderGroup.renderListArray[0].renderObjs.push(drawObj);
            }
        }
    }
    exports.Renderer = Renderer;
});
define("core/resourcemgr", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class ResourceManager {
        constructor() {
            this.shaderCache = {};
        }
        static getSingleton() {
            if (!ResourceManager.singleton) {
                ResourceManager.singleton = new ResourceManager();
            }
            return ResourceManager.singleton;
        }
        loadShaders(shaderFileList) {
            return new Promise((resolve, reject) => {
                let handleCount = 0;
                let isfinished = () => {
                    if (handleCount >= shaderFileList.length) {
                        resolve(this.shaderCache);
                    }
                };
                shaderFileList.forEach(fileName => {
                    this._fetchResource(fileName).then(resp => {
                        if (resp.ok) {
                            resp.text().then(value => {
                                this.shaderCache[fileName] = value;
                                handleCount += 1;
                                isfinished();
                            });
                        }
                        else {
                            console.log(`load shader false ${fileName}`);
                            handleCount += 1;
                            isfinished();
                        }
                    }, reason => {
                        console.log(`load shader false ${fileName}: ${reason}`);
                        handleCount += 1;
                        isfinished();
                    }).catch(e => {
                        reject(e);
                    });
                });
            });
        }
        _fetchResource(resPath) {
            return fetch(resPath, {
                method: 'get',
                headers: new Headers({
                    'Conten-Type': 'text/plain'
                }),
            });
        }
    }
    exports.ResourceManager = ResourceManager;
});
define("mirror", ["require", "exports", "render/drawobject", "render/vertexbuffer", "render/glcontext", "render/program", "render/renderer", "core/resourcemgr"], function (require, exports, drawobject_2, vertexbuffer_1, glcontext_1, program_2, renderer_1, resourcemgr_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    let Mirror = {
        ['DrawableObj']: drawobject_2.DrawableObj,
        ['EDrawType']: drawobject_2.EDrawType,
        ['RenderList']: drawobject_2.RenderList,
        ['RenderGroup']: drawobject_2.RenderGroup,
        ['Geometry']: vertexbuffer_1.Geometry,
        ['GLContext']: glcontext_1.GLContext,
        ['ProgramInfo']: program_2.ProgramInfo,
        ['Renderer']: renderer_1.Renderer,
        ['ERenderGroupType']: renderer_1.ERenderGroupType,
        ['ResourceManager']: resourcemgr_1.ResourceManager,
    };
    exports.default = Mirror;
});
define("core/webglinstaller", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class WebGLInstaller {
        constructor() {
            this.glshader = null;
            this.gl = null;
            this.program = null;
            var canvas = document.getElementById('canvas');
            if (!canvas) {
                console.error("can't get canvas.");
                return;
            }
            console.log("111111111111");
            this.gl = canvas.getContext("webgl");
            if (!this.gl) {
                console.log("no webgl2");
                this.gl = canvas.getContext("webgl");
                if (!this.gl) {
                    console.error("no support webgl.");
                    return;
                }
            }
        }
        drawBuffer(vertexArray) {
            let buffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffer);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, vertexArray, this.gl.STATIC_DRAW);
            var positionLocation = this.gl.getAttribLocation(this.program, "a_position");
            this.gl.enableVertexAttribArray(positionLocation);
            this.gl.vertexAttribPointer(positionLocation, 2, this.gl.FLOAT, false, 0, 0);
            this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
            this.gl.clear(this.gl.COLOR_BUFFER_BIT);
            this.gl.drawArrays(this.gl.POINTS, 0, 3);
        }
        setupProgramFromScript(vsShaderSource, fsShaderSource) {
            let vsShader = this._createShader(vsShaderSource, this.gl.VERTEX_SHADER);
            let fsShader = this._createShader(fsShaderSource, this.gl.FRAGMENT_SHADER);
            if (vsShader && fsShader) {
                this.program = this._createProgram(vsShader, fsShader);
                if (this.program) {
                    return true;
                }
            }
            return false;
        }
        _createShader(shaderSource, shaderType) {
            if (shaderType === this.gl.VERTEX_SHADER || shaderType === this.gl.FRAGMENT_SHADER) {
                this.glshader = this.gl.createShader(shaderType);
            }
            else {
                return;
            }
            this.gl.shaderSource(this.glshader, shaderSource);
            this.gl.compileShader(this.glshader);
            if (this.gl.getShaderParameter(this.glshader, this.gl.COMPILE_STATUS)) {
                console.log(`-->createAndCompileShaderSuccessful!!`);
                return this.glshader;
            }
            else {
                console.log(`-->CompileShaderFalse`);
                alert(this.gl.getShaderInfoLog(this.glshader));
            }
        }
        _createProgram(vs, fs) {
            let program = this.gl.createProgram();
            if (!program) {
                return;
            }
            this.gl.attachShader(program, vs);
            this.gl.attachShader(program, fs);
            this.gl.linkProgram(program);
            if (this.gl.getProgramParameter(program, this.gl.LINK_STATUS)) {
                this.gl.useProgram(program);
                return program;
            }
            else {
                alert(this.gl.getProgramInfoLog(program));
            }
        }
    }
    exports.default = WebGLInstaller;
});
define("test", ["require", "exports", "core/resourcemgr", "core/webglinstaller", "render/glcontext", "render/drawobject", "render/program", "render/renderer", "render/vertexbuffer"], function (require, exports, resourcemgr_2, webglinstaller_1, glcontext_2, drawobject_3, program_3, renderer_2, vertexbuffer_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    webglinstaller_1 = __importDefault(webglinstaller_1);
    exports.main = () => {
        console.log("main run ...");
        glcontext_2.GLContext.init('canvas');
        let gl = glcontext_2.GLContext.gl;
        let render = new renderer_2.Renderer(gl);
        let vs = "./shaders/test1_v.glsl";
        let fs = "./shaders/test1_f.glsl";
        let shaders = [vs, fs];
        resourcemgr_2.ResourceManager.getSingleton().loadShaders(shaders).then(shadercache => {
            let shaderMap = shadercache;
            console.log("===>" + JSON.stringify(shaderMap));
            let drawObj = new drawobject_3.DrawableObj;
            drawObj.drawType = drawobject_3.EDrawType.Triangles;
            drawObj.programInfo = new program_3.ProgramInfo(shaderMap[vs], shaderMap[fs]);
            let vertices1 = new Float32Array([
                -0.5, -0.5,
                0, 0.5,
                0.5, -0.5
            ]);
            let geometry = new vertexbuffer_2.Geometry(gl);
            geometry.addAttribute("position", {
                buffer: vertices1,
                numComponents: 2,
                sizeofStride: 8,
                offsetofStride: 0
            });
            drawObj.vertexInfo = geometry.vertexInfo;
            drawObj.drawArrayOffset = 0;
            drawObj.drawArrayCount = 3;
            render.addDrawAbleObj(drawObj, renderer_2.ERenderGroupType.Opaque);
            console.log("begin draw ..... ");
            render.render();
            console.log("end draw ..... ");
        });
        console.log("main done!");
    };
    let drawPointsSample = () => {
        glcontext_2.GLContext.init('canvas');
    };
    let testWebglConstant = () => {
        let webgl = new webglinstaller_1.default();
        let vs = "./shaders/test1_v.glsl";
        let fs = "./shaders/test1_f.glsl";
        let shaders = [vs, fs];
        resourcemgr_2.ResourceManager.getSingleton().loadShaders(shaders).then(shadercache => {
            let shaderMap = shadercache;
            console.log("===>" + JSON.stringify(shaderMap));
            if (webgl.setupProgramFromScript(shaderMap[vs], shaderMap[fs])) {
                let vertices1 = new Float32Array([
                    -0.5, -0.5,
                    0, 0.5,
                    0.5, -0.5
                ]);
                console.log("draw1 .... ");
                webgl.drawBuffer(vertices1);
                console.log("draw over .... ");
            }
        });
        console.log("main done!");
    };
});
define("core/scene", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Scene {
        constructor() {
        }
        render() {
        }
    }
    exports.default = Scene;
});
define("render/shaders/buildins/prefix_vertex.glsl", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = `
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;
uniform vec3 cameraPosition;

attribute vec3 position;
attribute vec3 normal;
attribute vec2 uv;

#ifdef USE_TANGENT

	attribute vec4 tangent;

#endif

#ifdef USE_COLOR

	attribute vec3 color;
#endif`;
});
define("render/shaders/templates/default_fragment.glsl", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = `
void main() {
	gl_FragColor = vec4( 1.0, 0.0, 0.0, 1.0 );
}
`;
});
define("render/shaders/templates/default_vertex.glsl", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = `
void main() {
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}
`;
});
var Matrix4 = function (opt_src) {
    var i, s, d;
    if (opt_src && typeof opt_src === 'object' && opt_src.hasOwnProperty('elements')) {
        s = opt_src.elements;
        d = new Float32Array(16);
        for (i = 0; i < 16; ++i) {
            d[i] = s[i];
        }
        this.elements = d;
    }
    else {
        this.elements = new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);
    }
};
Matrix4.prototype.setIdentity = function () {
    var e = this.elements;
    e[0] = 1;
    e[4] = 0;
    e[8] = 0;
    e[12] = 0;
    e[1] = 0;
    e[5] = 1;
    e[9] = 0;
    e[13] = 0;
    e[2] = 0;
    e[6] = 0;
    e[10] = 1;
    e[14] = 0;
    e[3] = 0;
    e[7] = 0;
    e[11] = 0;
    e[15] = 1;
    return this;
};
Matrix4.prototype.set = function (src) {
    var i, s, d;
    s = src.elements;
    d = this.elements;
    if (s === d) {
        return;
    }
    for (i = 0; i < 16; ++i) {
        d[i] = s[i];
    }
    return this;
};
Matrix4.prototype.concat = function (other) {
    var i, e, a, b, ai0, ai1, ai2, ai3;
    e = this.elements;
    a = this.elements;
    b = other.elements;
    if (e === b) {
        b = new Float32Array(16);
        for (i = 0; i < 16; ++i) {
            b[i] = e[i];
        }
    }
    for (i = 0; i < 4; i++) {
        ai0 = a[i];
        ai1 = a[i + 4];
        ai2 = a[i + 8];
        ai3 = a[i + 12];
        e[i] = ai0 * b[0] + ai1 * b[1] + ai2 * b[2] + ai3 * b[3];
        e[i + 4] = ai0 * b[4] + ai1 * b[5] + ai2 * b[6] + ai3 * b[7];
        e[i + 8] = ai0 * b[8] + ai1 * b[9] + ai2 * b[10] + ai3 * b[11];
        e[i + 12] = ai0 * b[12] + ai1 * b[13] + ai2 * b[14] + ai3 * b[15];
    }
    return this;
};
Matrix4.prototype.multiply = Matrix4.prototype.concat;
Matrix4.prototype.multiplyVector3 = function (pos) {
    var e = this.elements;
    var p = pos.elements;
    var v = new Vector3();
    var result = v.elements;
    result[0] = p[0] * e[0] + p[1] * e[4] + p[2] * e[8] + e[11];
    result[1] = p[0] * e[1] + p[1] * e[5] + p[2] * e[9] + e[12];
    result[2] = p[0] * e[2] + p[1] * e[6] + p[2] * e[10] + e[13];
    return v;
};
Matrix4.prototype.multiplyVector4 = function (pos) {
    var e = this.elements;
    var p = pos.elements;
    var v = new Vector4();
    var result = v.elements;
    result[0] = p[0] * e[0] + p[1] * e[4] + p[2] * e[8] + p[3] * e[12];
    result[1] = p[0] * e[1] + p[1] * e[5] + p[2] * e[9] + p[3] * e[13];
    result[2] = p[0] * e[2] + p[1] * e[6] + p[2] * e[10] + p[3] * e[14];
    result[3] = p[0] * e[3] + p[1] * e[7] + p[2] * e[11] + p[3] * e[15];
    return v;
};
Matrix4.prototype.transpose = function () {
    var e, t;
    e = this.elements;
    t = e[1];
    e[1] = e[4];
    e[4] = t;
    t = e[2];
    e[2] = e[8];
    e[8] = t;
    t = e[3];
    e[3] = e[12];
    e[12] = t;
    t = e[6];
    e[6] = e[9];
    e[9] = t;
    t = e[7];
    e[7] = e[13];
    e[13] = t;
    t = e[11];
    e[11] = e[14];
    e[14] = t;
    return this;
};
Matrix4.prototype.setInverseOf = function (other) {
    var i, s, d, inv, det;
    s = other.elements;
    d = this.elements;
    inv = new Float32Array(16);
    inv[0] = s[5] * s[10] * s[15] - s[5] * s[11] * s[14] - s[9] * s[6] * s[15]
        + s[9] * s[7] * s[14] + s[13] * s[6] * s[11] - s[13] * s[7] * s[10];
    inv[4] = -s[4] * s[10] * s[15] + s[4] * s[11] * s[14] + s[8] * s[6] * s[15]
        - s[8] * s[7] * s[14] - s[12] * s[6] * s[11] + s[12] * s[7] * s[10];
    inv[8] = s[4] * s[9] * s[15] - s[4] * s[11] * s[13] - s[8] * s[5] * s[15]
        + s[8] * s[7] * s[13] + s[12] * s[5] * s[11] - s[12] * s[7] * s[9];
    inv[12] = -s[4] * s[9] * s[14] + s[4] * s[10] * s[13] + s[8] * s[5] * s[14]
        - s[8] * s[6] * s[13] - s[12] * s[5] * s[10] + s[12] * s[6] * s[9];
    inv[1] = -s[1] * s[10] * s[15] + s[1] * s[11] * s[14] + s[9] * s[2] * s[15]
        - s[9] * s[3] * s[14] - s[13] * s[2] * s[11] + s[13] * s[3] * s[10];
    inv[5] = s[0] * s[10] * s[15] - s[0] * s[11] * s[14] - s[8] * s[2] * s[15]
        + s[8] * s[3] * s[14] + s[12] * s[2] * s[11] - s[12] * s[3] * s[10];
    inv[9] = -s[0] * s[9] * s[15] + s[0] * s[11] * s[13] + s[8] * s[1] * s[15]
        - s[8] * s[3] * s[13] - s[12] * s[1] * s[11] + s[12] * s[3] * s[9];
    inv[13] = s[0] * s[9] * s[14] - s[0] * s[10] * s[13] - s[8] * s[1] * s[14]
        + s[8] * s[2] * s[13] + s[12] * s[1] * s[10] - s[12] * s[2] * s[9];
    inv[2] = s[1] * s[6] * s[15] - s[1] * s[7] * s[14] - s[5] * s[2] * s[15]
        + s[5] * s[3] * s[14] + s[13] * s[2] * s[7] - s[13] * s[3] * s[6];
    inv[6] = -s[0] * s[6] * s[15] + s[0] * s[7] * s[14] + s[4] * s[2] * s[15]
        - s[4] * s[3] * s[14] - s[12] * s[2] * s[7] + s[12] * s[3] * s[6];
    inv[10] = s[0] * s[5] * s[15] - s[0] * s[7] * s[13] - s[4] * s[1] * s[15]
        + s[4] * s[3] * s[13] + s[12] * s[1] * s[7] - s[12] * s[3] * s[5];
    inv[14] = -s[0] * s[5] * s[14] + s[0] * s[6] * s[13] + s[4] * s[1] * s[14]
        - s[4] * s[2] * s[13] - s[12] * s[1] * s[6] + s[12] * s[2] * s[5];
    inv[3] = -s[1] * s[6] * s[11] + s[1] * s[7] * s[10] + s[5] * s[2] * s[11]
        - s[5] * s[3] * s[10] - s[9] * s[2] * s[7] + s[9] * s[3] * s[6];
    inv[7] = s[0] * s[6] * s[11] - s[0] * s[7] * s[10] - s[4] * s[2] * s[11]
        + s[4] * s[3] * s[10] + s[8] * s[2] * s[7] - s[8] * s[3] * s[6];
    inv[11] = -s[0] * s[5] * s[11] + s[0] * s[7] * s[9] + s[4] * s[1] * s[11]
        - s[4] * s[3] * s[9] - s[8] * s[1] * s[7] + s[8] * s[3] * s[5];
    inv[15] = s[0] * s[5] * s[10] - s[0] * s[6] * s[9] - s[4] * s[1] * s[10]
        + s[4] * s[2] * s[9] + s[8] * s[1] * s[6] - s[8] * s[2] * s[5];
    det = s[0] * inv[0] + s[1] * inv[4] + s[2] * inv[8] + s[3] * inv[12];
    if (det === 0) {
        return this;
    }
    det = 1 / det;
    for (i = 0; i < 16; i++) {
        d[i] = inv[i] * det;
    }
    return this;
};
Matrix4.prototype.invert = function () {
    return this.setInverseOf(this);
};
Matrix4.prototype.setOrtho = function (left, right, bottom, top, near, far) {
    var e, rw, rh, rd;
    if (left === right || bottom === top || near === far) {
        throw 'null frustum';
    }
    rw = 1 / (right - left);
    rh = 1 / (top - bottom);
    rd = 1 / (far - near);
    e = this.elements;
    e[0] = 2 * rw;
    e[1] = 0;
    e[2] = 0;
    e[3] = 0;
    e[4] = 0;
    e[5] = 2 * rh;
    e[6] = 0;
    e[7] = 0;
    e[8] = 0;
    e[9] = 0;
    e[10] = -2 * rd;
    e[11] = 0;
    e[12] = -(right + left) * rw;
    e[13] = -(top + bottom) * rh;
    e[14] = -(far + near) * rd;
    e[15] = 1;
    return this;
};
Matrix4.prototype.ortho = function (left, right, bottom, top, near, far) {
    return this.concat(new Matrix4().setOrtho(left, right, bottom, top, near, far));
};
Matrix4.prototype.setFrustum = function (left, right, bottom, top, near, far) {
    var e, rw, rh, rd;
    if (left === right || top === bottom || near === far) {
        throw 'null frustum';
    }
    if (near <= 0) {
        throw 'near <= 0';
    }
    if (far <= 0) {
        throw 'far <= 0';
    }
    rw = 1 / (right - left);
    rh = 1 / (top - bottom);
    rd = 1 / (far - near);
    e = this.elements;
    e[0] = 2 * near * rw;
    e[1] = 0;
    e[2] = 0;
    e[3] = 0;
    e[4] = 0;
    e[5] = 2 * near * rh;
    e[6] = 0;
    e[7] = 0;
    e[8] = (right + left) * rw;
    e[9] = (top + bottom) * rh;
    e[10] = -(far + near) * rd;
    e[11] = -1;
    e[12] = 0;
    e[13] = 0;
    e[14] = -2 * near * far * rd;
    e[15] = 0;
    return this;
};
Matrix4.prototype.frustum = function (left, right, bottom, top, near, far) {
    return this.concat(new Matrix4().setFrustum(left, right, bottom, top, near, far));
};
Matrix4.prototype.setPerspective = function (fovy, aspect, near, far) {
    var e, rd, s, ct;
    if (near === far || aspect === 0) {
        throw 'null frustum';
    }
    if (near <= 0) {
        throw 'near <= 0';
    }
    if (far <= 0) {
        throw 'far <= 0';
    }
    fovy = Math.PI * fovy / 180 / 2;
    s = Math.sin(fovy);
    if (s === 0) {
        throw 'null frustum';
    }
    rd = 1 / (far - near);
    ct = Math.cos(fovy) / s;
    e = this.elements;
    e[0] = ct / aspect;
    e[1] = 0;
    e[2] = 0;
    e[3] = 0;
    e[4] = 0;
    e[5] = ct;
    e[6] = 0;
    e[7] = 0;
    e[8] = 0;
    e[9] = 0;
    e[10] = -(far + near) * rd;
    e[11] = -1;
    e[12] = 0;
    e[13] = 0;
    e[14] = -2 * near * far * rd;
    e[15] = 0;
    return this;
};
Matrix4.prototype.perspective = function (fovy, aspect, near, far) {
    return this.concat(new Matrix4().setPerspective(fovy, aspect, near, far));
};
Matrix4.prototype.setScale = function (x, y, z) {
    var e = this.elements;
    e[0] = x;
    e[4] = 0;
    e[8] = 0;
    e[12] = 0;
    e[1] = 0;
    e[5] = y;
    e[9] = 0;
    e[13] = 0;
    e[2] = 0;
    e[6] = 0;
    e[10] = z;
    e[14] = 0;
    e[3] = 0;
    e[7] = 0;
    e[11] = 0;
    e[15] = 1;
    return this;
};
Matrix4.prototype.scale = function (x, y, z) {
    var e = this.elements;
    e[0] *= x;
    e[4] *= y;
    e[8] *= z;
    e[1] *= x;
    e[5] *= y;
    e[9] *= z;
    e[2] *= x;
    e[6] *= y;
    e[10] *= z;
    e[3] *= x;
    e[7] *= y;
    e[11] *= z;
    return this;
};
Matrix4.prototype.setTranslate = function (x, y, z) {
    var e = this.elements;
    e[0] = 1;
    e[4] = 0;
    e[8] = 0;
    e[12] = x;
    e[1] = 0;
    e[5] = 1;
    e[9] = 0;
    e[13] = y;
    e[2] = 0;
    e[6] = 0;
    e[10] = 1;
    e[14] = z;
    e[3] = 0;
    e[7] = 0;
    e[11] = 0;
    e[15] = 1;
    return this;
};
Matrix4.prototype.translate = function (x, y, z) {
    var e = this.elements;
    e[12] += e[0] * x + e[4] * y + e[8] * z;
    e[13] += e[1] * x + e[5] * y + e[9] * z;
    e[14] += e[2] * x + e[6] * y + e[10] * z;
    e[15] += e[3] * x + e[7] * y + e[11] * z;
    return this;
};
Matrix4.prototype.setRotate = function (angle, x, y, z) {
    var e, s, c, len, rlen, nc, xy, yz, zx, xs, ys, zs;
    angle = Math.PI * angle / 180;
    e = this.elements;
    s = Math.sin(angle);
    c = Math.cos(angle);
    if (0 !== x && 0 === y && 0 === z) {
        if (x < 0) {
            s = -s;
        }
        e[0] = 1;
        e[4] = 0;
        e[8] = 0;
        e[12] = 0;
        e[1] = 0;
        e[5] = c;
        e[9] = -s;
        e[13] = 0;
        e[2] = 0;
        e[6] = s;
        e[10] = c;
        e[14] = 0;
        e[3] = 0;
        e[7] = 0;
        e[11] = 0;
        e[15] = 1;
    }
    else if (0 === x && 0 !== y && 0 === z) {
        if (y < 0) {
            s = -s;
        }
        e[0] = c;
        e[4] = 0;
        e[8] = s;
        e[12] = 0;
        e[1] = 0;
        e[5] = 1;
        e[9] = 0;
        e[13] = 0;
        e[2] = -s;
        e[6] = 0;
        e[10] = c;
        e[14] = 0;
        e[3] = 0;
        e[7] = 0;
        e[11] = 0;
        e[15] = 1;
    }
    else if (0 === x && 0 === y && 0 !== z) {
        if (z < 0) {
            s = -s;
        }
        e[0] = c;
        e[4] = -s;
        e[8] = 0;
        e[12] = 0;
        e[1] = s;
        e[5] = c;
        e[9] = 0;
        e[13] = 0;
        e[2] = 0;
        e[6] = 0;
        e[10] = 1;
        e[14] = 0;
        e[3] = 0;
        e[7] = 0;
        e[11] = 0;
        e[15] = 1;
    }
    else {
        len = Math.sqrt(x * x + y * y + z * z);
        if (len !== 1) {
            rlen = 1 / len;
            x *= rlen;
            y *= rlen;
            z *= rlen;
        }
        nc = 1 - c;
        xy = x * y;
        yz = y * z;
        zx = z * x;
        xs = x * s;
        ys = y * s;
        zs = z * s;
        e[0] = x * x * nc + c;
        e[1] = xy * nc + zs;
        e[2] = zx * nc - ys;
        e[3] = 0;
        e[4] = xy * nc - zs;
        e[5] = y * y * nc + c;
        e[6] = yz * nc + xs;
        e[7] = 0;
        e[8] = zx * nc + ys;
        e[9] = yz * nc - xs;
        e[10] = z * z * nc + c;
        e[11] = 0;
        e[12] = 0;
        e[13] = 0;
        e[14] = 0;
        e[15] = 1;
    }
    return this;
};
Matrix4.prototype.rotate = function (angle, x, y, z) {
    return this.concat(new Matrix4().setRotate(angle, x, y, z));
};
Matrix4.prototype.setLookAt = function (eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ) {
    var e, fx, fy, fz, rlf, sx, sy, sz, rls, ux, uy, uz;
    fx = centerX - eyeX;
    fy = centerY - eyeY;
    fz = centerZ - eyeZ;
    rlf = 1 / Math.sqrt(fx * fx + fy * fy + fz * fz);
    fx *= rlf;
    fy *= rlf;
    fz *= rlf;
    sx = fy * upZ - fz * upY;
    sy = fz * upX - fx * upZ;
    sz = fx * upY - fy * upX;
    rls = 1 / Math.sqrt(sx * sx + sy * sy + sz * sz);
    sx *= rls;
    sy *= rls;
    sz *= rls;
    ux = sy * fz - sz * fy;
    uy = sz * fx - sx * fz;
    uz = sx * fy - sy * fx;
    e = this.elements;
    e[0] = sx;
    e[1] = ux;
    e[2] = -fx;
    e[3] = 0;
    e[4] = sy;
    e[5] = uy;
    e[6] = -fy;
    e[7] = 0;
    e[8] = sz;
    e[9] = uz;
    e[10] = -fz;
    e[11] = 0;
    e[12] = 0;
    e[13] = 0;
    e[14] = 0;
    e[15] = 1;
    return this.translate(-eyeX, -eyeY, -eyeZ);
};
Matrix4.prototype.lookAt = function (eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ) {
    return this.concat(new Matrix4().setLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ));
};
Matrix4.prototype.dropShadow = function (plane, light) {
    var mat = new Matrix4();
    var e = mat.elements;
    var dot = plane[0] * light[0] + plane[1] * light[1] + plane[2] * light[2] + plane[3] * light[3];
    e[0] = dot - light[0] * plane[0];
    e[1] = -light[1] * plane[0];
    e[2] = -light[2] * plane[0];
    e[3] = -light[3] * plane[0];
    e[4] = -light[0] * plane[1];
    e[5] = dot - light[1] * plane[1];
    e[6] = -light[2] * plane[1];
    e[7] = -light[3] * plane[1];
    e[8] = -light[0] * plane[2];
    e[9] = -light[1] * plane[2];
    e[10] = dot - light[2] * plane[2];
    e[11] = -light[3] * plane[2];
    e[12] = -light[0] * plane[3];
    e[13] = -light[1] * plane[3];
    e[14] = -light[2] * plane[3];
    e[15] = dot - light[3] * plane[3];
    return this.concat(mat);
};
Matrix4.prototype.dropShadowDirectionally = function (normX, normY, normZ, planeX, planeY, planeZ, lightX, lightY, lightZ) {
    var a = planeX * normX + planeY * normY + planeZ * normZ;
    return this.dropShadow([normX, normY, normZ, -a], [lightX, lightY, lightZ, 0]);
};
var Vector3 = function (opt_src) {
    var v = new Float32Array(3);
    if (opt_src && typeof opt_src === 'object') {
        v[0] = opt_src[0];
        v[1] = opt_src[1];
        v[2] = opt_src[2];
    }
    this.elements = v;
};
Vector3.prototype.normalize = function () {
    var v = this.elements;
    var c = v[0], d = v[1], e = v[2], g = Math.sqrt(c * c + d * d + e * e);
    if (g) {
        if (g == 1)
            return this;
    }
    else {
        v[0] = 0;
        v[1] = 0;
        v[2] = 0;
        return this;
    }
    g = 1 / g;
    v[0] = c * g;
    v[1] = d * g;
    v[2] = e * g;
    return this;
};
var Vector4 = function (opt_src) {
    var v = new Float32Array(4);
    if (opt_src && typeof opt_src === 'object') {
        v[0] = opt_src[0];
        v[1] = opt_src[1];
        v[2] = opt_src[2];
        v[3] = opt_src[3];
    }
    this.elements = v;
};
define("utils/defined", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Mirror;
    (function (Mirror) {
        function defined(value) {
            return value !== undefined && value !== null;
        }
        Mirror.defined = defined;
    })(Mirror = exports.Mirror || (exports.Mirror = {}));
});
define("utils/index", ["require", "exports", "utils/defined"], function (require, exports, defined_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    Object.defineProperty(exports, "__esModule", { value: true });
    __export(defined_1);
});
